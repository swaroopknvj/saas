<pdf baseFont="Helvetica,Cp1252,false">
<!-- Pending Orders -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  All Pending Orders
  $%else$
  Tous les ordres en cours
  $%endif$
</font>
<br>
<br>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Order Code</b></td>
		<td bgcolor="DBE5F1"><b>Portfolio Code</b></td>
		<td bgcolor="DBE5F1"><b>Nature</b><br></td>
		<td bgcolor="DBE5F1" align="right"><b>Adjustment Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Adjusted Quantity</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Price</b></td>
		<td bgcolor="DBE5F1"><b>Net Amount</b><br></td>
		<td bgcolor="DBE5F1"><b>Instrument</b><br></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantity</b><br></td>
		<td bgcolor="DBE5F1"><b>Status</b><br></td>
		<td bgcolor="DBE5F1"><b>Adjustment Nature</b><br></td>
		$%endif$
	</tr>
$%if TAP[1].Entities[1].Tcib_CorporateActionsG[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Tcib_CorporateActionsG[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ORDER_CODE$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].PORTFOLIO_NAME$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ORDER_NATURE$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ADJ_INSTR_CODE$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ADJ_QUANTITY$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].Price$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ORDER_NET_AMOUNT$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].INSTR_CODE$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].QUANTITY$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].STATUS$ $$TAP[1].Entities[1].Tcib_CorporateActionsG[C].OMSRequestType$ $$TAP[1].Entities[1].Tcib_CorporateActionsG[C].OMSRequestStatus$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ADJ_NATURE$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="14">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="14">Aucun résultat trouvé</td>
		$%endif$
	</tr>
$%endif$
</table>
</pdf>