<pdf baseFont="Helvetica,Cp1252,false">
<!-- Pending Orders -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  IPO Subscriptions
  $%else$
  Tous les ordres en cours
  $%endif$
</font>
<br>
<br>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Portfolio Name</b></td>
		<td bgcolor="DBE5F1"><b>Order Code</b></td>
		<td bgcolor="DBE5F1"><b>Instrument Code</b></td>
		<td bgcolor="DBE5F1"><b>Nature</b><br></td>
		<td bgcolor="DBE5F1"><b>Issue Type</b></td>
		<td bgcolor="DBE5F1"><b>Bid Type</b></td>
		<td bgcolor="DBE5F1"><b>Total Bid Qty</b></td>
		<td bgcolor="DBE5F1"><b>Alloted Qty </b><br></td>
		<td bgcolor="DBE5F1"><b>Allot.Price</b><br></td>
		<td bgcolor="DBE5F1"><b>Total Subscr.Amt</b><br></td>
		<td bgcolor="DBE5F1"><b>Status</b><br></td>
		<td bgcolor="DBE5F1"><b>IPO End Date</b><br></td>
		$%else$
		<td bgcolor="DBE5F1"><b>Nom du portefeuille</b></td>
		<td bgcolor="DBE5F1"><b>Code de l’ordre</b></td>
		<td bgcolor="DBE5F1"><b>Code de l'instrument</b></td>
		<td bgcolor="DBE5F1"><b>Nature</b><br></td>
		<td bgcolor="DBE5F1"><b>Type d’émission</b><br></td>
		<td bgcolor="DBE5F1"><b>Type d’offre</b></td>
		<td bgcolor="DBE5F1"><b>Quantité totale de l’offre</b></td>
		<td bgcolor="DBE5F1"><b>Quantité attribuée</b></td>
		<td bgcolor="DBE5F1"><b>Prix attribué</b><br></td>
		<td bgcolor="DBE5F1"><b>Montant total soucrit</b><br></td>
		<td bgcolor="DBE5F1"><b>Status</b><br></td>
		<td bgcolor="DBE5F1"><b>Date de fin</b><br></td>
		$%endif$
	</tr>
$%if TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].PORTFOLIO_NAME$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].ORDER_CODE$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].INSTR_CODE$</td>
		<td bgcolor="F0F0F0">Subscription</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].IpoIssueType$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].BidType$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].TotalBidQuantity$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].ExecQuantity$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].AvgExecQuote$$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].ORDER_CURRENCY$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].ORDER_NET_AMOUNT$$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].ACCOUNT_CURRENCY$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].Status$ $%IF TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].OMSRequestStatus != ""$($$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].OMSRequestType$ $$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].OMSRequestStatus$) $%ENDIF$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_IPOSubscriptionList[C].IpoEndDate$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="14">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="14">Aucun résultat trouvé</td>
		$%endif$
	</tr>
$%endif$
</table>
</pdf>