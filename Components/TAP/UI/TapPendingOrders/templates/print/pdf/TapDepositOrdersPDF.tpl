<pdf baseFont="Helvetica,Cp1252,false">
<!-- Pending Orders -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Deposit Orders
  $%else$
  Deposit Orders
  $%endif$
</font>
<br>
<br>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		
		<td bgcolor="DBE5F1"><b>Order Code</b></td>
		<td bgcolor="DBE5F1"><b>Type</b></td>
		<td bgcolor="DBE5F1"><b>Amount</b><br></td>
		<td bgcolor="DBE5F1"><b>Issuer</b></td>
		<td bgcolor="DBE5F1"><b>Interest Rate</b></td>
		<td bgcolor="DBE5F1"><b>Status</b></td>
		<td bgcolor="DBE5F1"><b>Account </b><br></td>
		
		$%else$
		<td bgcolor="DBE5F1"><b>Order Code</b></td>
		<td bgcolor="DBE5F1"><b>Type</b></td>
		<td bgcolor="DBE5F1"><b>Amount</b><br></td>
		<td bgcolor="DBE5F1"><b>Issuer</b></td>
		<td bgcolor="DBE5F1"><b>Interest Rate</b></td>
		<td bgcolor="DBE5F1"><b>Status</b></td>
		<td bgcolor="DBE5F1"><b>Account </b><br></td>
		$%endif$
	</tr>
$%if TAP[1].Entities[1].Tcib_DepositOrdersList[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Tcib_DepositOrdersList[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_DepositOrdersList[C].ORDER_CODE$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_DepositOrdersList[C].DepositType$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_DepositOrdersList[C].ORDER_NET_AMOUNT$</td>
		
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_DepositOrdersList[C].Counterparty$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_DepositOrdersList[C].InterestRate$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_DepositOrdersList[C].STATUS$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_DepositOrdersList[C].Account$</td>
		>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="14">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="14">Aucun résultat trouvé</td>
		$%endif$
	</tr>
$%endif$
</table>
</pdf>