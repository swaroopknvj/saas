$%if LANGUAGE_MAP_ALIAS == 'English'$All Pending Orders$%else$Tous les ordres en cours$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'
$Order Code,Portfolio Code,Nature,Adjustment Instrument,Adjusted Quantity,Price,Net Amount,>Instrument,Quantity,Status,Adjustment Nature$%endif$
$%if TAP[1].Entities[1].Tcib_CorporateActionsG[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Tcib_CorporateActionsG[C].lastInstance()$"$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ORDER_CODE$","$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].PORTFOLIO_NAME$","$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ORDER_NATURE$","$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ADJ_INSTR_CODE$","$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ADJ_QUANTITY$","$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].Price$",$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ORDER_NET_AMOUNT$,"$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].INSTR_CODE$",$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].QUANTITY$,"$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].STATUS$","$$TAP[1].Entities[1].Tcib_CorporateActionsG[C].ADJ_NATURE$"
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$