$%if LANGUAGE_MAP_ALIAS == 'English'$Today's Trade$%else$Ordres du jour$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Portfolio Name,Order Code,Instrument Code,Nature,Quantity,Price,Currency,Net Amount ,Currency,Exec. Qty,Unexec. Qty,Avg. Exec. Quote,Status,Block Order Code$%else$Portefeuille,Code du portefeuille,Code de l'instrument,Instrument,Nature,Statut de l'ordre,Quantité,Prix,Devise,Montant net,Devise,Marché,Type de limite,Validité Date,Prix limite,Devise,Prix Stop,Devise,Date création,Code de l’ordre,Code d'opération globale$%endif$
$%if TAP[1].Entities[1].TradeTodayList[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].TradeTodayList[C].lastInstance()$"$$TAP[1].Entities[1].TradeTodayList[C].PortfolioName$","$$TAP[1].Entities[1].TradeTodayList[C].OrderCode$","$$TAP[1].Entities[1].TradeTodayList[C].InstrDenom$","$$TAP[1].Entities[1].TradeTodayList[C].OrderNature$","$$TAP[1].Entities[1].TradeTodayList[C].Quantity$","$$TAP[1].Entities[1].TradeTodayList[C].Price$",$$TAP[1].Entities[1].TradeTodayList[C].OrderCurrency$,$$TAP[1].Entities[1].TradeTodayList[C].OrderNetAmount$,$$TAP[1].Entities[1].TradeTodayList[C].AccountCurrency$,$$TAP[1].Entities[1].TradeTodayList[C].ExecQuantity$,$$TAP[1].Entities[1].TradeTodayList[C].UnExecQuantity$,$$TAP[1].Entities[1].TradeTodayList[C].AvgExecQuote$,"$$TAP[1].Entities[1].TradeTodayList[C].Status$","$$TAP[1].Entities[1].TradeTodayList[C].GlobalOperationCode$"
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$
