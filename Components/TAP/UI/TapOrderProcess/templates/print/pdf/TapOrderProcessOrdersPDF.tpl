<pdf baseFont="Helvetica,Cp1252,false">
<!-- Order Process Orders -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  Orders
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
	    <td bgcolor="DBE5F1"><b>Nature</b></td>
		<td bgcolor="DBE5F1"><b>Instrument Code</b></td>
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1"><b>Portfolio</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantity</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Market Price</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Net Amount</b></td>
	 </tr>
$%if TAP[1].Entities[1].Tcib_PGOrdersDra[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Tcib_PGOrdersDra[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_PGOrdersDra[C].ORDER_NATURE_TXT$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_PGOrdersDra[C].INSTR_CODE$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_PGOrdersDra[C].INSTR_DENOM$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_PGOrdersDra[C].PORTFOLIO_CODE$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_PGOrdersDra[C].QUANTITY$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_PGOrdersDra[C].PRICE$ $$TAP[1].Entities[1].Tcib_PGOrdersDra[C].ORDER_CURRENCY$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_PGOrdersDra[C].ORDER_NET_AMOUNT$ $$TAP[1].Entities[1].Tcib_PGOrdersDra[C].ORDER_CURRENCY$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		<td bgcolor="F0F0F0" colspan="12">No results found</td>
    </tr>
$%endif$
</table>
</pdf>