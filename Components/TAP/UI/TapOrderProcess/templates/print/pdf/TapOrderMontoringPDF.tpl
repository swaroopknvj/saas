<pdf baseFont="Helvetica,Cp1252,false">
<!-- Order Process -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Order Process
  $%else$
  Opérations d’ordres
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Nature</b></td>
		<td bgcolor="DBE5F1"><b>Status</b></td>
		<td bgcolor="DBE5F1"><b>Portfolios</b></td>
		<td bgcolor="DBE5F1"><b>Description</b></td>
		<td bgcolor="DBE5F1"><b>Initiator</b></td>
		<td bgcolor="DBE5F1"><b>Initiated</b></td>
		$%else$
		<td bgcolor="DBE5F1"><b>Nature</b></td>
		<td bgcolor="DBE5F1"><b>Statut</b></td>
		<td bgcolor="DBE5F1"><b>Portefeuilles</b></td>
		<td bgcolor="DBE5F1"><b>Description</b></td>
		<td bgcolor="DBE5F1"><b>Initiateur</b></td>
		<td bgcolor="DBE5F1"><b>Initié</b></td>
		$%endif$
	</tr>
$%if TAP[1].Entities[1].Order[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Order[C].lastInstance()$
	<tr>
	    <td bgcolor="F0F0F0">TAP[1].Entities[1].Order[C].sessionNatureE</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Order[C].orderStatusE$</td>
		<td bgcolor="F0F0F0">Your Selection</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Order[C].Description$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Order[C].OrderInitiator$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Order[C].OrderInitiated$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
    </tr>
$%endif$
</table>
</pdf>