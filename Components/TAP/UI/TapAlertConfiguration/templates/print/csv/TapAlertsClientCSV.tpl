$%if LANGUAGE_MAP_ALIAS == 'English'$Clients$%else$Clients$%endif$
$%if LANGUAGE_MAP_ALIAS == 'English'$Name,Code,Market Value,Currency,Change (1 Year),Cash$%else$Nom,Code,Valeur de marché,Devise,Changement (1 An),Liquidité$%endif$
$%if TAP[1].Entities[1].Tcib_TcibThirdParty[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Tcib_TcibThirdParty[C].lastInstance()$"$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].name$","$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].code$",$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].extTdMktValM$,$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].extRefCur$,$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].extChangeMktValP$$%if TAP[1].Entities[1].Tcib_TcibThirdParty[C].extChangeMktValP != null$%$%endif$,$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].extCashPercent$$%if TAP[1].Entities[1].Tcib_TcibThirdParty[C].extCashPercent != null$%$%endif$
$%endfor$
$%else$$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$
