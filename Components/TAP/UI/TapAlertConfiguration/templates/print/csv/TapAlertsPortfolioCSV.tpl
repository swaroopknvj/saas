$%if LANGUAGE_MAP_ALIAS == 'English'$Portfolios $%else$Portefeuille$%endif$
$%if LANGUAGE_MAP_ALIAS == 'English'$Name,Code,Client Name,Market Value,Currency,Cash$%else$Nom,Code,Nom du client,Valeur de marché,Devise,Liquidité$%endif$
$%if TAP[1].Entities[1].Tcib_TcibPortfolio[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Tcib_TcibPortfolio[C].lastInstance()$"$$TAP[1].Entities[1].Tcib_TcibPortfolio[C].name$","$$TAP[1].Entities[1].Tcib_TcibPortfolio[C].code$","$$TAP[1].Entities[1].Tcib_TcibPortfolio[C].thirdName$",$$TAP[1].Entities[1].Tcib_TcibPortfolio[C].extTdMktValM$,$$TAP[1].Entities[1].Tcib_TcibPortfolio[C].extRefCur$,$$TAP[1].Entities[1].Tcib_TcibPortfolio[C].extCashPercent$$%if TAP[1].Entities[1].Tcib_TcibPortfolio[C].extCashPercent != null$%$%endif$
$%endfor$
$%else$$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$
