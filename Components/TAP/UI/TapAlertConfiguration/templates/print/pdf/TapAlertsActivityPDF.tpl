<pdf baseFont="Helvetica,Cp1252,false">
<!-- Activity Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Activities
  $%else$
  Activités
  $%endif$
</font>
<br>
<br>

<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>

    $%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Name</b></td>
    <td bgcolor="DBE5F1"><b>Status</b></td>
    <td bgcolor="DBE5F1"><b>Priority</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Due&nbsp;Date</b></td>
    <td bgcolor="DBE5F1"><b>Assigned&nbsp;To</b></td>
    <td bgcolor="DBE5F1"><b>Type</b></td>
    $%else$
    <td bgcolor="DBE5F1"><b>Nom</b></td>
    <td bgcolor="DBE5F1"><b>Statut</b></td>
    <td bgcolor="DBE5F1"><b>Priorité</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Date&nbsp;d'échéance</b></td>
    <td bgcolor="DBE5F1"><b>Attribuée&nbsp;à</b></td>
    <td bgcolor="DBE5F1"><b>Type</b></td>
    $%endif$
  </tr>

  $%if TAP[1].Entities[1].Tcib_Activities[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Tcib_Activities[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].Name$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].Status.value()$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].Priority.value()$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_Activities[C].DueDate$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].AssignedTo$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].Type.value()$</td>
  </tr>
$%endfor$
$%else$
	<tr>
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	<td bgcolor="F0F0F0" colspan="5">No results found</td>
	$%else$
	<td bgcolor="F0F0F0" colspan="5">Aucun résultat trouvé</td>
	$%endif$
	</tr>
$%endif$
</table>
</pdf>