<pdf baseFont="Helvetica,Cp1252,false">
<!-- Client Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Clients
  $%else$
  Clients
  $%endif$
</font>
<br>
<br>

<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Name</b></td>
    <td bgcolor="DBE5F1"><b>Code</b></td>
    <td bgcolor="DBE5F1"><b>Market&nbsp;Value</b><br></td>
	<td bgcolor="DBE5F1" align="right"><b>Change&nbsp;(1&nbsp;Year)</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Cash</b><br></td>
    $%else$
    <td bgcolor="DBE5F1"><b>Nom</b></td>
    <td bgcolor="DBE5F1"><b>Code</b></td>
    <td bgcolor="DBE5F1"><b>Valeur&nbsp;de&nbsp;marché</b><br></td>
	<td bgcolor="DBE5F1" align="right"><b>Changement&nbsp;(1&nbsp;An)</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Liquidité</b><br></td>
    $%endif$
  </tr>
$%if TAP[1].Entities[1].Tcib_TcibThirdParty[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Tcib_TcibThirdParty[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].name$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].code$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].extTdMktValM$$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].extRefCur$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].extChangeMktValP$$%if TAP[1].Entities[1].Tcib_TcibThirdParty[C].extChangeMktValP != null$%$%endif$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_TcibThirdParty[C].extCashPercent$$%if TAP[1].Entities[1].Tcib_TcibThirdParty[C].extCashPercent != null$%$%endif$</td>
  </tr>
$%endfor$
$%else$
	<tr>
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	<td bgcolor="F0F0F0" colspan="5">No results found</td>
	$%else$
	<td bgcolor="F0F0F0" colspan="5">Aucun résultat trouvé</td>
	$%endif$
	</tr>
$%endif$
</table>
</pdf>