<pdf baseFont="Helvetica,Cp1252,false">
<!-- Portfolio Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Risk & Performance Contributors - $$TAP[1].Entities[1].RiskIndicator$
  $%else$
  Contributeurs au risque et à la performance - $$TAP[1].Entities[1].RiskIndicator$
  $%endif$
</font>
<br>
<br>
<br>
<br>
<font size="12">
$%if LANGUAGE_MAP_ALIAS == 'English'$
Risk - Top Contributors
$%else$
Risque - Meilleurs contributeurs
$%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Market Value</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Contribution</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Risk</b></td>
    $%else$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Valeur de marché</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Contribution</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Risque</b></td>		
    $%endif$
	</tr>
$%if TAP[1].Entities[1].Measures[1].RiskLevel != '0.00' AND TAP[1].Entities[1].Measures[1].RiskLevel != 0 AND TAP[1].Entities[1].RiskPortfolioStatus != 3$
$%for 1 to TAP[1].Entities[1].RiskTopContributors[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].RiskTopContributors[C].Instrument$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RiskTopContributors[C].MarketValue$ $$TAP[1].Entities[1].RiskTopContributors[C].Currency$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RiskTopContributors[C].Contribution$ %</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RiskTopContributors[C].Risk$ %</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="4">No risk information is available.</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="4">Aucune information sur le risque n’est disponible.</td>
		$%endif$ 
  </tr>
$%endif$
</table>
</font>
<br>
<br>
<font size="12">
$%if LANGUAGE_MAP_ALIAS == 'English'$
Performance - Top Contributors
$%else$
Performance - Meilleurs contributeurs
$%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Market Value</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Contribution</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Performance</b></td>
    $%else$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Valeur de marché</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Contribution</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Performance</b></td>		
    $%endif$
	</tr>
$%if TAP[1].Entities[1].PerformanceTopContributors[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].PerformanceTopContributors[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].PerformanceTopContributors[C].Instrument$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].PerformanceTopContributors[C].MarketValue$ $$TAP[1].Entities[1].PerformanceTopContributors[C].Currency$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].PerformanceTopContributors[C].Contribution$ %</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].PerformanceTopContributors[C].Performance$ %</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
  </tr>
$%endif$
</table>
</font>
<br>
<br>
<font size="12">
$%if LANGUAGE_MAP_ALIAS == 'English'$
Risk - All Contributors
$%else$
Risque - Tous les contributeurs
$%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Market Value</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Contribution</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Risk</b></td>
    $%else$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Valeur de marché</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Contribution</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Risque</b></td>		
    $%endif$
	</tr>
$%if TAP[1].Entities[1].Measures[1].RiskLevel != '0.00' AND TAP[1].Entities[1].Measures[1].RiskLevel != 0 AND TAP[1].Entities[1].RiskPortfolioStatus != 3$
$%for 1 to TAP[1].Entities[1].RiskAllPositions[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].RiskAllPositions[C].Instrument$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RiskAllPositions[C].MarketValue$ $$TAP[1].Entities[1].RiskAllPositions[C].Currency$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RiskAllPositions[C].Contribution$ %</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RiskAllPositions[C].Risk$ %</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="4">No risk information is available.</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="4">Aucune information sur le risque n’est disponible.</td>
		$%endif$ 
  </tr>
$%endif$
</table>
</font>
<br>
<br>
<font size="12">
$%if LANGUAGE_MAP_ALIAS == 'English'$
Performance - All Contributors
$%else$
Performance - Tous les contributeurs
$%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Market Value</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Contribution</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Performance</b></td>
    $%else$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Valeur de marché</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Contribution</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Performance</b></td>		
    $%endif$
	</tr>
$%if TAP[1].Entities[1].PerformanceAllPositions[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].PerformanceAllPositions[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].PerformanceAllPositions[C].Instrument$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].PerformanceAllPositions[C].MarketValue$ $$TAP[1].Entities[1].PerformanceAllPositions[C].Currency$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].PerformanceAllPositions[C].Contribution$ %</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].PerformanceAllPositions[C].Performance$ %</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
  </tr>
$%endif$
</table>
</font>
</pdf>