$%if LANGUAGE_MAP_ALIAS == 'English'$Risk & Performance Contributors - $$TAP[1].Entities[1].RiskIndicator$$%else$Contributeurs au risque et à la performance - $$TAP[1].Entities[1].RiskIndicator$$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Risk - Top Contributors$%else$Risque - Meilleurs contributeurs$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Instrument,Market Value,Contribution,Risk$%else$Instrument,Valeur de marché,Contribution,Risque$%endif$
$%if TAP[1].Entities[1].Measures[1].RiskLevel != '0.00' AND TAP[1].Entities[1].Measures[1].RiskLevel != 0 AND TAP[1].Entities[1].RiskPortfolioStatus != 3$$%for 1 to TAP[1].Entities[1].RiskTopContributors[C].lastInstance()$"$$TAP[1].Entities[1].RiskTopContributors[C].Instrument$","$$TAP[1].Entities[1].RiskTopContributors[C].MarketValue$ $$TAP[1].Entities[1].RiskTopContributors[C].Currency$","$$TAP[1].Entities[1].RiskTopContributors[C].Contribution$ %","$$TAP[1].Entities[1].RiskTopContributors[C].Risk$ %"
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No risk information is available$%else$Aucune information sur le risque n’est disponible$%endif$ 
$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Performance - Top Contributors$%else$Performance - Meilleurs contributeurs$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Instrument,Market Value,Contribution,Performance$%else$Instrument,Valeur de marché,Contribution,Performance$%endif$
$%if TAP[1].Entities[1].PerformanceTopContributors[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].PerformanceTopContributors[C].lastInstance()$"$$TAP[1].Entities[1].PerformanceTopContributors[C].Instrument$","$$TAP[1].Entities[1].PerformanceTopContributors[C].MarketValue$ $$TAP[1].Entities[1].PerformanceTopContributors[C].Currency$","$$TAP[1].Entities[1].PerformanceTopContributors[C].Contribution$ %","$$TAP[1].Entities[1].PerformanceTopContributors[C].Performance$ %"
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$ 
$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Risk - All Contributors$%else$Risque - Tous les contributeurs$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Instrument,Market Value,Contribution,Risk$%else$Instrument,Valeur de marché,Contribution,Risque$%endif$
$%if TAP[1].Entities[1].Measures[1].RiskLevel != '0.00' AND TAP[1].Entities[1].Measures[1].RiskLevel != 0 AND TAP[1].Entities[1].RiskPortfolioStatus != 3$$%for 1 to TAP[1].Entities[1].RiskAllPositions[C].lastInstance()$"$$TAP[1].Entities[1].RiskAllPositions[C].Instrument$","$$TAP[1].Entities[1].RiskAllPositions[C].MarketValue$ $$TAP[1].Entities[1].RiskAllPositions[C].Currency$","$$TAP[1].Entities[1].RiskAllPositions[C].Contribution$ %","$$TAP[1].Entities[1].RiskAllPositions[C].Risk$ %"
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No risk information is available$%else$Aucune information sur le risque n’est disponible$%endif$ 
$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Performance - All Contributors$%else$Performance - Tous les contributeurs$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Instrument,Market Value,Contribution,Performance$%else$Instrument,Valeur de marché,Contribution,Performance$%endif$
$%if TAP[1].Entities[1].PerformanceAllPositions[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].PerformanceAllPositions[C].lastInstance()$"$$TAP[1].Entities[1].PerformanceAllPositions[C].Instrument$","$$TAP[1].Entities[1].PerformanceAllPositions[C].MarketValue$ $$TAP[1].Entities[1].PerformanceAllPositions[C].Currency$","$$TAP[1].Entities[1].PerformanceAllPositions[C].Contribution$ %","$$TAP[1].Entities[1].PerformanceAllPositions[C].Performance$ %"
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$ 
$%endif$

