<pdf baseFont="Helvetica,Cp1252,false">
<!-- Portfolio Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Compliance - Target
  $%else$
  Conformité - Objectif
  $%endif$
</font>
<br>
<br>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Strategy</b></td>
		<td bgcolor="DBE5F1"><b>Market Segment / Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Compliance</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Sub-levels Compliance</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Current Weight<br>Target Weight</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Objective Gap</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Margin</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Market Value</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Amount to Invest</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Minimum Weight<br>Maximum Weight</b></td>
    $%else$
		<td bgcolor="DBE5F1"><b>Stratégie</b></td>
		<td bgcolor="DBE5F1"><b>Segment du marche / Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Conformité</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Conformité des sous-niveaux</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Poids actuel<br>Poids cible</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Ecart de l'objectif</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Marge</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Valeur d'inventaire</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Montant à investor</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Poids minimum<br>Poids maximum</b></td>
    $%endif$
	</tr>
$%if TAP[1].Entities[1].AssetAllocation[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].AssetAllocation[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AssetAllocation[C].STRATEGY_FULL_NAME$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AssetAllocation[C].MktSegment$</td>
		<td bgcolor="F0F0F0" align="right"><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check$$TAP[1].Entities[1].AssetAllocation[C].CheckStrat$.png" width="12" height="12"></img></td>
		<td bgcolor="F0F0F0" align="right"><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check$$TAP[1].Entities[1].AssetAllocation[C].SubCheckStrat$.png" width="12" height="12"></img></td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AssetAllocation[C].ObjActualWeight$$%if TAP[1].Entities[1].AssetAllocation[C].ObjActualWeight != null$%$%endif$<br>$$TAP[1].Entities[1].AssetAllocation[C].ObjectiveWeight$$%if TAP[1].Entities[1].AssetAllocation[C].ObjectiveWeight != null$%$%endif$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AssetAllocation[C].ObjGap$$%if TAP[1].Entities[1].AssetAllocation[C].ObjGap != null$%$%endif$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AssetAllocation[C].Margin$$%if TAP[1].Entities[1].AssetAllocation[C].Margin != null$%$%endif$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AssetAllocation[C].MktVal$$%if TAP[1].Entities[1].AssetAllocation[C].MktVal != null$$$TAP[1].Entities[1].AssetAllocation[C].RefCurrency$    $%endif$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AssetAllocation[C].AmtToInv$$%if TAP[1].Entities[1].AssetAllocation[C].AmtToInv != null$$$TAP[1].Entities[1].AssetAllocation[C].RefCurrency$    $%endif$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AssetAllocation[C].MIN_WEIGHT$$%if TAP[1].Entities[1].AssetAllocation[C].MIN_WEIGHT != null$%$%endif$<br>$$TAP[1].Entities[1].AssetAllocation[C].MAX_WEIGHT$$%if TAP[1].Entities[1].AssetAllocation[C].MAX_WEIGHT != null$%$%endif$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
    </tr>
$%endif$
</table>
$%if TAP[1].Entities[1].AssetAllocation[C].lastInstance() > 0$
<br>

$%if LANGUAGE_MAP_ALIAS == 'English'$
<table border="0">
<tr><td><b>Note:</b>&nbsp;Compliance and Sub-level Compliance:<br>&nbsp;</td></tr>
</table>
<table border="0" width="40" cellspacing="1" cellpadding="1">
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check0.png" width="12" height="12"></img></td><td width="15">No Objectives Defined</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check1.png" width="12" height="12"></img></td><td width="15">Not Evaluated</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check2.png" width="12" height="12"></img></td><td width="15">Compliant</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check3.png" width="12" height="12"></img></td><td width="15">Not Compliant - Low Severity</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check4.png" width="12" height="12"></img></td><td width="15">Not Compliant - Medium Severity</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check5.png" width="12" height="12"></img></td><td width="15">Not Compliant - High Severity</td></tr>
</table>
$%else$
<table border="0">
<tr><td><b>Note:</b>&nbsp;Contrôle and Sous contrôle:<br>&nbsp;</td></tr>
</table>
<table border="0" width="40" cellspacing="1" cellpadding="1">
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check0.png" width="12" height="12"></img></td><td width="15">Aucun objectif défini</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check1.png" width="12" height="12"></img></td><td width="15">Non évalué</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check2.png" width="12" height="12"></img></td><td width="15">Conforme</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check3.png" width="12" height="12"></img></td><td width="15">Non conforme - Faible sévérité</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check4.png" width="12" height="12"></img></td><td width="15">Non conforme - Moyenne sévérité</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check5.png" width="12" height="12"></img></td><td width="15">Non conforme - Haute sévérité</td></tr>
</table>
$%endif$
<br>
$%endif$
</pdf>