<pdf baseFont="Helvetica,Cp1252,false">
<!-- Portfolio Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Compliance - Strategic
  $%else$
  Conformité - Stratégique
  $%endif$
</font>
<br>
<br>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Strategy</b></td>
		<td bgcolor="DBE5F1"><b>Market Segment</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Current Weight</b></td>
		<td bgcolor="DBE5F1"><b>Weights</b></td>
		<td bgcolor="DBE5F1"><b>Criticalness</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Market Value</b></td>
    $%else$
		<td bgcolor="DBE5F1"><b>Stratégique</b></td>
		<td bgcolor="DBE5F1"><b>Segment de marché</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Poids actuel</b></td>
		<td bgcolor="DBE5F1"><b>Poids</b></td>
		<td bgcolor="DBE5F1"><b>Criticité</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Valeur de marché</b></td>
    $%endif$
	</tr>
$%if TAP[1].Entities[1].StrategicCompliance[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].StrategicCompliance[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].StrategicCompliance[C].STRATEGY_FULL_NAME$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].StrategicCompliance[C].MktSegment$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].StrategicCompliance[C].ObjActualWeight$$%if TAP[1].Entities[1].StrategicCompliance[C].ObjActualWeight != null$%$%endif$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].StrategicCompliance[C].MIN_WEIGHT$%-$$TAP[1].Entities[1].StrategicCompliance[C].MAX_WEIGHT$%</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].StrategicCompliance[C].ObjSeverityValue$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].StrategicCompliance[C].MktVal$$%if TAP[1].Entities[1].StrategicCompliance[C].MktVal != null$$$TAP[1].Entities[1].StrategicCompliance[C].RefCurrency$    $%endif$</td>
		
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
    </tr>
$%endif$
</table>
</pdf>