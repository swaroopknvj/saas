<pdf baseFont="Helvetica,Cp1252,false">
<!-- Portfolio Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Compliance - Constraints
  $%else$
  Conformité - Contraintes
  $%endif$
</font>
<br>
<br>
</center>
<br>
<br>
<font size="10">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Modelling Constraints
  $%else$
  Contraintes de modélisation
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Description</b></td>
		<td bgcolor="DBE5F1"><b>Compliance</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Current Value</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Minimum</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Maximum</b></td>
    $%else$
		<td bgcolor="DBE5F1"><b>Description</b></td>
		<td bgcolor="DBE5F1"><b>Conformité</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Valeur actuelle</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Minimum</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Maximum</b></td>
    $%endif$
	</tr>
$%if TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Description$</td>
		<td bgcolor="F0F0F0"><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Compliance$.png" width="12" height="12"></img></td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].CurrentPosition$$%if TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].CurrentPosition != null$$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].PositionType$$%endif$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Minimum$$%if TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Minimum != null$$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].MinimumType$$%endif$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Maximum$$%if TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Maximum != null$$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].MaximumType$$%endif$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
    </tr>
$%endif$
</table>


<br>
<br>
<font size="10">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Constraints Sets
  $%else$
  Contraintes sur positions
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Description</b></td>
		<td bgcolor="DBE5F1"><b>Compliance</b></td>
    $%else$
		<td bgcolor="DBE5F1"><b>Description</b></td>
		<td bgcolor="DBE5F1"><b>Conformité</b></td>
    $%endif$
	</tr>
$%if TAP[1].Entities[1].Compliance[1].ConstraintsSet[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Compliance[1].ConstraintsSet[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Compliance[1].ConstraintsSet[C].Description$</td>
		<td bgcolor="F0F0F0" align="right"><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check$$TAP[1].Entities[1].Compliance[1].ConstraintsSet[C].Compliance$.png" width="12" height="12"></img></td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
    </tr>
$%endif$
</table>


<br>
<br>
<font size="10">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Position Excluded from Mandate
  $%else$
  Positions exclues du mandat de gestion
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Excluded Quantity</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Remaining Quantity</b></td>
    $%else$
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantité exclue</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantité restante</b></td>
    $%endif$
	</tr>
$%if TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].InstrDenom$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].ExcludedQuantity$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].RemainingQuantity$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
    </tr>
$%endif$
</table>




$%if TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].lastInstance() > 0$
<br>

$%if LANGUAGE_MAP_ALIAS == 'English'$
<table border="0">
<tr><td><b>Note:</b>&nbsp;Compliance and Sub-level Compliance:<br>&nbsp;</td></tr>
</table>
<table border="0" width="40" cellspacing="1" cellpadding="1">
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check0.png" width="12" height="12"></img></td><td width="15">No Objectives Defined</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check1.png" width="12" height="12"></img></td><td width="15">Not Evaluated</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check2.png" width="12" height="12"></img></td><td width="15">Compliant</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check3.png" width="12" height="12"></img></td><td width="15">Not Compliant - Low Severity</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check4.png" width="12" height="12"></img></td><td width="15">Not Compliant - Medium Severity</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check5.png" width="12" height="12"></img></td><td width="15">Not Compliant - High Severity</td></tr>
</table>
$%else$
<table border="0">
<tr><td><b>Note:</b>&nbsp;Contrôle and Sous contrôle:<br>&nbsp;</td></tr>
</table>
<table border="0" width="40" cellspacing="1" cellpadding="1">
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check0.png" width="12" height="12"></img></td><td width="15">Aucun objectif défini</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check1.png" width="12" height="12"></img></td><td width="15">Non évalué</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check2.png" width="12" height="12"></img></td><td width="15">Conforme</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check3.png" width="12" height="12"></img></td><td width="15">Non conforme - Faible sévérité</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check4.png" width="12" height="12"></img></td><td width="15">Non conforme - Moyenne sévérité</td></tr>
<tr><td><img src="$$LIBRARY_HOME$/templates/print/pdf/images/check5.png" width="12" height="12"></img></td><td width="15">Non conforme - Haute sévérité</td></tr>
</table>
$%endif$
<br>
$%endif$
</pdf>