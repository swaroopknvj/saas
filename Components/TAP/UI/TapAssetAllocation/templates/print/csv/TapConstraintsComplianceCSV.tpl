$%if LANGUAGE_MAP_ALIAS == 'English'$Compliance - Constraints$%else$Conformité - Contraintes$%endif$
$%if LANGUAGE_MAP_ALIAS == 'English'$ Modelling Constraints$%else$ Contraintes de modélisation$%endif$


$%if LANGUAGE_MAP_ALIAS == 'English'$Description,Compliance,Current Value,Minimum,Maximum$%else$Description,Conformité,Valeur actuelle,Minimum,Maximum$%endif$
$%if TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].lastInstance()$"$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Description$","$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Compliance$","$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].CurrentPosition$$%if TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].CurrentPosition != null$$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].PositionType$$%endif$","$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Minimum$$%if TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Minimum != null$$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].MinimumType$$%endif$","$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Maximum$$%if TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].Maximum != null$$$TAP[1].Entities[1].Compliance[1].ModellingConstraints[C].MaximumType$$%endif$"
$%endfor$

$%if LANGUAGE_MAP_ALIAS == 'English'$  Constraints Sets$%else$ Contraintes sur positions$%endif$


$%if LANGUAGE_MAP_ALIAS == 'English'$Description,Compliance$%else$Description,Conformité$%endif$
$%if TAP[1].Entities[1].Compliance[1].ConstraintsSet[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Compliance[1].ConstraintsSet[C].lastInstance()$"$$TAP[1].Entities[1].Compliance[1].ConstraintsSet[C].Description$","$$TAP[1].Entities[1].Compliance[1].ConstraintsSet[C].Compliance$"
$%endfor$

$%if LANGUAGE_MAP_ALIAS == 'English'$ Position Excluded from Mandate$%else$ Positions exclues du mandat de gestion$%endif$


$%if LANGUAGE_MAP_ALIAS == 'English'$Instrument,Excluded Quantity,Remaining Quantity$%else$Instrument,Quantité exclue,Quantité restante$%endif$
$%if TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].lastInstance()$"$$TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].InstrDenom$","$$TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].ExcludedQuantity$","$$TAP[1].Entities[1].Compliance[1].PositionsExcluded[C].RemainingQuantity$"
$%endfor$


$%if LANGUAGE_MAP_ALIAS == 'English'$
Note: Compliance and Sub-levels Compliance:
0 - No Objectives Defined
1 - Not Evaluated
2 - Compliant
3 - Not Compliant - Low Severity
4 - Not Compliant - Medium Severity
5 - Not Compliant - High Severity
$%else$
Note: Contrôle and Sous contrôle:<br> 
0 - Aucun objectif défini<br>
1 - Non évalué<br>
2 - Conforme<br>
3 - Non conforme - Faible sévérité<br>
4 - Non conforme - Moyenne sévérité<br>
5 - Non conforme - Haute sévérité<br>
$%endif$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$ 
$%endif$

