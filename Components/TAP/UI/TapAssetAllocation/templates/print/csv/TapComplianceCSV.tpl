$%if LANGUAGE_MAP_ALIAS == 'English'$Compliance$%else$Conformité$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Strategy,Market Segment / Instrument,Compliance,Sub-levels Compliance,Current Weight,Target Weight,Objective Gap,Margin,Market Value,Currency,Amount to Invest,Currency,Minimum Weight,Maximum Weight$%else$Stratégie,Segment du marche / Instrument,Conformité,Conformité des sous-niveaux,Poids actuel,Poids cible,Ecart de l'objectif,Marge,Valeur d'inventaire,Devise,Montant à investor,Devise,Poids minimum,Poids maximum$%endif$
$%if TAP[1].Entities[1].AssetAllocation[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].AssetAllocation[C].lastInstance()$"$$TAP[1].Entities[1].AssetAllocation[C].STRATEGY_FULL_NAME$","$$TAP[1].Entities[1].AssetAllocation[C].MktSegment$","$$TAP[1].Entities[1].AssetAllocation[C].CheckStrat$","$$TAP[1].Entities[1].AssetAllocation[C].SubCheckStrat$","$$TAP[1].Entities[1].AssetAllocation[C].ObjActualWeight$$%if TAP[1].Entities[1].AssetAllocation[C].ObjActualWeight != null$%$%endif$","$$TAP[1].Entities[1].AssetAllocation[C].ObjectiveWeight$$%if TAP[1].Entities[1].AssetAllocation[C].ObjectiveWeight != null$%$%endif$","$$TAP[1].Entities[1].AssetAllocation[C].ObjGap$$%if TAP[1].Entities[1].AssetAllocation[C].ObjGap != null$%$%endif$","$$TAP[1].Entities[1].AssetAllocation[C].Margin$$%if TAP[1].Entities[1].AssetAllocation[C].Margin != null$%$%endif$",$$TAP[1].Entities[1].AssetAllocation[C].MktVal$,$%if TAP[1].Entities[1].AssetAllocation[C].MktVal != null$$$TAP[1].Entities[1].AssetAllocation[C].RefCurrency$    $%endif$,$$TAP[1].Entities[1].AssetAllocation[C].AmtToInv$,"$%if TAP[1].Entities[1].AssetAllocation[C].AmtToInv != null$$$TAP[1].Entities[1].AssetAllocation[C].RefCurrency$    $%endif$","$$TAP[1].Entities[1].AssetAllocation[C].MIN_WEIGHT$$%if TAP[1].Entities[1].AssetAllocation[C].MIN_WEIGHT != null$%$%endif$","$$TAP[1].Entities[1].AssetAllocation[C].MAX_WEIGHT$$%if TAP[1].Entities[1].AssetAllocation[C].MAX_WEIGHT != null$%$%endif$"
$%endfor$
$%if LANGUAGE_MAP_ALIAS == 'English'$
Note: Compliance and Sub-levels Compliance:
0 - No Objectives Defined
1 - Not Evaluated
2 - Compliant
3 - Not Compliant - Low Severity
4 - Not Compliant - Medium Severity
5 - Not Compliant - High Severity
$%else$
Note: Contrôle and Sous contrôle:<br> 
0 - Aucun objectif défini<br>
1 - Non évalué<br>
2 - Conforme<br>
3 - Non conforme - Faible sévérité<br>
4 - Non conforme - Moyenne sévérité<br>
5 - Non conforme - Haute sévérité<br>
$%endif$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$ 
$%endif$

