<pdf baseFont="Helvetica,Cp1252,false">
<!-- Trades Today -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Today's Trade
  $%else$
  Ordres du jour
  $%endif$
</font>
<br>
<br>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Switch Order Code</b></td>
		<td bgcolor="DBE5F1"><b>Instrument Code</b></td>
		<td bgcolor="DBE5F1"><b>Nature</b><br></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantity</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Price</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Net Amount</b></td>
		<td bgcolor="DBE5F1"><b>Exec. Qty</b><br></td>
		<td bgcolor="DBE5F1"><b>Unexec. Qty</b><br></td>
		<td bgcolor="DBE5F1" align="right"><b>Avg. Exec. Quote</b><br></td>
		<td bgcolor="DBE5F1"><b>Status</b><br></td>
		$%else$
		<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
		<td bgcolor="DBE5F1"><b>Code de l'instrument<br>Instrument</b></td>
		<td bgcolor="DBE5F1"><b>Nature</b><br></td>
		<td bgcolor="DBE5F1"><b>Statut de l'ordre</b><br></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantité</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Prix</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Montant net</b></td>
		<td bgcolor="DBE5F1"><b>Marché</b><br></td>
		<td bgcolor="DBE5F1"><b>Type de limite</b><br></td>
		<td bgcolor="DBE5F1" align="right"><b>Validité Date</b><br></td>
		<td bgcolor="DBE5F1" align="right"><b>Prix limite<br>Prix Stop</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Date création</b></td>
		<td bgcolor="DBE5F1"><b>Code de l’ordre</b><br></td>
		<td bgcolor="DBE5F1"><b>Code d'opération globale</b><br></td>
		$%endif$
	</tr>
$%if TAP[1].Entities[1].SwitchTradeTodayListG[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].SwitchTradeTodayListG[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].OrderCode$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].InstrCode$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].OrderNature$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].Quantity$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].Price$$$TAP[1].Entities[1].SwitchTradeTodayListG[C].OrderCurrency$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].OrderNetAmount$$$TAP[1].Entities[1].SwitchTradeTodayListG[C].AccountCurrency$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].ExecQuantity$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].UnExecQuantity$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].AvgExecQuote$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].SwitchTradeTodayListG[C].Status$ $$TAP[1].Entities[1].SwitchTradeTodayListG[C].OMSRequestType$ $$TAP[1].Entities[1].SwitchTradeTodayListG[C].OMSRequestStatus$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="14">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspam="14">Aucun résultat trouvé</td>
		$%endif$
	</tr>
$%endif$
</table>
</pdf>