<pdf baseFont="Helvetica,Cp1252,false">
<!-- Notification Activity Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Notification Details - Activity
  $%else$
  Détails de la notification - Activity
  $%endif$
</font>
<br>
<font size="12">$$TAP[1].Entities[1].Tcib_TcibAlertNotificationDetails[1].subject$</font>
<br>
$%if TAP[1].Entities[1].Tcib_Activities[C].lastInstance() > 0$
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Description</b></td>
    <td bgcolor="DBE5F1"><b>Status</b></td>
	<td bgcolor="DBE5F1"><b>Priority</b></td>
    <td bgcolor="DBE5F1"><b>Due&nbsp;Date</b><br></td>
	<td bgcolor="DBE5F1"><b>Assigned&nbsp;To</b><br></td>
	<td bgcolor="DBE5F1"><b>Type</b><br></td>
    $%else$
    <td bgcolor="DBE5F1"><b>Description</b></td>
    <td bgcolor="DBE5F1"><b>Statut</b></td>
	<td bgcolor="DBE5F1"><b>Priorité</b></td>
    <td bgcolor="DBE5F1"><b>Date&nbsp;d’échéance</b><br></td>
	<td bgcolor="DBE5F1"><b>Attribuée&nbsp;à</b><br></td>
	<td bgcolor="DBE5F1"><b>Type</b><br></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].Tcib_Activities[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].Name$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].Status.value()$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].Priority.value()$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].DueDate$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].AssignedTo$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_Activities[C].Type.value()$</td>
  </tr>
$%endfor$
</table>
<table border="0">
<tr><td>
$%if LANGUAGE_MAP_ALIAS == 'English'$
<b>Note:</b>&nbsp;The results shown above are based on online information and may differ from the time the notification was sent.
$%else$
<b>Remarque:</b>&nbsp;La liste de résultats affichée ci-dessus se base sur les informations actuelles et ne représente pas l’état au moment de l’exécution de l’alerte
$%endif$
</td></tr>
</table>
<br>
<br>
$%else$
	<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
	<tr>
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	<td bgcolor="F0F0F0">No results found</td>
	$%else$
	<td bgcolor="F0F0F0">Aucun résultat trouvé</td>
	$%endif$
	</tr>
	</table>
$%endif$
</pdf>