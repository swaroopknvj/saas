<pdf baseFont="Helvetica,Cp1252,false">
<!-- Notification Portfolio Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Notification Details - Portfolio
  $%else$
  Détails de la notification - Portefeuille
  $%endif$
</font>
<br>
<font size="12">$$TAP[1].Entities[1].Tcib_TcibAlertNotificationDetails[1].subject$</font>
<br>
$%if TAP[1].Entities[1].Tcib_PortfolioList[C].lastInstance() > 0$
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Name</b></td>
    <td bgcolor="DBE5F1"><b>Code</b></td>
    <td bgcolor="DBE5F1"><b>Client&nbsp;Name</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Market&nbsp;Value</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Cash</b><br></td>
    $%else$
    <td bgcolor="DBE5F1"><b>Nom</b></td>
    <td bgcolor="DBE5F1"><b>Code</b></td>
    <td bgcolor="DBE5F1"><b>Nom&nbsp;du&nbsp;client</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Valeur&nbsp;de&nbsp;marché</b><br></td>
	<td bgcolor="DBE5F1" align="right"><b>Liquidité</b><br></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].Tcib_PortfolioList[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_PortfolioList[C].Name$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_PortfolioList[C].Code$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_PortfolioList[C].ClientName$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_PortfolioList[C].MarketValue$ $$TAP[1].Entities[1].Tcib_PortfolioList[C].Currency$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_PortfolioList[C].Cash$ $%if TAP[1].Entities[1].Tcib_PortfolioList[C].Cash != null$%$%endif$</td>
  </tr>
$%endfor$
</table>
<table border="0">
<tr><td>
$%if LANGUAGE_MAP_ALIAS == 'English'$
<b>Note:</b>&nbsp;The results shown above are based on online information and may differ from the time the notification was sent.
$%else$
<b>Remarque:</b>&nbsp;La liste de résultats affichée ci-dessus se base sur les informations actuelles et ne représente pas l’état au moment de l’exécution de l’alerte
$%endif$
</td></tr>
</table>
<br>
<br>
$%else$
	<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
	<tr>
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	<td bgcolor="F0F0F0">No results found</td>
	$%else$
	<td bgcolor="F0F0F0">Aucun résultat trouvé</td>
	$%endif$
	</tr>
	</table>
$%endif$
</pdf>