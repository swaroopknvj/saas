$%if LANGUAGE_MAP_ALIAS == 'English'$Portfolios $%else$Portefeuille$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Name,Code,Client Name,Market Value,Currency,Cash$%else$Nom,Code,Nom du client,Valeur de marché,Devise,Liquidité$%endif$
$%if TAP[1].Entities[1].Tcib_PortfolioList[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Tcib_PortfolioList[C].lastInstance()$"$$TAP[1].Entities[1].Tcib_PortfolioList[C].name$","$$TAP[1].Entities[1].Tcib_PortfolioList[C].code$","$$TAP[1].Entities[1].Tcib_PortfolioList[C].ClientName$",$$TAP[1].Entities[1].Tcib_PortfolioList[C].MarketValue$,$$TAP[1].Entities[1].Tcib_PortfolioList[C].Currency$,$$TAP[1].Entities[1].Tcib_PortfolioList[C].Cash$$%if TAP[1].Entities[1].Tcib_PortfolioList[C].Cash != null$%$%endif$
$%endfor$
$%if LANGUAGE_MAP_ALIAS == 'English'$
Note: The results shown above are based on online information and may differ from the time the notification was sent.
$%else$
Remarque: La liste de résultats affichée ci-dessus se base sur les informations actuelles et ne représente pas l’état au moment de l’exécution de l’alerte
$%endif$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$