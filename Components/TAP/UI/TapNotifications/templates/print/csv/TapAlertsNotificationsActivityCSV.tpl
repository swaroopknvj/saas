$%if LANGUAGE_MAP_ALIAS == 'English'$Activities$%else$Activities$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Description,Status,Priority,Due Date,Assigned To,Type$%else$Description,Statut,Priorité,Date d’échéance,Attribuée à,Type$%endif$
$%if TAP[1].Entities[1].Tcib_Activities[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Tcib_Activities[C].lastInstance()$"$$TAP[1].Entities[1].Tcib_Activities[C].Name$","$$TAP[1].Entities[1].Tcib_Activities[C].Status.value()$","$$TAP[1].Entities[1].Tcib_Activities[C].Priority.value()$","$$TAP[1].Entities[1].Tcib_Activities[C].DueDate$","$$TAP[1].Entities[1].Tcib_Activities[C].AssignedTo$","$$TAP[1].Entities[1].Tcib_Activities[C].Type.value()$"
$%endfor$
$%if LANGUAGE_MAP_ALIAS == 'English'$
Note: The results shown above are based on online information and may differ from the time the notification was sent.
$%else$
Remarque: La liste de résultats affichée ci-dessus se base sur les informations actuelles et ne représente pas l’état au moment de l’exécution de l’alerte
$%endif$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$
