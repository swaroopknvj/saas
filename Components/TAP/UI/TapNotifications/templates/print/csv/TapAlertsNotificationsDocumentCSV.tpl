$%if LANGUAGE_MAP_ALIAS == 'English'$Documents$%else$Documents$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Name,Type,Status,Expiry Date,Client Name$%else$Nom,Type,Status,Date d'expiration,Nom du Client$%endif$
$%if TAP[1].Entities[1].Tcib_ThirdDocuments[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Tcib_ThirdDocuments[C].lastInstance()$"$$TAP[1].Entities[1].Tcib_ThirdDocuments[C].Title$","$$TAP[1].Entities[1].Tcib_ThirdDocuments[C].Type.value()$","$$TAP[1].Entities[1].Tcib_ThirdDocuments[C].Status.value()$",$$TAP[1].Entities[1].Tcib_ThirdDocuments[C].ExpiryDate$,"$$TAP[1].Entities[1].Tcib_ThirdDocuments[C].thirdPartyName$"$%endfor$
$%if LANGUAGE_MAP_ALIAS == 'English'$
Note: The results shown above are based on online information and may differ from the time the notification was sent.
$%else$
Remarque: La liste de résultats affichée ci-dessus se base sur les informations actuelles et ne représente pas l’état au moment de l’exécution de l’alerte
$%endif$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$
