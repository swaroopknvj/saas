<pdf baseFont="Helvetica,Cp1252,false">
<!-- Data Profile Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if TAP[1].Entities[1].EditBusinessEnity[1].Mode == 'Edit'$
  $$TAP[1].Entities[1].EditBusinessEnity[1].dataProfCode$ - Composition
  $%else$
  $$TAP[1].Entities[1].Profile[1].DataProfileValue$ - Composition
  $%endif$
</font>
<br>
<br>
$%if TAP[1].Entities[1].DataProfCompos[C].lastInstance() > 0$
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Data Security Profile</b></td>
    <td bgcolor="DBE5F1"><b>Update Authorised</b></td>
	<td bgcolor="DBE5F1"><b>Delete Authorised</b></td>
    $%else$
    <td bgcolor="DBE5F1"><b>Profil de sécurité des données</b></td>
    <td bgcolor="DBE5F1"><b>Mise à jour autorisée</b></td>
	<td bgcolor="DBE5F1"><b>Suppression autorisée</b></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].DataProfCompos[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DataProfCompos[C].dataSecuProfCode$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DataProfCompos[C].authUpdateF$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DataProfCompos[C].authDeleteF$</td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%else$
	<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
	<tr>
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	<td bgcolor="F0F0F0">No results found</td>
	$%else$
	<td bgcolor="F0F0F0">Aucun résultat trouvé/td>
	$%endif$
	</tr>
	</table>
$%endif$
</pdf>