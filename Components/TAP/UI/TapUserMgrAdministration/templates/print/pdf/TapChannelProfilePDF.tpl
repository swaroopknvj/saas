<pdf baseFont="Helvetica,Cp1252,false">
<!-- Channel Profile Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  $$TAP[1].Entities[1].Profile[1].ChannelProfileValue$- Composition
  $%else$
  $$TAP[1].Entities[1].Profile[1].ChannelProfileValue$- Composition
  $%endif$
</font>
<br>
<br>
$%if TAP[1].Entities[1].ChannelProfCompos[C].lastInstance() > 0$
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Channel</b></td>
    <td bgcolor="DBE5F1"><b>Function Security Profile</b></td>
	<td bgcolor="DBE5F1"><b>Screen Profile</b></td>
	<td bgcolor="DBE5F1"><b>External Service Profile</b></td>
	<td bgcolor="DBE5F1"><b>Report Profile</b></td>
    $%else$
    <td bgcolor="DBE5F1"><b>Canal</b></td>
    <td bgcolor="DBE5F1"><b>Profil de sécurité des fonctions</b></td>
	<td bgcolor="DBE5F1"><b>Profil d'écran</b></td>
	<td bgcolor="DBE5F1"><b>Profil du Service Externe</b></td>
	<td bgcolor="DBE5F1"><b>Profil de rapport</b></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].ChannelProfCompos[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ChannelProfCompos[C].applChannelProfileCode$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ChannelProfCompos[C].funcSecuProfCode$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ChannelProfCompos[C].screenProfileCode$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].ChannelProfCompos[C].extServiceProfCode$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ChannelProfCompos[C].reportProfileCode$</td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%else$
	<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
	<tr>
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	<td bgcolor="F0F0F0">No results found</td>
	$%else$
	<td bgcolor="F0F0F0">Aucun résultat trouvé/td>
	$%endif$
	</tr>
	</table>
$%endif$
</pdf>