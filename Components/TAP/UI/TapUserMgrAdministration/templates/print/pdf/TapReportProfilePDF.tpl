<pdf baseFont="Helvetica,Cp1252,false">
<!-- Report Profile Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  $$TAP[1].Entities[1].Profile[1].ReportProfileValue$- Composition
  $%else$
  $$TAP[1].Entities[1].Profile[1].ReportProfileValue$- Composition
  $%endif$
</font>
<br>
<br>
$%if TAP[1].Entities[1].ReportProfCompos[C].lastInstance() > 0$
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Code</b></td>
    <td bgcolor="DBE5F1"><b>Denomination</b></td>
	<td bgcolor="DBE5F1"><b>Nature</b></td>
    $%else$
    <td bgcolor="DBE5F1"><b>Code</b></td>
    <td bgcolor="DBE5F1"><b>Dénomination</b></td>
	<td bgcolor="DBE5F1"><b>Nature</b></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].ReportProfCompos[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ReportProfCompos[C].reportCode$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ReportProfCompos[C].reportDenom$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ReportProfCompos[C].reportNatureE.value()$</td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%else$
	<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
	<tr>
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	<td bgcolor="F0F0F0">No results found</td>
	$%else$
	<td bgcolor="F0F0F0">Aucun résultat trouvé/td>
	$%endif$
	</tr>
	</table>
$%endif$
</pdf>