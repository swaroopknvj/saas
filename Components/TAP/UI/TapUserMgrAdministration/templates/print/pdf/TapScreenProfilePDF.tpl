<pdf baseFont="Helvetica,Cp1252,false">
<!-- Screen Profile Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  $$TAP[1].Entities[1].Profile[1].ScreenProfileValue$- Composition
  $%else$
  $$TAP[1].Entities[1].Profile[1].ScreenProfileValue$- Composition
  $%endif$
</font>
<br>
<br>
$%if TAP[1].Entities[1].ScreenProfileCompos[C].lastInstance() > 0$
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Screen</b></td>
    <td bgcolor="DBE5F1"><b>Entity</b></td>
	<td bgcolor="DBE5F1"><b>Nature attribute</b></td>
	<td bgcolor="DBE5F1"><b>Nature</b></td>
	<td bgcolor="DBE5F1"><b>Function</b></td>
    $%else$
    <td bgcolor="DBE5F1"><b>écran</b></td>
    <td bgcolor="DBE5F1"><b>Entité</b></td>
	<td bgcolor="DBE5F1"><b>Attribut Nature</b></td>
	<td bgcolor="DBE5F1"><b>Nature</b></td>
	<td bgcolor="DBE5F1"><b>Fonction</b></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].ScreenProfileCompos[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ScreenProfileCompos[C].screenName$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ScreenProfileCompos[C].entityName$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ScreenProfileCompos[C].natAttribName$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].ScreenProfileCompos[C].natureE$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].ScreenProfileCompos[C].functionName$</td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%else$
	<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
	<tr>
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	<td bgcolor="F0F0F0">No results found</td>
	$%else$
	<td bgcolor="F0F0F0">Aucun résultat trouvé/td>
	$%endif$
	</tr>
	</table>
$%endif$
</pdf>