<pdf baseFont="Helvetica,Cp1252,false">
<!-- Functional Security Profile Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  $$TAP[1].Entities[1].Profile[1].FuncSecProfileValue$ - Composition
  $%else$
  $$TAP[1].Entities[1].Profile[1].FuncSecProfileValue$ - Composition
  $%endif$
</font>
<br>
<br>
$%if TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].lastInstance() > 0$
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Function Procedure Name</b></td>
    <td bgcolor="DBE5F1"><b>Function Name</b></td>
	<td bgcolor="DBE5F1"><b>Object type</b></td>
    <td bgcolor="DBE5F1"><b>Min Status</b></td>
	<td bgcolor="DBE5F1"><b>Max Status</b></td>
    <td bgcolor="DBE5F1"><b>Create</b></td>
	<td bgcolor="DBE5F1"><b>Update</b></td>
    <td bgcolor="DBE5F1"><b>Delete</b></td>
	<td bgcolor="DBE5F1"><b>Security Level</b></td>
    <td bgcolor="DBE5F1"><b>Risk View</b></td>
	<td bgcolor="DBE5F1"><b>Real time</b></td>
    $%else$
    <td bgcolor="DBE5F1"><b>Nom de la procédure de fonction</b></td>
    <td bgcolor="DBE5F1"><b>Nom de la fonction</b></td>
	<td bgcolor="DBE5F1"><b>Type d'objet</b></td>
    <td bgcolor="DBE5F1"><b>Statut min</b></td>
	<td bgcolor="DBE5F1"><b>Statut max</b></td>
    <td bgcolor="DBE5F1"><b>Créer</b></td>
	<td bgcolor="DBE5F1"><b>Mettre à jour</b></td>
    <td bgcolor="DBE5F1"><b>Effacer</b></td>
	<td bgcolor="DBE5F1"><b>Niveau de sécurité</b></td>
    <td bgcolor="DBE5F1"><b>Vue du risque</b></td>
	<td bgcolor="DBE5F1"><b>Temps réél</b></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].functionProcName$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].functionName$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].entityName$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].minStatusE$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].maxStatusE$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].deleteF$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].createF$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].updateF$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].securityLevelE$</td>
	<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].riskViewF$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_FuncSecuProfCompo[C].realTimeF$</td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%else$
	<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
	<tr>
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	<td bgcolor="F0F0F0">No results found</td>
	$%else$
	<td bgcolor="F0F0F0">Aucun résultat trouvé/td>
	$%endif$
	</tr>
	</table>
$%endif$
</pdf>