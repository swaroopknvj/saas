$%if LANGUAGE_MAP_ALIAS == 'English'$Portfolios $%else$Portfolios $%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Name,Code,Client Name,Market Value,Currency,Cash$%else$Nom,Code,Nom du client,Valeur de marché,Devise,Liquidité$%endif$
$%if TAP[1].Entities[1].Tcib_Portfolios[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Tcib_Portfolios[C].lastInstance()$"$$TAP[1].Entities[1].Tcib_Portfolios[C].Name$","$$TAP[1].Entities[1].Tcib_Portfolios[C].Code$","$$TAP[1].Entities[1].Tcib_Portfolios[C].UserCode$",$$TAP[1].Entities[1].Tcib_Portfolios[C].MarketValue$,$$TAP[1].Entities[1].Tcib_Portfolios[C].Currency$,$$TAP[1].Entities[1].Tcib_Portfolios[C].CashPercentage$$%if TAP[1].Entities[1].Tcib_Portfolios[C].CashPercentage != null$%$%endif$
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$
No results found
$%else$
Aucun résultat trouvé
$%endif$
$%endif$