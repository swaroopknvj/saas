$%if LANGUAGE_MAP_ALIAS == 'English'$Activities$%else$Activités$%endif$
$%if TAP[1].Entities[1].Tcib_Activities[C].lastInstance() > 0$
$%if LANGUAGE_MAP_ALIAS == 'English'$Name,Status,Priority,Due Date,Assigned To,Type$%else$Nom,Priorité,Statut,Date d'échéance, Attribuée à, Type$%endif$
$%for 1 to TAP[1].Entities[1].Tcib_Activities[C].lastInstance()$"$$TAP[1].Entities[1].Tcib_Activities[C].Name$","$$TAP[1].Entities[1].Tcib_Activities[C].Status.value()$","$$TAP[1].Entities[1].Tcib_Activities[C].Priority.value()$","$$TAP[1].Entities[1].Tcib_Activities[C].DueDate$","$$TAP[1].Entities[1].Tcib_Activities[C].AssignedTo$","$$TAP[1].Entities[1].Tcib_Activities[C].Type.value()$"
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$


