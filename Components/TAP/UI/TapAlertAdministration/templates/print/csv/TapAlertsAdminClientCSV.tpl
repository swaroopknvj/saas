$%if LANGUAGE_MAP_ALIAS == 'English'$Clients$%else$Clients$%endif$
$%if LANGUAGE_MAP_ALIAS == 'English'$Name,Code,Market Value,Currency,Change (1 Year),Cash$%else$Nom,Code,Valeur de marché,Devise,Changement (1 An),Liquidité$%endif$
$%if TAP[1].Entities[1].Tcib_ThirdParties[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Tcib_ThirdParties[C].lastInstance()$"$$TAP[1].Entities[1].Tcib_ThirdParties[C].Name$","$$TAP[1].Entities[1].Tcib_ThirdParties[C].Code$",$$TAP[1].Entities[1].Tcib_ThirdParties[C].MarketValue$,$$TAP[1].Entities[1].Tcib_ThirdParties[C].Currency$,$$TAP[1].Entities[1].Tcib_ThirdParties[C].Variation$$%if TAP[1].Entities[1].Tcib_ThirdParties[C].Variation != null$%$%endif$,$$TAP[1].Entities[1].Tcib_ThirdParties[C].CashPercentage$$%if TAP[1].Entities[1].Tcib_ThirdParties[C].CashPercentage != null$%$%endif$
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$
No results found
$%else$
Aucun résultat trouvé
$%endif$
$%endif$
