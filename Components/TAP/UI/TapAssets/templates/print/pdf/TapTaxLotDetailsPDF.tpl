<pdf baseFont="Helvetica,Cp1252,false">
<!-- Tax Lot Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	Tax Lot Details
	$%else$
	Detail lot fiscal
	$%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Instrument Code<br>Instr. Denom.</b></td>
		<td bgcolor="DBE5F1"><b>Portfolio Name</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Acquisition Date<br>Acquisition Quantity</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Acquisition Price<br>Acquisition Cost</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Remaining Quantity</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Market Price Date<br>Market Price</b></td>	
		<td bgcolor="DBE5F1" align="right"><b>Unrealized P&L (Long)<br>Unrealized P&L (Short)</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Realised Global P&L (Long)<br>Realised Global P&L (Short)</b></td>
		<td bgcolor="DBE5F1"><b>Cost Method<br>Lot Age (in years)</b></td>
		$%else$
		<td bgcolor="DBE5F1"><b>Code d'instrument<br>Dénomination de l'instrument</b></td>
		<td bgcolor="DBE5F1"><b>Nom du portefeuille</b></td>
		<td bgcolor="DBE5F1"><b>Date d’acquisition<br>Quantité d'acquisition</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Prix d'acquisition<br>Coût d'acquisition</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantité Résiduelle</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Date du prix du marché<br>Prix du marché</b></td>	
		<td bgcolor="DBE5F1" align="right"><b>Non-réalisé P&L longue<br>Non-réalisé P&L Court</b></td>
		<td bgcolor="DBE5F1" align="right"><b>P&L Réalisé Global Longue<br>P&L Réalisé Global Court</b></td>
		<td bgcolor="DBE5F1"><b>Méthode de coût<br>Age du lot (en années)</b></td>
		$%endif$
	 </tr>
$%if TAP[1].Entities[1].TaxLotDetails[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].TaxLotDetails[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].TaxLotDetails[C].instrumentCode$<br>$$TAP[1].Entities[1].TaxLotDetails[C].instrumentDenom$</td>
		<td bgcolor="F0F0F0">$$TAP[1].WorkingElements[1].SelectedPortfolio$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].TaxLotDetails[C].acquisitionD$<br>$$TAP[1].Entities[1].TaxLotDetails[C].initialQuantityN$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].TaxLotDetails[C].initialPriceN$ $$TAP[1].Entities[1].TaxLotDetails[C].currency$<br>$$TAP[1].Entities[1].TaxLotDetails[C].initialPosNetAmountM$ $$TAP[1].Entities[1].TaxLotDetails[C].currency$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].TaxLotDetails[C].quantityN$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].TaxLotDetails[C].domainCalcPivotD$<br>$$TAP[1].Entities[1].TaxLotDetails[C].marketPriceN$ $$TAP[1].Entities[1].TaxLotDetails[C].currency$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].TaxLotDetails[C].unrealPlLongM$<br>$$TAP[1].Entities[1].TaxLotDetails[C].unrealPlShortM$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].TaxLotDetails[C].posPlLtNetAmountM$<br>$$TAP[1].Entities[1].TaxLotDetails[C].posPlStNetAmountM$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].TaxLotDetails[C].fusionRuleE$<br>$$TAP[1].Entities[1].TaxLotDetails[C].taxLotAgeYearN$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
    </tr>
$%endif$
</table>
</pdf>