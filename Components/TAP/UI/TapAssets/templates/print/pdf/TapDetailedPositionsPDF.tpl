<pdf baseFont="Helvetica,Cp1252,false">
<!-- Portfolio Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Detailed Positions
  $%else$
  Positions détaillées
  $%endif$
</font>
<br>
<br>
$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy != '1'$
$%if TAP[1].Entities[1].DetailedPositionsAll[C].lastInstance() > 0$
<font size="8">
<table width="100%" border="0" bordercolor="ffffff" cellspacing="2" cellpadding="4">
	<tr>
	    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="95B3D7"><b>Portfolio Name</b></td>
		$%else$
		<td bgcolor="95B3D7"><b>Portefeuille</b></td>
		$%endif$
		<td bgcolor="95B3D7">$$TAP[1].Entities[1].DetailedPositionsAll[1].PortfolioName$</td>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="95B3D7"><b>Portfolio Code</b></td>
		$%else$
		<td bgcolor="95B3D7"><b>Code du portefeuille</b></td>
		$%endif$
		<td bgcolor="95B3D7">$$TAP[1].Entities[1].DetailedPositionsAll[1].Portfolio$</td>
	</tr>
</table>
$%endif$
$%endif$

$%if TAP[1].Entities[1].DetailedPositionsAll[C].lastInstance() < 1$
<font size="8">
	<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
		<tr>
			$%if LANGUAGE_MAP_ALIAS == 'English'$
			$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
			<td bgcolor="DBE5F1"><b>Portfolio Name<br>Portfolio Code</b></td>
			$%endif$
			<td bgcolor="DBE5F1"><b>Instrument<br>Code</b></td>
			<td bgcolor="DBE5F1" align="right"><b>Quantity</b></td>
			<td bgcolor="DBE5F1"><b>Currency<br>Quote&nbsp;Date</b></td>
			<td bgcolor="DBE5F1" align="right"><b>Quote<br>Cost&nbsp;Quote</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Exchange&nbsp;Rate<br>Cost&nbsp;Exch.&nbsp;Rate</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Market&nbsp;Value<br>Cost&nbsp;Value</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Profit&nbsp;/&nbsp;Loss&nbsp;%<br>Profit&nbsp;/&nbsp;Loss</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Weight<br>Category&nbsp;Weight</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Region<br>Sector</b><br></td>
			$%else$
			$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
			<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
			$%endif$
			<td bgcolor="DBE5F1"><b>Code de<br>l'instrument</b></td>
			<td bgcolor="DBE5F1" align="right"><b>Quantité</b></td>
			<td bgcolor="DBE5F1"><b>Devise<br>Date&nbsp;du&nbsp;cours</b></td>
			<td bgcolor="DBE5F1" align="right"><b>Cours<br>Coût&nbsp;du&nbsp;cours</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Taux&nbsp;de&nbsp;change<br>Coût&nbsp;du&nbsp;taux&nbsp;de&nbsp;change</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Valeur&nbsp;de&nbsp;marché<br>Coût&nbsp;d'achat</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Gain&nbsp;/&nbsp;Perte&nbsp;%<br>Gain&nbsp;/&nbsp;Perte</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Pondération<br>Catégorie&nbsp;de&nbsp;pondération</b><br></td>
			<td bgcolor="DBE5F1" align="right"><b>Région<br>Secteur</b><br></td>
			$%endif$
		</tr>
		<tr>
			$%if LANGUAGE_MAP_ALIAS == 'English'$
				$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
				<td bgcolor="F0F0F0" colspan="10">No results found</td>
				$%else$
				<td bgcolor="F0F0F0" colspan="9">No results found</td>
				$%endif$
			$%else$
				$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
				<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
				$%else$
				<td bgcolor="F0F0F0" colspan="9">Aucun résultat trouvé</td>
				$%endif$
			$%endif$
		</tr>
	</table>
$%endif$

$%if TAP[1].Entities[1].DetailedPositionsStocks[C].lastInstance() > 0$
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Stocks
  $%else$
  Actions
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portfolio Name<br>Portfolio Code</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Instrument<br>Code</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Quantity</b></td>
    <td bgcolor="DBE5F1"><b>Currency<br>Quote&nbsp;Date</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Quote<br>Cost&nbsp;Quote</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Exchange&nbsp;Rate<br>Cost&nbsp;Exch.&nbsp;Rate</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Market&nbsp;Value<br>Cost&nbsp;Value</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Profit&nbsp;/&nbsp;Loss&nbsp;%<br>Profit&nbsp;/&nbsp;Loss</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Weight<br>Category&nbsp;Weight</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Region<br>Sector</b><br></td>
    $%else$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Code de<br>l'instrument</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Quantité</b></td>
  	<td bgcolor="DBE5F1"><b>Devise<br>Date&nbsp;du&nbsp;cours</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Cours<br>Coût&nbsp;du&nbsp;cours</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Taux&nbsp;de&nbsp;change<br>Coût&nbsp;du&nbsp;taux&nbsp;de&nbsp;change</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Valeur&nbsp;de&nbsp;marché<br>Coût&nbsp;d'achat</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Gain&nbsp;/&nbsp;Perte&nbsp;%<br>Gain&nbsp;/&nbsp;Perte</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Pondération<br>Catégorie&nbsp;de&nbsp;pondération</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Région<br>Secteur</b><br></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].DetailedPositionsStocks[C].lastInstance()$
  <tr>
    $%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsStocks[C].PortfolioName$<br>$$TAP[1].Entities[1].DetailedPositionsStocks[C].Portfolio$</td>
	$%endif$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsStocks[C].Denomination$<br>$$TAP[1].Entities[1].DetailedPositionsStocks[C].Code$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsStocks[C].Quantity$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsStocks[C].Ccy$<br>$$TAP[1].Entities[1].DetailedPositionsStocks[C].MarketPriceDate$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsStocks[C].MarketPrice$ $$TAP[1].Entities[1].DetailedPositionsStocks[C].MarketPriceCcy$<br>$$TAP[1].Entities[1].DetailedPositionsStocks[C].CostPrice$ $$TAP[1].Entities[1].DetailedPositionsStocks[C].MarketPriceCcy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsStocks[C].FXRate$<br>$$TAP[1].Entities[1].DetailedPositionsStocks[C].CostFXRate$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsStocks[C].ValueValCcy$ $$TAP[1].Entities[1].DetailedPositionsStocks[C].RefCcy$<br>$$TAP[1].Entities[1].DetailedPositionsStocks[C].CostValCcy$ $$TAP[1].Entities[1].DetailedPositionsStocks[C].RefCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsStocks[C].ProfitLossPercent$$%IF "##TAP[1].Entities[1].DetailedPositionsStocks[C].ProfitLossPercent#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsStocks[C].ProfitLoss$ $$TAP[1].Entities[1].DetailedPositionsStocks[C].RefCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsStocks[C].Weight$$%IF "##TAP[1].Entities[1].DetailedPositionsStocks[C].Weight#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsStocks[C].CategoryWeight$&nbsp;%<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsStocks[C].RegionClass$<br>$$TAP[1].Entities[1].DetailedPositionsStocks[C].Sector$<br></td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%endif$

$%if TAP[1].Entities[1].DetailedPositionsFixedIncome[C].lastInstance() > 0$
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Fixed Income
  $%else$
  Titres à revenu fixe
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portfolio Name<br>Portfolio Code</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Instrument<br>Code</b></td>
  	<td bgcolor="DBE5F1"><b>Nominal</b></td>
  	<td bgcolor="DBE5F1"><b>Currency<br>Quote&nbsp;Date</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Quote<br>Cost&nbsp;Quote</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Exchange&nbsp;Rate<br>Cost&nbsp;Exch.&nbsp;Rate</b><br></td>
  	<td bgcolor="DBE5F1"><b>Accrued&nbsp;Interest<br>Next &nbsp;Coupon</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Market&nbsp;Value<br>Cost&nbsp;Value</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Profit&nbsp;/&nbsp;Loss&nbsp;%<br>Profit&nbsp;/&nbsp;Loss</b><br></td>
  	<td bgcolor="DBE5F1"><b>Weight<br>Category&nbsp;Weight</b><br></td>
  	<td bgcolor="DBE5F1"><b>Maturity<br>Duration</b><br></td>
  	<td bgcolor="DBE5F1"><b>Rating<br>YTM</b><br></td>
    $%else$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Code de<br>l'instrument</b></td>
    <td bgcolor="DBE5F1"><b>Nominal</b></td>
    <td bgcolor="DBE5F1"><b>Devise<br>Date&nbsp;du&nbsp;cours</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Cours<br>Coût&nbsp;du&nbsp;cours</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Taux&nbsp;de&nbsp;change<br>Coût&nbsp;du&nbsp;taux&nbsp;de&nbsp;change</b><br></td>
    <td bgcolor="DBE5F1"><b>Intérêts&nbsp;courus<br>Prochain&nbsp;coupon</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Valeur&nbsp;de&nbsp;marché<br>Coût&nbsp;d'achat</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Gain&nbsp;/&nbsp;Perte&nbsp;%<br>Gain&nbsp;/&nbsp;Perte</b><br></td>
    <td bgcolor="DBE5F1"><b>Pondération<br>Catégorie&nbsp;de&nbsp;pondération</b><br></td>
    <td bgcolor="DBE5F1"><b>Echéance<br>Duration</b><br></td>
    <td bgcolor="DBE5F1"><b>Annotation<br>Rendement à échéance</b><br></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].DetailedPositionsFixedIncome[C].lastInstance()$
  <tr>
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].PortfolioName$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].Portfolio$</td>
	$%endif$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].Denomination$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].Code$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].Quantity$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].Ccy$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].MarketPriceDate$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].MarketPrice$ $$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].MarketPriceCcy$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].CostPrice$ $$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].MarketPriceCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].FXRate$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].CostFXRate$<br></td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].AccruedInterest$ $$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].RefCcy$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].NextCouponDate$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].ValueValCcy$ $$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].RefCcy$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].CostValCcy$ $$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].RefCcy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].ProfitLossPercent$$%IF "##TAP[1].Entities[1].DetailedPositionsFixedIncome[C].ProfitLossPercent#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].ProfitLoss$ $$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].RefCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].Weight$$%IF "##TAP[1].Entities[1].DetailedPositionsFixedIncome[C].Weight#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].CategoryWeight$&nbsp;%<br></td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].MaturityDate$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].Duration$<br></td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].RatingClass$<br>$$TAP[1].Entities[1].DetailedPositionsFixedIncome[C].YTM$<br></td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%endif$

$%if TAP[1].Entities[1].DetailedPositionsFunds[C].lastInstance() > 0$
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Funds
  $%else$
  Fonds
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portfolio Name<br>Portfolio Code</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Instrument<br>Code</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Quantity</b></td>
    <td bgcolor="DBE5F1"><b>Currency<br>Quote&nbsp;Date</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Quote<br>Cost&nbsp;Quote</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Exchange&nbsp;Rate<br>Cost&nbsp;Exch.&nbsp;Rate</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Market&nbsp;Value<br>Cost&nbsp;Value</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Profit&nbsp;/&nbsp;Loss&nbsp;%<br>Profit&nbsp;/&nbsp;Loss</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Weight<br>Category&nbsp;Weight</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Region<br>Type</b><br></td>
    $%else$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Code de<br>l'instrument</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Quantité</b></td>
    <td bgcolor="DBE5F1"><b>Devise<br>Date&nbsp;du&nbsp;cours</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Cours<br>Coût&nbsp;du&nbsp;cours</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Taux&nbsp;de&nbsp;change<br>Coût&nbsp;du&nbsp;taux&nbsp;de&nbsp;change</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Valeur&nbsp;de&nbsp;marché<br>Coût&nbsp;d'achat</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Gain&nbsp;/&nbsp;Perte&nbsp;%<br>Gain&nbsp;/&nbsp;Perte</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Pondération<br>Catégorie&nbsp;de&nbsp;pondération</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Région<br>Type</b><br></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].DetailedPositionsFunds[C].lastInstance()$
  <tr>
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFunds[C].PortfolioName$<br>$$TAP[1].Entities[1].DetailedPositionsFunds[C].Portfolio$</td>
	$%endif$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFunds[C].Denomination$<br>$$TAP[1].Entities[1].DetailedPositionsFunds[C].Code$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFunds[C].Quantity$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsFunds[C].Ccy$<br>$$TAP[1].Entities[1].DetailedPositionsFunds[C].MarketPriceDate$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFunds[C].MarketPrice$ $$TAP[1].Entities[1].DetailedPositionsFunds[C].MarketPriceCcy$<br>$$TAP[1].Entities[1].DetailedPositionsFunds[C].CostPrice$ $$TAP[1].Entities[1].DetailedPositionsFunds[C].MarketPriceCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFunds[C].FXRate$<br>$$TAP[1].Entities[1].DetailedPositionsFunds[C].CostFXRate$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFunds[C].ValueValCcy$ $$TAP[1].Entities[1].DetailedPositionsFunds[C].RefCcy$<br>$$TAP[1].Entities[1].DetailedPositionsFunds[C].CostValCcy$ $$TAP[1].Entities[1].DetailedPositionsFunds[C].RefCcy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFunds[C].ProfitLossPercent$$%IF "##TAP[1].Entities[1].DetailedPositionsFunds[C].ProfitLossPercent#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsFunds[C].ProfitLoss$ $$TAP[1].Entities[1].DetailedPositionsFunds[C].RefCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFunds[C].Weight$$%IF "##TAP[1].Entities[1].DetailedPositionsFunds[C].Weight#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsFunds[C].CategoryWeight$$%IF "##TAP[1].Entities[1].DetailedPositionsFunds[C].CategoryWeight#" != ""$&nbsp;%$%ENDIF$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsFunds[C].RegionClass$<br>$$TAP[1].Entities[1].DetailedPositionsFunds[C].Type$<br></td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%endif$

$%if TAP[1].Entities[1].DetailedPositionsDerivatives[C].lastInstance() > 0$
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Derivatives
  $%else$
  Dérivés
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portfolio Name<br>Portfolio Code</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Instrument<br>Code</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Quantity<br>Contract&nbsp;Size</b></td>
    <td bgcolor="DBE5F1"><b>Currency<br>Quote&nbsp;Date</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Quote<br>Cost&nbsp;Quote</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Exchange&nbsp;Rate<br>Cost&nbsp;Exch.&nbsp;Rate</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Market&nbsp;Value<br>Cost&nbsp;Value</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Profit&nbsp;/&nbsp;Loss&nbsp;%<br>Profit&nbsp;/&nbsp;Loss</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Weight<br>Category&nbsp;Weight</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Maturity<br>Strike Price</b><br></td>
    <td bgcolor="DBE5F1"><b>Underlying</b></td>
    $%else$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Code de<br>l'instrument</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Quantité<br>Taille du contrat</b></td>
    <td bgcolor="DBE5F1"><b>Devise<br>Date&nbsp;du&nbsp;cours</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Cours<br>Coût&nbsp;du&nbsp;cours</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Taux&nbsp;de&nbsp;change<br>Coût&nbsp;du&nbsp;taux&nbsp;de&nbsp;change</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Valeur&nbsp;de&nbsp;marché<br>Coût&nbsp;d'achat</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Gain&nbsp;/&nbsp;Perte&nbsp;%<br>Gain&nbsp;/&nbsp;Perte</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Pondération<br>Catégorie&nbsp;de&nbsp;pondération</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Echéance<br>Prix d'exercice</b><br></td>
    <td bgcolor="DBE5F1"><b>Sous-jacent</b></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].DetailedPositionsDerivatives[C].lastInstance()$
  <tr>
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].PortfolioName$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].Portfolio$</td>
	$%endif$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].Denomination$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].Code$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].Quantity$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].ContractSize$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].Ccy$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].MarketPriceDate$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].MarketPrice$ $$TAP[1].Entities[1].DetailedPositionsDerivatives[C].MarketPriceCcy$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].CostPrice$ $$TAP[1].Entities[1].DetailedPositionsDerivatives[C].MarketPriceCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].FXRate$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].CostFXRate$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].ValueValCcy$ $$TAP[1].Entities[1].DetailedPositionsDerivatives[C].RefCcy$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].CostValCcy$ $$TAP[1].Entities[1].DetailedPositionsDerivatives[C].RefCcy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].ProfitLossPercent$$%IF "##TAP[1].Entities[1].DetailedPositionsDerivatives[C].ProfitLossPercent#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].ProfitLoss$ $$TAP[1].Entities[1].DetailedPositionsDerivatives[C].RefCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].Weight$$%IF "##TAP[1].Entities[1].DetailedPositionsDerivatives[C].Weight#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].CategoryWeight$&nbsp;%<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].MaturityDate$<br>$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].StrikePrice$ $$TAP[1].Entities[1].DetailedPositionsDerivatives[C].StrikePriceCcy$<br></td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsDerivatives[C].Underlying$<br></td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%endif$

$%if TAP[1].Entities[1].DetailedPositionsLiquidity[C].lastInstance() > 0$
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Liquidity
  $%else$
  Liquidité
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portfolio Name<br>Portfolio Code</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Account</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Balance</b></td>
    <td bgcolor="DBE5F1"><b>Currency</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Exchange&nbsp;Rate<br>Cost&nbsp;Exch.&nbsp;Rate</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Market&nbsp;Value<br>Cost&nbsp;Value</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Profit&nbsp;/&nbsp;Loss&nbsp;%<br>Profit&nbsp;/&nbsp;Loss</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Weight<br>Category&nbsp;Weight</b><br></td>
    $%else$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Compte</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Solde</b></td>
    <td bgcolor="DBE5F1"><b>Devise</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Taux&nbsp;de&nbsp;change<br>Coût&nbsp;du&nbsp;taux&nbsp;de&nbsp;change</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Valeur&nbsp;de&nbsp;marché<br>Coût&nbsp;d'achat</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Gain&nbsp;/&nbsp;Perte&nbsp;%<br>Gain&nbsp;/&nbsp;Perte</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Pondération<br>Catégorie&nbsp;de&nbsp;pondération</b><br></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].DetailedPositionsLiquidity[C].lastInstance()$
  <tr>
    $%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].PortfolioName$<br>$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].Portfolio$</td>
	$%endif$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].Code$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].Quantity$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].Ccy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].FXRate$<br>$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].CostFXRate$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].ValueValCcy$ $$TAP[1].Entities[1].DetailedPositionsLiquidity[C].RefCcy$<br>$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].CostValCcy$ $$TAP[1].Entities[1].DetailedPositionsLiquidity[C].RefCcy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].ProfitLossPercent$$%IF "##TAP[1].Entities[1].DetailedPositionsLiquidity[C].ProfitLossPercent#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].ProfitLoss$ $$TAP[1].Entities[1].DetailedPositionsLiquidity[C].RefCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].Weight$$%IF "##TAP[1].Entities[1].DetailedPositionsLiquidity[C].Weight#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsLiquidity[C].CategoryWeight$&nbsp;%<br></td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%endif$

$%if TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].lastInstance() > 0$
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Money Market
  $%else$
  Marché monétaire
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portfolio Name<br>Portfolio Code</b></td>
	$%endif$
    <td bgcolor="DBE5F1" align="right"><b>Contract<br>Contract Type</b></td>
    <td bgcolor="DBE5F1"><b>Balance</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Exchange&nbsp;Rate<br>Cost&nbsp;Exch.&nbsp;Rate</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Accrued&nbsp;Interest<br>Maturity</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Market&nbsp;Value<br>Cost&nbsp;Value</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Profit&nbsp;/&nbsp;Loss&nbsp;%<br>Profit&nbsp;/&nbsp;Loss</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Weight<br>Category&nbsp;Weight</b><br></td>
    $%else$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
	$%endif$
    <td bgcolor="DBE5F1" align="right"><b>Contrat<br>Type de l'instrument</b></td>
    <td bgcolor="DBE5F1"><b>Solde</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Taux&nbsp;de&nbsp;change<br>Coût&nbsp;du&nbsp;taux&nbsp;de&nbsp;change</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Intérêts&nbsp;courus<br>Echéance</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Valeur&nbsp;de&nbsp;marché<br>Coût&nbsp;d'achat</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Gain&nbsp;/&nbsp;Perte&nbsp;%<br>Gain&nbsp;/&nbsp;Perte</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Pondération<br>Catégorie&nbsp;de&nbsp;pondération</b><br></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].lastInstance()$
  <tr>
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].PortfolioName$<br>$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].Portfolio$</td>
	$%endif$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].Code$<br>$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].ContractType$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].Quantity$ $$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].Ccy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].FXRate$<br>$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].CostFXRate$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].AccruedInterest$ $$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].Ccy$<br>$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].MaturityDate$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].ValueValCcy$ $$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].RefCcy$<br>$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].CostValCcy$ $$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].RefCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].ProfitLossPercent$$%IF "##TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].ProfitLossPercent#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].ProfitLoss$ $$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].RefCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].Weight$$%IF "##TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].Weight#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsMoneyMarket[C].CategoryWeight$&nbsp;%<br></td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%endif$

$%if TAP[1].Entities[1].DetailedPositionsForwards[C].lastInstance() > 0$
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Forwards
  $%else$
  Contrats à terme
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portfolio Name<br>Portfolio Code</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Contract</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Amount Bought<br>Amount Sold</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Quote<br>Cost quote</b><br></td>
    <td bgcolor="DBE5F1"><b>Maturity</b></td>
    <td bgcolor="DBE5F1"><b>Market&nbsp;Value</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Weight<br>Category&nbsp;Weight</b><br></td>
    $%else$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
	$%endif$
    <td bgcolor="DBE5F1"><b>Contrat</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Montant acheté<br>Montant vendu</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Cours<br>Coût du cours</b><br></td>
    <td bgcolor="DBE5F1"><b>Echéance</b></td>
    <td bgcolor="DBE5F1"><b>Valeur de marché</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Pondération<br>Catégorie&nbsp;de&nbsp;pondération</b><br></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].DetailedPositionsForwards[C].lastInstance()$
  <tr>
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsForwards[C].PortfolioName$<br>$$TAP[1].Entities[1].DetailedPositionsForwards[C].Portfolio$</td>
	$%endif$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsForwards[C].Code$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsForwards[C].Quantity$ $$TAP[1].Entities[1].DetailedPositionsForwards[C].Ccy$<br>$$TAP[1].Entities[1].DetailedPositionsForwards[C].CounterpartQty$ $$TAP[1].Entities[1].DetailedPositionsForwards[C].CounterpartCcy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsForwards[C].MarketPrice$<br>$$TAP[1].Entities[1].DetailedPositionsForwards[C].CostPrice$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsForwards[C].MaturityDate$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsForwards[C].ValueValCcy$ $$TAP[1].Entities[1].DetailedPositionsForwards[C].RefCcy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsForwards[C].Weight$&nbsp;%<br>$$TAP[1].Entities[1].DetailedPositionsForwards[C].CategoryWeight$&nbsp;%<br></td>
  </tr>
$%endfor$
</table>
<br>
<br>
$%endif$

$%if TAP[1].Entities[1].DetailedPositionsOthers[C].lastInstance() > 0$
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Others
  $%else$
  Autres
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
  <tr>
    $%if LANGUAGE_MAP_ALIAS == 'English'$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portfolio Name<br>Portfolio Code</b></td>
	$%endif$
    <td bgcolor="DBE5F1" align="right"><b>Instrument<br>Code</b></td>
  	<td bgcolor="DBE5F1"><b>Quantity</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Currency<br>Quote Date</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Quote<br>Cost Quote</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Exchange&nbsp;Rate<br>Cost&nbsp;Exch.&nbsp;Rate</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Market&nbsp;Value<br>Cost&nbsp;Value</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Profit&nbsp;/&nbsp;Loss&nbsp;%<br>Profit&nbsp;/&nbsp;Loss</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Weight<br>Category&nbsp;Weight</b><br></td>
    $%else$
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
	<td bgcolor="DBE5F1"><b>Portefeuille<br>Code du portefeuille</b></td>
	$%endif$
    <td bgcolor="DBE5F1" align="right"><b>Code de<br>l'instrument</b></td>
    <td bgcolor="DBE5F1"><b>Quantité</b></td>
    <td bgcolor="DBE5F1" align="right"><b>Devise<br>Date&nbsp;du&nbsp;cours</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Cours<br>Coût&nbsp;du&nbsp;cours</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Taux&nbsp;de&nbsp;change<br>Coût&nbsp;du&nbsp;taux&nbsp;de&nbsp;change</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Valeur&nbsp;de&nbsp;marché<br>Coût&nbsp;d'achat</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Gain&nbsp;/&nbsp;Perte&nbsp;%<br>Gain&nbsp;/&nbsp;Perte</b><br></td>
    <td bgcolor="DBE5F1" align="right"><b>Pondération<br>Catégorie&nbsp;de&nbsp;pondération</b><br></td>
    $%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].DetailedPositionsOthers[C].lastInstance()$
  <tr>
	$%if TAP[1].WorkingElements[1].ShowPortfolioHierarchy == '1'$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsOthers[C].PortfolioName$<br>$$TAP[1].Entities[1].DetailedPositionsOthers[C].Portfolio$</td>
	$%endif$
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].DetailedPositionsOthers[C].Denomination$<br>$$TAP[1].Entities[1].DetailedPositionsOthers[C].Code$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsOthers[C].Quantity$</td>
    <td bgcolor="F0F0F0" align="left">$$TAP[1].Entities[1].DetailedPositionsOthers[C].Ccy$<br>$$TAP[1].Entities[1].DetailedPositionsOthers[C].MarketPriceDate$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsOthers[C].MarketPrice$ $$TAP[1].Entities[1].DetailedPositionsOthers[C].MarketPriceCcy$<br>$$TAP[1].Entities[1].DetailedPositionsOthers[C].CostPrice$ $$TAP[1].Entities[1].DetailedPositionsOthers[C].MarketPriceCcy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsOthers[C].FXRate$<br>$$TAP[1].Entities[1].DetailedPositionsOthers[C].CostFXRate$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsOthers[C].ValueValCcy$ $$TAP[1].Entities[1].DetailedPositionsOthers[C].RefCcy$<br>$$TAP[1].Entities[1].DetailedPositionsOthers[C].CostValCcy$ $$TAP[1].Entities[1].DetailedPositionsOthers[C].RefCcy$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsOthers[C].ProfitLossPercent$$%IF "##TAP[1].Entities[1].DetailedPositionsOthers[C].ProfitLossPercent#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsOthers[C].ProfitLoss$ $$TAP[1].Entities[1].DetailedPositionsOthers[C].RefCcy$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].DetailedPositionsOthers[C].Weight$$%IF "##TAP[1].Entities[1].DetailedPositionsOthers[C].Weight#" != ""$&nbsp;%$%ENDIF$<br>$$TAP[1].Entities[1].DetailedPositionsOthers[C].CategoryWeight$&nbsp;%<br></td>
  </tr>
$%endfor$
</table>
$%endif$

</pdf>