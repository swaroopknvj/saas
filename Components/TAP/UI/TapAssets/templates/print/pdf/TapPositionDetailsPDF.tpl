<pdf baseFont="Helvetica,Cp1252,false">
<!-- Position Details -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
	$%if LANGUAGE_MAP_ALIAS == 'English'$
	Position Details - Transactions
	$%else$
	Positions détaillées - Transactions
	$%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Instrument Code<br>Instr. Denom.</b></td>
		<td bgcolor="DBE5F1"><b>Portfolio Name<br>Nature</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantity<br>Price + Currency</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Exchange rate</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Net Amount (Op. Ccy.)<br>Net Amount (Ref. Ccy.)</b></td>
		<td bgcolor="DBE5F1"><b>Trade date<br>Value Date</b></td>	
		<td bgcolor="DBE5F1"><b>Accounting Date</b></td>
		<td bgcolor="DBE5F1"><b>Account Code<br>Account Denom</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Fees And Taxes (Op. Ccy.)<br>Gross Amount (Op. Ccy.)</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Accrued Int. (Op. Ccy.)<br>Operation Code</b></td>
		$%else$
		<td bgcolor="DBE5F1"><b>Code de l'instrument<br>Description de l'instrument</b></td>
		<td bgcolor="DBE5F1"><b>Portefeuille<br>Nature</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantité<br>Prix + Devise</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Taux de change</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Montant net (devise de l'op.)<br>Montant net (devise de ref.)</b></td>
		<td bgcolor="DBE5F1"><b>Date de transaction<br>Date de valeur</b></td>
		<td bgcolor="DBE5F1"><b>Date de comptabilisation</b></td>
		<td bgcolor="DBE5F1"><b>Code du compte<br>Compte</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Frais et taxes (devise de l'op.)<br>Montant brut (devise de l'op.)</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Int cour (devise de l'op.)<br>Code de l'opération</b></td>
		$%endif$
	 </tr>
$%if TAP[1].Entities[1].RecentOperationsHistory[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].RecentOperationsHistory[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].RecentOperationsHistory[C].InstrCode$<br>$$TAP[1].Entities[1].RecentOperationsHistory[C].InstrDenom$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].RecentOperationsHistory[C].PortfolioName$<br>$$TAP[1].Entities[1].RecentOperationsHistory[C].OperationNature$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RecentOperationsHistory[C].Quantity$<br>$$TAP[1].Entities[1].RecentOperationsHistory[C].Price$ $$TAP[1].Entities[1].RecentOperationsHistory[C].PriceCcy$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RecentOperationsHistory[C].ExchRate$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RecentOperationsHistory[C].NetAmountOperCurr$ $$TAP[1].Entities[1].RecentOperationsHistory[C].PosCcy$<br>$$TAP[1].Entities[1].RecentOperationsHistory[C].NetAmountRefCurr$ $$TAP[1].Entities[1].RecentOperationsHistory[C].RefCcy$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].RecentOperationsHistory[C].OperationDate$<br>$$TAP[1].Entities[1].RecentOperationsHistory[C].ValueDate$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].RecentOperationsHistory[C].AccountingDate$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].RecentOperationsHistory[C].AccountCode$<br>$$TAP[1].Entities[1].RecentOperationsHistory[C].Account$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RecentOperationsHistory[C].FeesAndTaxes$ $$TAP[1].Entities[1].RecentOperationsHistory[C].PosCcy$<br>$$TAP[1].Entities[1].RecentOperationsHistory[C].GrossAmountOperCurr$ $$TAP[1].Entities[1].RecentOperationsHistory[C].PosCcy$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].RecentOperationsHistory[C].AccruedInterest$ $$TAP[1].Entities[1].RecentOperationsHistory[C].PosCcy$<br>$$TAP[1].Entities[1].RecentOperationsHistory[C].OperationCode$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="10">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="10">Aucun résultat trouvé</td>
		$%endif$ 
    </tr>
$%endif$
</table>
</pdf>