$%if LANGUAGE_MAP_ALIAS == 'English'$Account Movements$%else$Mouvements de compte$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Account Code,Account$%else$Code du compte,Compte$%endif$
$$TAP[1].WorkingElements[1].TAPSelectedAccount[1].CurrentSessionAccount$,$$TAP[1].WorkingElements[1].TAPSelectedAccount[1].CurrentSessionAccountName$

$%if LANGUAGE_MAP_ALIAS == 'English'$Trade Date,Debit/Credit,Op. Ccy.,Account Balance,Op. Ccy.,Nature,Instrument Code,Instrument,Quantity,Price,Currency,Value Date,Accounting Date,Operation Code,Reversal$%else$Date de transaction,Débit/crédit,Devise de l'op.,Solde du compte,Devise de l'op.,Nature,Code de l'instrument,Instrument,Quantité,Prix,Devise,Date de valeur,Date de comptabilisation,Code de l'opération,Extourne$%endif$$%if TAP[1].Entities[1].AccountOperations[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].AccountOperations[C].lastInstance()$$$TAP[1].Entities[1].AccountOperations[C].OperationDate$,$$TAP[1].Entities[1].AccountOperations[C].DebitCredit$,$$TAP[1].Entities[1].AccountOperations[C].PosCurrency$,$$TAP[1].Entities[1].AccountOperations[C].AccountBalance$,$$TAP[1].Entities[1].AccountOperations[C].PosCurrency$,$$TAP[1].Entities[1].AccountOperations[C].OperationNature$,"$$TAP[1].Entities[1].AccountOperations[C].InstrCode$","$$TAP[1].Entities[1].AccountOperations[C].InstrDenom$",$$TAP[1].Entities[1].AccountOperations[C].Quantity$,$$TAP[1].Entities[1].AccountOperations[C].Price$$%if TAP[1].Entities[1].AccountOperations[C].PriceCcy == '%'$ %$%endif$,$%if TAP[1].Entities[1].AccountOperations[C].PriceCcy != '%'$$$TAP[1].Entities[1].AccountOperations[C].PriceCcy$$%endif$,$$TAP[1].Entities[1].AccountOperations[C].ValueDate$,$$TAP[1].Entities[1].AccountOperations[C].AccountingDate$,$$TAP[1].Entities[1].AccountOperations[C].OperationCode$,$$TAP[1].Entities[1].AccountOperations[C].ReversalLabel$
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$ 
$%endif$

