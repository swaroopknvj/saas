<pdf baseFont="Helvetica,Cp1252,false">
<!-- Account Operations -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Account Movements
  $%else$
  Mouvements de compte
  $%endif$
</font>
<br>
<br>
<font size="8">
<table width="100%" border="0" bordercolor="ffffff" cellspacing="2" cellpadding="4">
	<tr>
	    $%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="95B3D7"><b>Account Code</b></td>
		$%else$
		<td bgcolor="95B3D7"><b>Code du compte</b></td>
		$%endif$
		<td bgcolor="95B3D7">$$TAP[1].WorkingElements[1].TAPSelectedAccount[1].CurrentSessionAccount$</td>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="95B3D7"><b>Account</b></td>
		$%else$
		<td bgcolor="95B3D7"><b>Compte</b></td>
		$%endif$
		<td bgcolor="95B3D7">$$TAP[1].WorkingElements[1].TAPSelectedAccount[1].CurrentSessionAccountName$</td>
	</tr>
</table>
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Trade Date</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Debit/Credit</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Account Balance</b></td>
		<td bgcolor="DBE5F1"><b>Nature</b></td>
		<td bgcolor="DBE5F1"><b>Instrument Code</b></td>
		<td bgcolor="DBE5F1"><b>Instrument</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantity</b></td>
		<td bgcolor="DBE5F1"align="right"><b>Price</b><br></td>
		<td bgcolor="DBE5F1"><b>Value Date</b></td>
		<td bgcolor="DBE5F1"><b>Accounting Date</b></td>
		<td bgcolor="DBE5F1"><b>Operation Code</b></td>
		<td bgcolor="DBE5F1"><b>Reversal</b></td>
		$%else$
		<td bgcolor="DBE5F1"><b>Date de transaction</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Débit/crédit</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Solde du compte</b></td>
		<td bgcolor="DBE5F1"><b>Nature</b></td>
		<td bgcolor="DBE5F1"><b>Code de l'instrument</b></td>
		<td bgcolor="DBE5F1"><b>Instrument</b><br></td>
		<td bgcolor="DBE5F1" align="right"><b>Quantité</b></td>
		<td bgcolor="DBE5F1" align="right"><b>Prix</b><br></td>
		<td bgcolor="DBE5F1"><b>Date de valeur</b></td>
		<td bgcolor="DBE5F1"><b>Date de comptabilisation</b></td>
		<td bgcolor="DBE5F1"><b>Code de l'opération</b></td>
		<td bgcolor="DBE5F1"><b>Extourne</b></td>
		$%endif$
	 </tr>
$%if TAP[1].Entities[1].AccountOperations[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].AccountOperations[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AccountOperations[C].OperationDate$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AccountOperations[C].DebitCredit$ $$TAP[1].Entities[1].AccountOperations[C].PosCurrency$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AccountOperations[C].AccountBalance$ $$TAP[1].Entities[1].AccountOperations[C].PosCurrency$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AccountOperations[C].OperationNature$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AccountOperations[C].InstrCode$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AccountOperations[C].InstrDenom$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AccountOperations[C].Quantity$</td>
		<td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].AccountOperations[C].Price$ $$TAP[1].Entities[1].AccountOperations[C].PriceCcy$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AccountOperations[C].ValueDate$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AccountOperations[C].AccountingDate$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AccountOperations[C].OperationCode$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].AccountOperations[C].ReversalLabel$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="12">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="12">Aucun résultat trouvé</td>
		$%endif$ 
    </tr>
$%endif$
</table>
</pdf>