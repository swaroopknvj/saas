$%if LANGUAGE_MAP_ALIAS == 'English'$External Banks$%else$External Banks$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Code,Name,Denomination,Tax Country$%else$Code,Name,Denomination,Tax Country$%endif$
$%if TAP[1].Entities[1].Search[1].ExternalBanks[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].Search[1].ExternalBanks[C].lastInstance()$"$$TAP[1].Entities[1].Search[1].ExternalBanks[C].code$","$$TAP[1].Entities[1].Search[1].ExternalBanks[C].name$","$$TAP[1].Entities[1].Search[1].ExternalBanks[C].extDenomination$","$$TAP[1].Entities[1].Search[1].ExternalBanks[C].taxGeoExtDenomination$
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$No results found$%endif$
$%endif$