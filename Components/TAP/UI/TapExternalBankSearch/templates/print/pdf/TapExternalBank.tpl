<pdf baseFont="Helvetica,Cp1252,false">
<!-- Pending Orders -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  External Banks
  $%else$
  External Banks
  $%endif$
</font>
<br>
<br>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Code</b></td>
		<td bgcolor="DBE5F1"><b>Name</b></td>
		<td bgcolor="DBE5F1"><b>Denomination</b></td>
		<td bgcolor="DBE5F1"><b>Tax Country</b></td>
		
		$%else$
		<td bgcolor="DBE5F1"><b>Code</b></td>
		<td bgcolor="DBE5F1"><b>Name</b></td>
		<td bgcolor="DBE5F1"><b>Denomination</b></td>
		<td bgcolor="DBE5F1"><b>Tax Country</b></td>
		$%endif$
	</tr>
$%if TAP[1].Entities[1].Search[1].ExternalBanks[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].Search[1].ExternalBanks[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Search[1].ExternalBanks[C].code$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Search[1].ExternalBanks[C].name$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Search[1].ExternalBanks[C].extDenomination$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].Search[1].ExternalBanks[C].taxGeoExtDenominationt$</td>
		
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="14">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="14">No results found</td>
		$%endif$
	</tr>
$%endif$
</table>
</pdf>