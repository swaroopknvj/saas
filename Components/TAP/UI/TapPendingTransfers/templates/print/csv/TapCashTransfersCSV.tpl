$%if LANGUAGE_MAP_ALIAS == 'English'$Cash Transfers$%else$Cash Transfers$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Portfolio Name,Transfer Code,Debit Account,Credit Account,Transfer Type,Status,Net Amount$%else$Nom du portefeuille,Code du transfert,Compte à débiter,Compte à créditer,Type de transfert,Statut,Montant net$%endif$
$%if TAP[1].Entities[1].PendingTransfers[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].PendingTransfers[C].lastInstance()$"$$TAP[1].Entities[1].PendingTransfers[C].PortfolioName$","$$TAP[1].Entities[1].PendingTransfers[C].TransferCode$","$$TAP[1].Entities[1].PendingTransfers[C].DebitAccount$","$$TAP[1].Entities[1].PendingTransfers[C].CreditAccount$","$$TAP[1].Entities[1].PendingTransfers[C].TransferType$","$$TAP[1].Entities[1].PendingTransfers[C].Status$","$$TAP[1].Entities[1].PendingTransfers[C].NetAmount$"
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$