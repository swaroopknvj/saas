$%if LANGUAGE_MAP_ALIAS == 'English'$Securities Transfers$%else$Transferts de titres$%endif$

$%if LANGUAGE_MAP_ALIAS == 'English'$Portfolio Name,Transfer Code,Instrument Code,Transfer Type,Status,Quantity,Net Amount$%else$Nom du portefeuille,Code du transfert,Code de l’instrument,Type de transfert,Statut,Quantité,Montant net$%endif$
$%if TAP[1].Entities[1].PendingTransfers[C].lastInstance() > 0$$%for 1 to TAP[1].Entities[1].PendingTransfers[C].lastInstance()$"$$TAP[1].Entities[1].PendingTransfers[C].PortfolioName$","$$TAP[1].Entities[1].PendingTransfers[C].TransferCode$","$$TAP[1].Entities[1].PendingTransfers[C].InstrumentCode$","$$TAP[1].Entities[1].PendingTransfers[C].TransferType$","$$TAP[1].Entities[1].PendingTransfers[C].Status$",$$TAP[1].Entities[1].PendingTransfers[C].Quantity$,$$TAP[1].Entities[1].PendingTransfers[C].NetAmount$
$%endfor$
$%else$
$%if LANGUAGE_MAP_ALIAS == 'English'$No results found$%else$Aucun résultat trouvé$%endif$
$%endif$