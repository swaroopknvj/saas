<pdf baseFont="Helvetica,Cp1252,false">
<!-- Pending Orders -->
<page size='A4' orientation='landscape'>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/brand/TemenosLogoSmall.jpg" width="100" height="35"></img>
<center>
<br>
<br>
<font size="15">
  $%if LANGUAGE_MAP_ALIAS == 'English'$
  Cash Transfers
  $%else$
  Cash Transfers
  $%endif$
</font>
<br>
<br>
<br>
<br>
<font size="8">
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2" lastHeaderRow="0" cellsfitpage="true">
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="DBE5F1"><b>Portfolio Name</b></td>
		<td bgcolor="DBE5F1"><b>Transfer Code</b></td>
		<td bgcolor="DBE5F1"><b>Debit Account</b></td>
		<td bgcolor="DBE5F1"><b>Credit Account</b></td>
		<td bgcolor="DBE5F1"><b>Transfer Type</b><br></td>
		<td bgcolor="DBE5F1"><b>Status</b></td>
		<td bgcolor="DBE5F1"><b>Net Amount</b></td>
		$%else$
		<td bgcolor="DBE5F1"><b>Nom du portefeuille</b></td>
		<td bgcolor="DBE5F1"><b>Code du transfert</b></td>
		<td bgcolor="DBE5F1"><b>Compte à débiter</b></td>
		<td bgcolor="DBE5F1"><b>Compte à créditer</b></td>
		<td bgcolor="DBE5F1"><b>Type de transfert</b><br></td>
		<td bgcolor="DBE5F1"><b>Statut</b><br></td>
		<td bgcolor="DBE5F1"><b>Montant net</b></td>
		$%endif$
	</tr>
$%if TAP[1].Entities[1].PendingTransfers[C].lastInstance() > 0$
$%for 1 to TAP[1].Entities[1].PendingTransfers[C].lastInstance()$
	<tr>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].PendingTransfers[C].PortfolioName$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].PendingTransfers[C].TransferCode$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].PendingTransfers[C].DebitAccount$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].PendingTransfers[C].CreditAccount$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].PendingTransfers[C].TransferType$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].PendingTransfers[C].Status$</td>
		<td bgcolor="F0F0F0">$$TAP[1].Entities[1].PendingTransfers[C].NetAmount$</td>
	</tr>
$%endfor$
$%else$
	<tr>
		$%if LANGUAGE_MAP_ALIAS == 'English'$
		<td bgcolor="F0F0F0" colspan="14">No results found</td>
		$%else$
		<td bgcolor="F0F0F0" colspan="14">Aucun résultat trouvé</td>
		$%endif$
	</tr>
$%endif$
</table>
</pdf>