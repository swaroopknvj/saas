/*
 * $RCSfile: popup.js,v $
 * $Author: alan.heath $
 * $Revision: 1.7 $
 * $Date: 2012/01/06 16:42:14 $
 *
 * Copyright (c) 2001-2006 edge IPK Limited, All rights reserved.
 *
 * This source code is protected by copyright laws and international copyright treaties,
 * as well as other intellectual property laws and treaties.
 *  
 * Access to, alteration, duplication or redistribution of this source code in any form 
 * is not permitted without the prior written authorisation of edge IPK.
 * 
 */
 
var timeout = 2000;

var skipcycle = false;

function cancelCallerPopup(ns, pageno, pageKey, pageValue, event)
{
  if (!getVariable(ns, 'hasSubmitted') && (!event.clientX || event.clientX < -3000 || event.clientY < 0))
  {
	  try{
		window.opener.cancelpopup(ns, pageno, pageKey, pageValue, "CANCELPOPUP");
	  }
	  catch (e){}
	  try{
		window.close();
	  }
	  catch (e){}	  
  }
}


function fcsOnMe()
{
  if (!skipcycle)
  {
    window.focus();
  }
  mytimer = setTimeout('fcsOnMe()', timeout);
}