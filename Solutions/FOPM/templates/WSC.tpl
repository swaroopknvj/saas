$%if COMPONENT_ID_PREFIX != ""$
    $$FORMCONTENT$
$%else$
$%if PRESENTATIONTYPE != Portlet || IS_RUNPREVIEW == "Y"$
<!DOCTYPE html>
  <html dir="$$PRESENTATIONTYPE.LAYOUTDIRECTION$" class="$$!FOOTER_LOCKED$ $$!HEADER_LOCKED$ $$!BOX_VIEW$ $$!BOX_VIEW_PM$ $$!FULL_SCREEN$ $$!SHOW_SIDEBAR$ $$!FONT_SIZE$ $$!DENSITY$ $$!HIGH_CONTRAST$">
    <head>
$%IF !Redirect = true$
	    <meta http-equiv="refresh" content="0;URL='$$CONTEXTPATH$/$$CONTROLLING_SERVLET$?MODE=__04E7FA35F7303C78+FormButton+32&$$PAGE_KEY$=$$PAGE_VAL$&SUBSESSIONID=$$!SUBSESSIONID$'" />
$%ENDIF$
      <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
      <meta name="author" content="Temenos"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
      <title>Temenos WealthSuite</title>
      <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon-32x32.png">
      $$HEADCONTENT$
      $%IF DEVICE_INFO.isHybrid == "Y"$
      <script type="text/javascript" src="html/js/cordova/cordova_loader.js"></script>
      $%ENDIF$
      $%IF PRESENTATIONTYPE.LAYOUTDIRECTION == "rtl"$
      <link rel="stylesheet" href="html/css/T-rtl-classes.css" media="screen"/>
      $%ENDIF$
      <link id="override_css" type="text/css" rel="stylesheet" href="html/css/StyleOverridesWSC.css" media="screen"/>
	  <script type="text/javascript">
        var UserAgent = "$$HTTP_HEADER.User-Agent$";
        var LANGUAGE = "$$LANGUAGE_MAP_ALIAS$";
        var SelectedSubMenu = "$$TAP[1].WorkingElements[1].SelectedSubMenu$";
        var SelectedTab = "$$TAP[1].WorkingElements[1].SelectedSearchTabs$";
        var tSep = "$$PRESENTATIONTYPE.DIGITGROUPSYMBOL$";
        var dSep = "$$PRESENTATIONTYPE.DECIMALSYMBOL$";
        var profurl = "ClientPerformanceResult";
        var correlationId = "$%XCORRELATIONID$";
        var theme = "$$THEME$";
        var sessionTimeout = setTimeout(function(){jQuery('.SessionExpiredLogout').trigger('click');}, $$SessionTimeout$*1000-10000,true);
      </script>
      <script type="text/javascript" src="html/js/WSC.js"></script>
      <script type="text/javascript" src="html/js/WSC_addon.js"></script>
	  <script type="text/javascript" src="templates/widgets/com.temenos.widgets.jquery/chosen/chosen.jquery.min.js"></script>
      <link type="text/javascript" src="templates/widgets/com.temenos.widgets.jquery/chosen/chosen.css"/>
  </head>
  <body class="$$THEME$" data-phase="$$PHASE$">
    <div class="tc-skip tc-global-font tc-global-color normal-weight tc-tab-highlight"><a href="#EDGE_CONNECT_PHASE">Skip to Main Content</a></div>
    <div id="helpTextDiv" style="display: none;"></div>
    <div class="tc-breakpoint-hint"></div>
    $%IF PRESENTATIONTYPE != "Rich HTML"$
      <div class="tc-global-font" style="position: absolute; width: 100%; top: 100px; text-align: center; padding: 1rem">
      <span class="tc-icon icon-warning"><span></span></span>
      This template and the Temenos theme should only be used with Rich HTML presentation types. </div>
    $%ENDIF$
$%endif$
    <form id="$$NAMESPACE$form1" method="post" action="$$ACTIONURL$" onsubmit="return false;" autocomplete="off">
      <div>
        <input type="hidden" name="MODE"/>
        <input type="hidden" name="$$PAGE_KEY$" value="$$PAGE_VAL$"/>
        <input type="hidden" name="MENUSTATE"/>
        <input type="hidden" name="NAMESPACE" value="$$NAMESPACE$"/>
        <input type="hidden" name="WorkingElements[1].Navigation[1].NextPhase"/>
      </div>
      $$FORMCONTENT$
    </form>
$%if PRESENTATIONTYPE != Portlet || IS_RUNPREVIEW == "Y"$
    <form name="sessionTimeoutForm" method=POST action="servletcontroller" autocomplete="off">
      <input type="hidden" name="PRODUCT" value="">
      <input type="hidden" name="PRESENTATION_TYPE" value="">
      <input type="hidden" name="MODE" value="XX">
    </form>
    <script>
function runWidgetAjaxHooks(p_hook, ns, p_ajaxCaller, p_result){
  if(typeof(wscCallback)!='undefined') wscCallback();
  var widgetList = getVariable(ns, "WIDGET_LIST");
  var ok = true;
  for (var widget in widgetList){
    try{
      var f = getObjectByPackageString(widgetList[widget] + "." + p_hook);
      if (typeof f === 'function') {
      	ok &= f(ns, p_ajaxCaller, p_result);
      }
    }
    catch (e){
    }
  }
  return ok;
}
function beforeAjaxPOSTRequest(url, async, parameters, ns, ajaxCaller, req){
  AJAXstartstamp= new Date().valueOf();
  if(typeof TemenosLoader != 'undefined') TemenosLoader.triggerShow();
  return true;
}
function afterAjaxPOSTRequest(async, req, ajaxCaller, ns, uid) {
  if(typeof TemenosLoader != 'undefined') TemenosLoader.triggerShow();
  window.clearTimeout(sessionTimeout);
  sessionTimeout = setTimeout(function(){jQuery('.SessionExpiredLogout').trigger('click');}, $$SessionTimeout$*1000-10000);
  if(async){AJAXcompletestamp = new Date().valueOf()}
  return true;
}
/* OLD FUNCTIONALITY REMOVED
// Additions by NGA to cope with document viewing
function afterInitForm(p_namespace){
  $%IF !DownloadFileName != ''$
  $%IF !TemplateCMD = downloadDoc$
  downloadDoc();        // open a new pop-up Browser window to save the file contents
	$%ENDIF$
  $%IF !TemplateCMD = downloadInlineDoc$
  downloadInlineDoc();  // open a new pop-up Browser window to display the file contents
  $%ENDIF$
  $%ENDIF$
}
function downloadDoc(){
  try{
    openWindow=window.open('$$CONTEXTPATH$/MemoryRetrievalServlet/$$!DownloadFileName$?inline=false','','toolbar=0,location=0,left=100,top=100,width=800,height=800,resizable=yes');
    openWindow.focus();
  }	catch (e){
    alert("Either popup blocker is enabled or file cannot be downloaded for viewing at this moment.");
  }
}
function downloadInlineDoc(){
  try{
    openWindow=window.open('$$CONTEXTPATH$/MemoryRetrievalServlet/$$!DownloadFileName$?inline=true','','toolbar=0,location=0,left=100,top=100,width=800,height=800,resizable=yes');
    openWindow.focus();
  }	catch (e){
    alert("Either popup blocker is enabled or file cannot be downloaded for viewing at this moment.");
  }
}
*/
// Additions by NGA to cope with document viewing
function afterInitForm(p_namespace) {
	setFocusToFirst(p_namespace, null, null);
  $%IF !DownloadFileName != ''$
    $%IF !TemplateCMD = downloadDoc$
      downloadDoc();  // open a new pop-up Browser window to save the file contents
  	$%ENDIF$
    $%IF !TemplateCMD = downloadInlineDoc$
      downloadInlineDoc();  // open a new pop-up Browser window to display the file contents
    $%ENDIF$
  $%ENDIF$

//  closeProgress();
}
function downloadInlineDoc() {
	try{
		openWindow=window.open('$$CONTEXTPATH$/MemoryRetrievalServlet/$$!DownloadFileName$?inline=false','','toolbar=0,location=0,left=100,top=100,width=800,height=800,resizable=yes');
		openWindow.focus();
	}	catch (e){
		alert("Either popup blocker is enabled or file cannot be downloaded for viewing at this moment.");
	}
}
function downloadDoc() {
  var downloadCSV = function(content, fileName, mimeType) {
    var a = document.createElement('a');
    mimeType = mimeType || 'application/octet-stream';
    if(navigator.msSaveBlob){ // IE10
      navigator.msSaveBlob(new Blob([content], {type: mimeType}), fileName);
    }else if (URL && 'download' in a){ //html5 A[download]
      a.href = URL.createObjectURL(new Blob([content], {type: mimeType}));
      a.setAttribute('download', fileName);
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    } else {
      location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
    }
  }
  $.get('$$CONTEXTPATH$/MemoryRetrievalServlet/$$!DownloadFileName$', function(data){
//    var csvContent = data.replace(/,[=+@]/g, ',').replace(/"[=+@-]*/g, '"');
    var csvContent = data.replace(/[=+]*/g, '');
    downloadCSV(csvContent, '$$!DownloadFileName$', 'text/csv;encoding:utf-8');
  });
}
$%IF !PUSH_ENABLED != 'Y'$
  function webSocketStart(contextPath){return null;}
$%ENDIF$
    </script>
  </body>
</html>
$%endif$
$%endif$
