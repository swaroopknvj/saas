﻿function initGauge(oid){
  buildGauge(oid);
  if(IPQ[oid].slid == 'Y'){                       // ----- When using the slider with gauge
    var tid   = IPQ.cip+IPQ[oid].tid,
        t     = $('#'+tid),
        steps = $(t).find('tbody tr').length,     // number of items (rows) returned in the table
        pselv = IPQ[oid].pcode,                   // preselected code value
        pselr = 2;                                // init preselected row
    IPQ[oid].riskLevels = [];                     // init Array of risk levels from table
    $(t).find('tbody tr').each(function(i,r){     // parse rows in table
      IPQ[oid].riskLevels.push([                  // build Array of risk levels
        parseInt($(r).find('.ssriskLevel').text()), // set level as first item
        $(r).find('.ssname').text().trim(),       // set name as second item
        $(r).find('.sscode').text().trim()        // set code as third item
      ]);
      if($(r).find('.sscode').text().trim() == pselv) pselr = i+1;    // set preselected row when code equals preselected code
    });

    var slider = $('#'+oid+'_slider');            // get slider object
    var sliderAction = 'focus';
    $(slider)
      .on(sliderAction, function(){$('#'+IPQ.cip+IPQ[oid].hbutton).trigger('click')})
      .attr('min', 1)
      .attr('max', steps)
      .val(pselr)
      .css('width', (IPQ[oid].radius*2 - IPQ[oid].strokeWidth + 6)+'px');
    $(slider).parent().css('margin-left', '-134px');
//console.log('setSlider from initGauge ' , initGauge.caller.name , pselr);
    setSlider(oid,pselr);
    $('.slider i.icon.Lower').on('tap', function(){if(pselr > 1){$(slider).val(parseInt($(slider).val())-1); setSlider(oid,slider.val()); $(slider).focus()}});
    $('.slider i.icon.Higher').on('tap', function(){if(pselr < steps){$(slider).val(parseInt($(slider).val())+1); setSlider(oid,slider.val()); $(slider).focus()}});
  }else{                                          // ----- When using gauge only
    if(IPQ[oid].ipVal != ''){
      buildNeedle(oid,IPQ[oid].ipVal);
      $('.SelectedStrategies_Level')
        .text(parseInt(IPQ[oid].ipVal))
    }
    $('.SelectedStrategies_Name')
      .css('margin-left', IPQ[oid].legend == 'Y' ? '-134px' : 0)
      .text(IPQ[oid].gsst+' '+IPQ[oid].ipNam);
  }
  var ml = ($('.gaugeWidget').width()-$('#'+oid+'_svg').width())/2 + IPQ[oid].radius + IPQ[oid].strokeWidth - 15;
  if(IPQ[oid].ipVal == '' && IPQ[oid].slid == 'N') {
    $('.SelectedStrategies_Level').parent().hide();
  }else{
    $('.SelectedStrategies_Level')
      .css({'margin-left': ml+'px', 'background-color':IPQ[oid].gfgc});
    $('.SelectedStrategies_Text')
      .css('margin-left', IPQ[oid].legend == 'Y' ? '-132px' : 0)
      .text(((IPQ[oid].ipVal == '' && IPQ[oid].slid == 'N') ? 'No ' : '') + IPQ[oid].grlt);
  }
//  $('#'+oid+'_gauge').html($('#'+oid+'_gauge').html());
}
function setSlider(oid,v){
  $('#'+oid).val(IPQ[oid].riskLevels[v-1][2]);
  $('.SelectedStrategies_Name')
    .css('margin-left', '-134px')
    .text(IPQ[oid].gsst+' '+IPQ[oid].riskLevels[v-1][1] );
  $('.SelectedStrategies_Level')
    .text(IPQ[oid].riskLevels[v-1][0])
  buildNeedle(oid,IPQ[oid].riskLevels[v-1][0]);      // call gauge with selected item risklevel value
}
function setGauge(oid,v){
  $('#'+oid+'_gauge path.arc_front').attr('d', describeArc(0, 0, IPQ[oid].radius-IPQ[oid].strokeWidth+IPQ[oid].strokeWidth/3, 0, 180/IPQ[oid].gmax*v));
}
function buildGauge(oid){
  if(IPQ[oid].ipVal == '' && IPQ[oid].slid == 'N') {
    $('#'+oid+'_svg').remove();
    return false;
  }
  var gauge = $('#'+oid+'_svg'),
      w = IPQ[oid].radius*2 + IPQ[oid].strokeWidth + 2,
      h = IPQ[oid].radius + IPQ[oid].strokeWidth/2 + 1;
  $(gauge)
    .attr('width', w+40+ (IPQ[oid].legend == 'Y' ? 128 : 0))
    .attr('height', h+20)
    .attr('view-box', '0 0 '+(w+40+(IPQ[oid].legend == 'Y' ? 128 : 0))+' '+(h+20));
  $(gauge).find('g.scale')
    .attr('transform', 'translate('+(h+20)+', '+(h+20)+')');
  $(gauge).find('g.arcs')
    .attr('transform', 'translate('+(h+20)+', '+(h+20)+')');
  for(var i = 0; i < IPQ[oid].zone.length-1; i++){
    $(gauge).find('path.arc_back'+(i+1))
      .attr('stroke-width', IPQ[oid].strokeWidth)
      .attr('stroke', IPQ[oid].zone[i][3])
      .attr('fill', 'none')
      .attr('d', describeArc(0, 0, IPQ[oid].radius, 180/IPQ[oid].gmax*(IPQ[oid].zone[i][1]-1), 180/IPQ[oid].gmax*IPQ[oid].zone[i][2]))
  }
/*
  $(gauge).find('path.arc_front')
    .attr('stroke-width', IPQ[oid].strokeWidth/3)
    .attr('stroke', IPQ[oid].gfgc)
    .attr('fill', 'none');
*/
  buildscale(oid);
  if(IPQ[oid].legend == 'Y') buildLegend(oid);
}
function buildscale(oid){
  var gauge_scale = $('#'+oid+'_svg').find('g.scale'),
      zone = IPQ[oid].zone,
      zonel = zone.length-1;
  gauge_scale.find('line.scaleItem0')
    .attr('x1', -(IPQ[oid].radius + IPQ[oid].strokeWidth/2 + 5))
    .attr('y1', 0)
    .attr('x2', -(IPQ[oid].radius + IPQ[oid].strokeWidth/2))
    .attr('y2', 0)
    .attr('stroke', IPQ[oid].gfgc)
    .attr('stroke-width', 2)
  gauge_scale.find('text.scaleText0')
    .attr('x', -(IPQ[oid].radius + IPQ[oid].strokeWidth/2 +12))
    .attr('y', 0)
    .attr('fill', IPQ[oid].gfgc)
    .attr('text-anchor', 'middle')
    .css('font-weight', 400)
    .text('0')

  for(var i = 0; i < zonel; i++){
    var rad = Math.PI * zone[i][2] / IPQ[oid].gmax,
        len1 = IPQ[oid].radius + IPQ[oid].strokeWidth/2-1 + 5,
        len2 = IPQ[oid].radius + IPQ[oid].strokeWidth/2-1;
        len3 = IPQ[oid].radius + IPQ[oid].strokeWidth/2 + 10;
    gauge_scale.find('line.scaleItem'+(i+1))
      .attr('x1', -(len1 * Math.cos(rad)).toFixed(2))
      .attr('y1', -(len1 * Math.sin(rad)).toFixed(2))
      .attr('x2', -(len2 * Math.cos(rad)).toFixed(2))
      .attr('y2', -(len2 * Math.sin(rad)).toFixed(2))
      .attr('stroke', IPQ[oid].gfgc)
      .attr('stroke-width', 2)
    gauge_scale.find('text.scaleText'+(i+1))
      .attr('x', -(len3 * Math.cos(rad)).toFixed(2))
      .attr('y', -(len3 * Math.sin(rad)).toFixed(2))
      .attr('fill', IPQ[oid].gfgc)
      .attr('text-anchor', 'middle')
      .css('font-weight', 400)
      .text(zone[i][2])
  }
}
function buildNeedle(oid,v){
  var rad = Math.PI * v / IPQ[oid].gmax,
      len = IPQ[oid].radius + IPQ[oid].strokeWidth/2 + 10,
      points = {
        x1:0,
        y1:0,
        x2: -(len * Math.cos(rad)).toFixed(2),
        y2: -(len * Math.sin(rad)).toFixed(2)
      }
  $('#'+oid+'_svg').find('line.needle')
    .attr('stroke-linecap' , 'round')
    .attr('x1', points.x1)
    .attr('y1', points.y1)
    .attr('x2', points.x2)
    .attr('y2', points.y2)
    .attr('stroke' , IPQ[oid].gfgc)
    .attr('stroke-width', 3)
}
function buildNeedle_polygon(oid,v){
  var rad = Math.PI * v / IPQ[oid].gmax,
      len = IPQ[oid].radius + IPQ[oid].strokeWidth/2 + 3,
      points = {
        x1:0,
        y1:0,
        x2: -(len * Math.cos(rad)).toFixed(2),
        y2: -(len * Math.sin(rad)).toFixed(2)
      }
  $('#'+oid+'_svg').find('polygon.needle')
    .attr('stroke-linecap' , 'round')
    .attr('points', points.x1+','+points.y1+' '+points.x2+','+points.y2)
    .attr('stroke' , IPQ[oid].gfgc)
}
function buildLegend(oid){
  var legend = $('#'+oid+'_svg').find('g.gauge-legend'),
      zone = IPQ[oid].zone,
      zonel = zone.length-1,
      svgwidth = $('#'+oid+'_svg').width(),
      svgheight = $('#'+oid+'_svg').height();
//  legend.attr('transform', 'translate('+parseInt(svgwidth-100)+','+parseInt(svgheight/10)+')');
  legend.attr('transform', 'translate('+((IPQ[oid].radius+IPQ[oid].strokeWidth)*2+10)+','+parseInt(svgheight/10)+')');
  for(var i = 0; i < zonel; i++){
    legend.find('g.legend-item'+i)
      .attr('transform', 'translate(9,'+(3+(17*i))+')')
    legend.find('g.legend-item'+i)
      .find('text')
        .text(zone[i][0])
        .attr('x', 21)
        .attr('y', 15)
        .attr('text-anchor', 'start');
    legend.find('g.legend-item'+i)
      .find('rect')
        .attr('fill', zone[i][3]) //x="0" y="4" width="16" height="12"
        .attr('x', 0)
        .attr('y', 4)
        .attr('width', 16)
        .attr('height', 12);
  }
}


function polarToCartesian(centerX, centerY, radius, angleInDegrees){
  var angleInRadians = (angleInDegrees-180) * Math.PI / 180.0;
  return {
    x: centerX + (radius * Math.cos(angleInRadians)),
    y: centerY + (radius * Math.sin(angleInRadians))
  };
}
function describeArc(x, y, radius, startAngle, endAngle){
  var start = polarToCartesian(x, y, radius, endAngle);
  var end = polarToCartesian(x, y, radius, startAngle);
  var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";
  return [
    "M", start.x, start.y,
    "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
  ].join(" ");
}