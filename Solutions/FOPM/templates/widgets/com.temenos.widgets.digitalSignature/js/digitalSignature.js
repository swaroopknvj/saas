$(document).ready(function(){
	var arr = [];
    i = 0;
	$('.signature-pad').each(function(index,element){
		arr[i++] = $(this).attr('id');
	});
	arr.map(function (id) {
		signatureInit(id);
	});  
});

function resizeCanvasPop(canvas) {
  var ratio =  Math.max(window.devicePixelRatio || 1, 1);
  canvas.width = canvas.offsetWidth * ratio;
  canvas.height = canvas.offsetHeight * ratio;
  canvas.getContext("2d").scale(ratio, ratio);
}

function signatureInit(id){

var dataURL = "";
var wrapper = document.getElementById(id);
var disableButton = $(wrapper).data('disablebutton');
var DSQuestion = $(wrapper).data('dsquestion');
var clearButton = wrapper.querySelector("[data-action=clear]");
var skipSign = wrapper.querySelector("[data-action=skip-sign]");
var canvas = wrapper.querySelector("canvas");
var signaturePad = new SignaturePad(canvas);

window.addEventListener("resize", resizeCanvas);
resizeCanvas();

function resizeCanvas() {

  var ratio =  Math.max(window.devicePixelRatio || 1, 1);
  canvas.width = canvas.offsetWidth * ratio;
  canvas.height = canvas.offsetHeight * ratio;
  canvas.getContext("2d").scale(ratio, ratio);

  signaturePad.clear();
  
  $(canvas).removeClass("disableCanvas");
  $(skipSign).removeAttr("checked").parent().removeClass('checked');
  $('#'+disableButton).attr("disabled", "disabled");
}

$(canvas).on("mouseup mouseleave touchend", function (event) {
  if (signaturePad.isEmpty()) {
    dataURL = "";
	$('#'+DSQuestion).val("");
  } else {
    dataURL = signaturePad.toDataURL('image/png').split("data:image/png;")[1];
	$('#'+DSQuestion).val(dataURL);
	if(disableButton.length){
		$('#'+disableButton).removeAttr("disabled");
	}
  }
});

clearButton.addEventListener("click", function (event) {
  signaturePad.clear();
  $('#'+DSQuestion).val("");
  if(disableButton.length){
    $('#'+disableButton).attr("disabled", "disabled");
    $(skipSign).removeAttr("checked").parent().removeClass('checked');
    $(canvas).removeClass("disableCanvas");
  }
});

skipSign.addEventListener("click", function (event) {
  if(skipSign.checked == true){
	signaturePad.clear();
	$('#'+DSQuestion).val("");
	$(canvas).addClass("disableCanvas");
	if(disableButton.length){
		$('#'+disableButton).removeAttr("disabled");
	}
  }else{
    $(canvas).removeClass("disableCanvas");
	if(disableButton.length){
	$('#'+disableButton).attr("disabled", "disabled");
	}
  }
});

} 