(function( factory ){
  if ( typeof define === "function" && define.amd ) {
    define([ "../jquery.ui.datepicker" ], factory );
  } else {
    factory( jQuery.datepicker );
  }
}(function( datepicker ) {
  datepicker.regional['fr'] = {
    closeText: 'Fermer',
    prevText: 'Mois pr&eacute;c&eacute;dent',
    nextText: 'Mois suivant',
    currentText: 'Aujourd\'hui',
    monthNames: ['Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre'],
    monthNamesShort: ['Janv.', 'F&eacute;vr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Ao&ucirc;t', 'Sept.', 'Oct.', 'Nov.', 'D&eacute;c.'],
    dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
    dayNamesShort: ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
    dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
    weekHeader: 'Sem.',
//    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };
  datepicker.setDefaults(datepicker.regional['fr']);
  return datepicker.regional['fr'];
}));