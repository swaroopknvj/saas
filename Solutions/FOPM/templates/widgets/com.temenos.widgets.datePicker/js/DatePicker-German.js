(function( factory ){
  if ( typeof define === "function" && define.amd ) {
    define([ "../jquery.ui.datepicker" ], factory );
  } else {
    factory( jQuery.datepicker );
  }
}(function( datepicker ) {
  datepicker.regional['de'] = {
    closeText: 'Schliessen',
    prevText: 'Vorheriger',
    nextText: 'N&auml;chster',
    currentText: 'Heute',
    monthNames: ['Januar', 'Februar', 'M&auml;rz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
    monthNamesShort: ['Jan.', 'Feb.', 'Mrz.', 'Apr.', 'Mai', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Okt.', 'Nov.', 'Dez.'],
    dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
    dayNamesShort: ['Son.', 'Mon.', 'Die.', 'Mit.', 'Don.', 'Fre.', 'Sam.'],
    dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
    weekHeader: 'Woche',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };
  datepicker.setDefaults(datepicker.regional['de']);
  return datepicker.regional['de'];
}));