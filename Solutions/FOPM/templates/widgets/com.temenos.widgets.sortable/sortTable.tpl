<script>
$(document).ready(function(){
  if(jQuery.ui){
    $('.sortTable').find('tbody').sortable({
      forcePlaceholderSize:true,
      handle:'i.icon-drag',
      stop: rowdrop
    })
    $('td:visible').each(function(){$(this).css('width', $(this).width()*0.9 +'px')});
  }
})
function rowdrop(){
  $('.sortTable tbody tr').each(function(i,r){
    $(r).find('.rank').val(i+1);
  })
}
</script>
<style>
td a span i.icon-drag {cursor: n-resize;}
tr.ui-sortable-helper {display: table-row;}
tr.ui-sortable-helper>td {display: inline;}
td.Number.techID {width: 100px !important;}
</style>