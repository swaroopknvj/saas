<style>
tr.bggrey {background-color:#f1f1f1;}
tr.notMapped {background-color: #e8e8e8;}
span.noData {margin-left: 2px;}
div.cindent {text-indent:2rem}
i.icon.sh {vertical-align:top}
.PrimeTable .formAnswer textarea.formItem {line-height: 2rem;}
td.rowSpanOnSub {background-color:#fff;}
td.rowSpanOnSub div {padding-left: 1rem;}
td.rowSpanOnSub div.header {background-color:#f1f1f1; border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; margin: -2px -1px 2px; height: 35.3px;}
</style>
<script>
var table1 = $('.PrimeTable');
var table2 = $('.SecondaryTable');
function mergeTables(){
  table1.find('tbody tr').each(function(i,r){
    $(r).addClass('bggrey bold');
    $(r).attr('data-si', $(r).find('.si').text().trim());
    $(r).attr('data-rank', $(r).find('.rank').text().trim());
    $(r).find('td:visible').first().find('>div').prepend($('<i class="icon BlueMagic down sh" onclick="sh(this)"></i>'));
  });
  table2.find('tbody tr').each(function(i,r){
    $(r).attr('data-id', $(r).find('.id').text().trim());
    $(r).attr('data-si', $(r).find('.si').text().trim());
    $(r).attr('data-rank', $(r).find('.rank').text().trim());
    $(r).find('td:visible').first().find('>div').addClass('cindent')
  });
  reorderTableRows($(table1).attr('id'),'rank','asc');
  reorderTableRows($(table2).attr('id'),'rank','desc');

  table2.find('tbody tr').each(function(i,r){
    var si = $(r).data('si'),
        srow = table1.find('tbody tr[data-si="'+si+'"]').eq(0);
    srow.after($(r));
  });
  if(table2.find('tbody tr').length){
    var AvailableText = $('div.AvailableText').find('label').text().trim();
    table1.append($('<tr class="bggrey bold notMapped"/>').append($('<td colspan="9">'+AvailableText+'</td>').prepend($('<i class="icon BlueMagic down sh" onclick="sh(this)"></i>'))));
    table2.find('tbody tr').each(function(i,r){
      srow = table1.find('tbody tr:last');
      srow.after($(r));
    });
  }
  table2.hide();
  table1.find('tbody tr.bggrey').each(function(i,r){        // hide chevron if no questions after section
    if($(r).next('tr').hasClass('bggrey') || $(r).next('tr').length==0) $(r).find('i.icon.sh').css('visibility','collapse')
  });
  table1.find('tbody td.rowSpanOnSub').each(function(i,c){
    var rows = $(c).closest('tbody').find('tr[data-si='+$(c).closest('tr').data('si')+']').length;
    $(c).attr('rowspan', rows)
        .attr('data-rowspan', rows)
        .prepend($('<div class="header"/>').text($(c).closest('table').find('thead th').eq($(c).index()).text().trim())) // style="line-height:3em"
    $(c).find('textarea').attr('rows', rows)
  })
};
function sh(o){
  $(o).closest('tr').nextUntil('tr.bggrey').toggle();
  if($(o).closest('tr').find('td[rowspan]')) toggleRowSpan($(o));
  $(o).toggleClass('down right')
}
function toggleRowSpan(o){
  var obj = $(o).closest('tr').find('.formItem[rows]');
  obj.parent().toggle();
  var cell = obj.closest('td');
  if(cell.attr('rowspan')==1) {
    cell.attr('rowspan',cell.data('rowspan'))
  }else{
    cell.attr('rowspan',1)
  }
}
$(document).ready(function(){
  mergeTables();
//  setTimeout("mergeTables()", 200);
})
</script>