﻿function unique(array){
  return array.filter(function(el,index,arr){
    return index == arr.indexOf(el);
  });
}
var MatrixData = {};
  var measures = [], titles = [], types = [];

function matrixInit(o){
  MatrixData.settings = o;
  var tid = o.tid;
  var preMatrix = $('#'+tid);
  if(!preMatrix.length) return false;
  var Matrix = $('#MX_'+tid);

  var maxrows  = o.maxrows;             // integer
  var maxcols  = o.maxcols;             // integer
  var sortX    = o.sortX;               // 'none', 'data-x', 'data-x-rank', 'sum'
  var sortXdir = o.sortXdir;            // 'none', 'asc', 'desc'
  var sortY    = o.sortY;               // 'none', 'data-y', 'data-y-rank', 'sum'
  var sortYdir = o.sortYdir;            // 'none', 'asc', 'desc'
  var refccy   = o.refccy;              // Reference Currency
  var RowTot   = o.RowTot;              // 'none', 'before', after', 'both'
  var ColTot   = o.ColTot;              // 'none', 'before', after', 'both'
  var AllTot   = o.AllTot;              // 'none', 'yes'
  var dataviz  = o.Measures;            // array of measures [table data column number (0 based), "display in cells", "display in ColTot", "display in RowTot", "display in AllTot", Number of Decimals]
  var ZeroVals = o.ZeroVals;            // array : display zero values in ["cells", "ColTot", "RowTot", "AllTot"]

  var dataXdim  = preMatrix.find('thead th.data-x').index();         // Dimension for Columns
  var dataXdimR = preMatrix.find('thead th.data-x-rank').index();    // Columns Rank
  var dataYdim  = preMatrix.find('thead th.data-y').index();         // Dimension for Rows
  var dataYdimR = preMatrix.find('thead th.data-y-rank').index();    // Rows Rank
  var titleX = preMatrix.find('thead th.data-x').text().trim();
  var titleY = preMatrix.find('thead th.data-y').text().trim();

  var measures = [], titles = [], types = [];

  $(dataviz).each(function(m,d){
    measures.push(dataviz[m][0]);
  });
  $(measures).each(function(m,t){
    titles.push(preMatrix.find(' thead th').eq(measures[m]).text().trim())
    types.push(preMatrix.find(' thead th').eq(measures[m]).attr('class'))
  });
  var MxArray=[], MxArrayR=[], MyArray=[], MyArrayR=[];
  preMatrix.find(' tbody tr').each(function(i,r){
    MxArray.push($(r).find('td').eq(dataXdim).text());
    $(r).attr('data-x', $(r).find('td').eq(dataXdim).text().trim())
    MxArrayR.push($(r).find('td').eq(dataXdimR).text().trim());

    MyArray.push($(r).find('td').eq(dataYdim).text());
    $(r).attr('data-y', $(r).find('td').eq(dataYdim).text().trim())
    MyArrayR.push($(r).find('td').eq(dataYdimR).text().trim());
  });
  MxArray   = unique(MxArray);
  MxArrayR  = unique(MxArrayR);
  MyArray   = unique(MyArray);
  MyArrayR  = unique(MyArrayR);

  var MXL = [], MYL = [];               // Labels for X, Y axis with ranking
  for(var i = 0; i < MxArray.length; i++){MXL.push([MxArray[i],MxArrayR[i]])}
  for(var i = 0; i < MyArray.length; i++){MYL.push([MyArray[i],MyArrayR[i]])}

  var MxLArray=[], MyLArray=[];         // Limited cols / rows array
  var MxOArray=[], MyOArray=[];         // Other cols / rows array

// ----- Here we go, take some actions
  buildMatrixData();                    // build JSON with Matrix Data
//console.log(MatrixData);
//console.log(JSON.stringify(MatrixData));

  buildMatrix();                        // start building the new matrix
  postMatrixDisplay();
  if(o.linksEnable == "Y"){
    if(o.tfilter == "Y")
      setMatrixFilterLinks(Matrix, o.filterTableID, o.linkX, o.linkY);
    if(o.tlink =="Y")
      setMatrixPageLinks(Matrix, o);
    if(o.filterss == "Y")
      setMatrixFilterSS(Matrix, o.Component_ID_Prefix, o.filterssX, o.filterssY, o.filterssB);
  }
  if(o.xtt == 'Y'){
    getCellXTT();
    if($('body').hasClass('ios')||$('body').hasClass('surface')){      // iPad tooltips
      Matrix.find('[xtt]').on('tap', function(){XTT(this, true)})
    }else{
      Matrix.find('[xtt]').hover(function(){XTT(this)}, function(){XTTremove(this)})
    }
  }
  $(preMatrix).remove();                // remove original table

  var cellWidth = 100/(MxArray.length + 3);                 // calculate optimal matrix column width
  Matrix.find('thead td').css('width',cellWidth+'%');       // equalize matrix columns width

  /* ----- Build Aggregated JSON from prematrix values ----- */
  function buildMatrixData(){           // Data for Matrix
    MatrixData.cells = {};              // Object data for Matrix cells
    MatrixData.totX = {};               // Object data for Total cells in Columns
    MatrixData.totY = {};               // Object data for Total cells in Rows
    MatrixData.totXY = {};              // Object data for Grand Total cell

    getMatrixCells();                   // Get content for Matrix Cells
    getMatrixColsTot();                 // Get content for Matrix Columns Total Cells
    getMatrixRowsTot();                 // Get content for Matrix Rows Total Cells
    getMatrixGrandTot();                // Get content for Matrix Grand Total Cell
    getMatrixSortOrderX();              // Get sort order for Matrix Columns
    getMatrixSortOrderY();              // Get sort order for Matrix Rows
    getMatrixLimitX();                  // Get sum for Others columns
    getMatrixLimitY();                  // Get sum for Others row
    getMatrixLimitXY();                 // Get sum for Others intersection

    function getMatrixCells(){
     for(var y=0; y<MyArray.length; y++){          // Data for Matrix data cells
        MatrixData.cells[MyArray[y]] = {};
        for(var x=0; x<MxArray.length; x++){
          MatrixData.cells[MyArray[y]][MxArray[x]] = {};
          var datarows = preMatrix.find('tbody tr[data-y="'+MyArray[y]+'"][data-x="'+MxArray[x]+'"]');
          for(var m = 0; m < measures.length; m++){
            var cellsum = 0, celldata = false;
            $(datarows).each(function(i,c){
              var cellcont = $(c).find('td').eq(measures[m]).text().trim();
              if(Number(cellcont)) cellsum += Number(cellcont);
              if(cellcont != '') celldata = true;
            })
            MatrixData.cells[MyArray[y]][MxArray[x]][m] = [cellsum, celldata];
          }
        }
      }
    }
    function getMatrixColsTot(){
      for(var x=0; x<MxArray.length; x++){          // Data for Matrix columns total cells
        MatrixData.totX[MxArray[x]] = {};
        var datarows = preMatrix.find('tbody tr[data-x="'+MxArray[x]+'"]');
        for(var m=0; m<measures.length; m++){
          var cellsum = 0, celldata = false;
          $(datarows).each(function(i,c){
            var cellcont = $(c).find('td').eq(measures[m]).text().trim();
            if(Number(cellcont)) cellsum += Number(cellcont);
            if(cellcont != '') celldata = true;
          })
          MatrixData.totX[MxArray[x]][m] = [cellsum, celldata];
        }
      }
    }
    function getMatrixRowsTot(){
      for(var y=0; y<MyArray.length; y++){          // Data for Matrix rows total cells
        MatrixData.totY[MyArray[y]] = {};
        var datarows = preMatrix.find('tbody tr[data-y="'+MyArray[y]+'"]');
        for(var m=0; m<measures.length; m++){
          var cellsum = 0, celldata = false;
          $(datarows).each(function(i,c){
            var cellcont = $(c).find('td').eq(measures[m]).text().trim();
            if(Number(cellcont)) cellsum += Number(cellcont);
            if(cellcont != '') celldata = true;
          })
          MatrixData.totY[MyArray[y]][m] = [cellsum, celldata];
        }
      }
    }
    function getMatrixGrandTot(){
      var datarows = preMatrix.find('tbody tr');    // Data for Matrix grand total cell
      for(var m=0; m<measures.length; m++){
        var cellsum = 0, celldata = false;
        $(datarows).each(function(i,c){
          var cellcont = $(c).find('td').eq(measures[m]).text().trim();
          if(Number(cellcont)) cellsum += Number(cellcont);
          if(cellcont != '') celldata = true;
        })
        MatrixData.totXY[m] = [cellsum, celldata];
      }
    }
    function getMatrixSortOrderX(){
      if(o.sortX == 'none' || o.sortXdir == 'none'){// Compute sortorder X
        MatrixData.sortX = MxArray;
      }else if(o.sortX == 'data-x-rank'){           // by data-x-rank
        var SortX = MXL, Xsort = [];
        if(o.sortXdir == 'asc') {SortX.sort(function(a, b){return a-b})}
        else                    {SortX.sort(function(a, b){return b-a})}
        for(var x = 0; x < MxArray.length; x++){
          Xsort.push(SortX[x][0])
        }
        MatrixData.sortX = Xsort;
      }else if(o.sortX == 'data-x'){                // by data-x
        var SortX = MxArray;
        if(o.sortXdir == 'asc') {SortX.sort()}
        else                    {SortX.sort().reverse()}
        MatrixData.sortX = SortX;
      }else{                                        // by sum
        var SortX = [], Xsort = [];
        for(var x = 0; x < MxArray.length; x++){SortX.push(MatrixData.totX[MxArray[x]][0][0])}
        if(o.sortXdir == 'asc') {SortX.sort(function(a, b){return a-b})}
        else                    {SortX.sort(function(a, b){return b-a})}
        for(var x = 0; x < MxArray.length; x++){
          for(var a = 0; a < MxArray.length; a++){
            if(MatrixData.totX[MxArray[a]][0][0] == SortX[x]){
              Xsort.push(MxArray[a])
            }
          }
        }
        Xsort = unique(Xsort);
        MatrixData.sortX = Xsort;
      }
    }
    function getMatrixSortOrderY(){
      if(o.sortY == 'none' || o.sortYdir == 'none'){// Compute sortorder Y
        MatrixData.sortY = MyArray;
      }else if(o.sortY == 'data-y-rank'){           // by data-y-rank
        var SortY = MYL, Ysort = [];
        if(o.sortYdir == 'asc') {SortY.sort(function(a, b){return a-b})}
        else                    {SortY.sort(function(a, b){return b-a})}
        for(var y = 0; y < MyArray.length; y++){
          Ysort.push(SortY[x][0])
        }
        MatrixData.sortY = Xsort;
      }else if(o.sortY == 'data-y'){                // by data-y
        var SortY = MyArray;
        if(o.sortYdir == 'asc') {SortY.sort()}
        else                    {SortY.sort().reverse()}
        MatrixData.sortY = SortY;
      }else{                                        // by sum
        var SortY = [];
        for(y=0; y<MyArray.length; y++){SortY.push(MatrixData.totY[MyArray[y]][0][0])}
        if(o.sortYdir == 'asc') {SortY.sort(function(a, b){return a-b})}
        else                    {SortY.sort(function(a, b){return b-a})}
        var Ysort = [];
        for(var y = 0; y < MyArray.length; y++){
          for(var a = 0; a < MyArray.length; a++){
            if(MatrixData.totY[MyArray[a]][0][0] == SortY[y]){
              Ysort.push(MyArray[a])
            }
          }
        }
        Ysort = unique(Ysort);
        MatrixData.sortY = Ysort;
      }
    }
    function getMatrixLimitX(){         // sum up 'Other' columns
      if(maxcols >= MxArray.length) MxLArray = MxArray;
      else{
        for(var x = 0; x < maxcols-1; x++){MxLArray.push(MatrixData.sortX[x])}              // Matrix Columns to display
        for(var x = maxcols-1; x < MxArray.length; x++){MxOArray.push(MatrixData.sortX[x])} // Matrix Others Columns
        MatrixData.otherX = {};
        for(var y = 0; y < MyArray.length; y++){
          MatrixData.otherX[MatrixData.sortY[y]] = [];
          for(var m = 0; m < measures.length; m++){
            var cellsum = 0, celldata = false;
            for(var x = maxcols-1; x < MxArray.length; x++){
              var cellcont = MatrixData.cells[MatrixData.sortY[y]][MatrixData.sortX[x]][m];
              cellsum += cellcont[0];
              if(cellcont[1]) celldata = true;
            }
            MatrixData.otherX[MatrixData.sortY[y]].push([cellsum, celldata]);
          }
        }
        MatrixData.otherXSum = [];
        for(var m = 0; m < measures.length; m++){
          var cellsum = 0, celldata = false;
          for(var y = 0; y < MyArray.length; y++){
            var cellcont = MatrixData.otherX[MatrixData.sortY[y]][m];
            cellsum += cellcont[0];
            if(cellcont[1]) celldata = true;
          }
          MatrixData.otherXSum.push([cellsum, celldata]);
        }
      }
    }
    function getMatrixLimitY(){
      if(maxrows >= MyArray.length) MyLArray = MyArray;
      else{
        for(var y = 0; y < maxrows-1; y++){MyLArray.push(MatrixData.sortY[y])}              // Matrix Rows to display
        for(var y = maxrows-1; y < MyArray.length; y++){MyOArray.push(MatrixData.sortY[y])} // Matrix Others Rows
        MatrixData.otherY = {};
        for(var x = 0; x < MxArray.length; x++){
          MatrixData.otherY[MatrixData.sortX[x]] = [];
          for(var m = 0; m < measures.length; m++){
            var cellsum = 0, celldata = false;
            for(var y = maxrows-1; y < MyArray.length; y++){
              var cellcont = MatrixData.cells[MatrixData.sortY[y]][MatrixData.sortX[x]][m];
              cellsum += cellcont[0];
              if(cellcont[1]) celldata = true;
            }
            MatrixData.otherY[MatrixData.sortX[x]].push([cellsum, celldata]);
          }
        }
        MatrixData.otherYSum = [];
        for(var m = 0; m < measures.length; m++){
          var cellsum = 0, celldata = false;
          for(var x = 0; x < MxArray.length; x++){
            var cellcont = MatrixData.otherY[MatrixData.sortX[x]][m]
            cellsum += cellcont[0];
            if(cellcont[1]) celldata = true;
          }
          MatrixData.otherYSum.push([cellsum, celldata]);
        }
      }
    }
    function getMatrixLimitXY(){
      if(!(MatrixData.otherX && MatrixData.otherY)) return;
      MatrixData.otherXYSum = [];
      for(var m = 0; m < measures.length; m++){
        var cellsum = 0, celldata = false;
        for(var y = 0; y < MyOArray.length; y++){
          var cellcont = MatrixData.otherX[MyOArray[y]][m];
          cellsum += cellcont[0];
          if(cellcont[1]) celldata = true;
        }
        MatrixData.otherXYSum.push([cellsum, celldata]);
      }
    }
  }

  /* ----- Complete Matrix Construction ----- */
  function buildMatrix(){
    buildMatrixHeader();                // Matrix Header
    buildMatrixBody();                  // Matrix Data Cells + Totals column cell
    buildMatrixFooter();                // Matrix Totals Row

    function buildMatrixHeader(){       // build matrix header
      var mheader = Matrix.find('thead');
      var newRow = $('<tr/>');
      newRow.append($('<th/>').addClass('hlabel'));
      for(var x = 0; x < MxLArray.length; x++){
        var cell = $('<th/>').text(MatrixData.sortX[x]).addClass('clabel');
        if(o.xtt == 'Y'){cell.attr('xtt', JSON.stringify({'head':titleX+': '+MatrixData.sortX[x],'data':''}))}    // extended tooltip handling
        else{cell.attr('title', titleX + ' : ' + MatrixData.sortX[x])}          // normal tooltip
        newRow.append(cell);
      }
      if(MxOArray.length){              // Other column
        var cell = $('<th/>').text(o.OLabelX).addClass('clabel other').attr('ref', MxOArray.join('|'));
        if(o.xtt == 'Y'){cell.attr('xtt', JSON.stringify({'head':titleX+': '+MxOArray.join(', '),'data':''}))}    // extended tooltip handling
        else{cell.attr('title', titleX + ' : ' + MxOArray.join(', '))}          // normal tooltip
        newRow.append(cell);
      }
      if(o.TLabelY.length){             // Total column
        newRow.append($('<th/>').addClass('mtotal').text(o.TLabelY));
      }else{
        newRow.append($('<th/>').addClass('nolabel'));
      }
      mheader.append(newRow);
    }
    function buildMatrixBody(){         // build Matrix body cells
      var mbody = Matrix.find('tbody');
      for(var y=0; y<MyLArray.length; y++){
        var newRow = $('<tr/>');
        var cell = $('<td/>');
        cell.text(MatrixData.sortY[y]);
        cell.addClass('mlabel');
        if(o.xtt == 'Y'){cell.attr('xtt', JSON.stringify({'head':titleY+': '+MatrixData.sortY[y],'data':''}))}    // extended tooltip handling
        else{cell.attr('title', titleY + ' : ' + MatrixData.sortY[y])}          // normal tooltip
        newRow.append(cell);
        for(var x=0; x<MxLArray.length; x++){
          var cell = $('<td/>');
          for(var m=0; m < measures.length; m++){
            var dat = MatrixData.cells[MatrixData.sortY[y]][MatrixData.sortX[x]][m];
            if(!dat[1]){                                                                  // no data = no display
              cell.append($('<i/>'));
            }else if(dataviz[m][1] == 'no' && dataviz[m][2] == 'no'){                     // produce, but hide data (display in cell: no + display in totals column: no : generate for tooltip)
              cell.append($('<p class="hidden"/>').append(getCell(dat[0],m,x,y,o.ttitem)));
            }else if(dataviz[m][1] != 'no'){                                              // display data
              cell.append(getCell(dat[0],m,x,y,o.ttitem));
            }else{                                                                        // dislplay empty item with no data
              cell.append($('<i/>').text('\20')); // &#032; // &#160; // &nbsp;
            }
          }
          newRow.append(cell);
        }
        if(MxOArray.length){              // add Others column cell
          var cell = $('<td/>').addClass('other').attr('ref', MxOArray.join('|'));
          for(var m=0; m < measures.length; m++){
            var dat  = MatrixData.otherX[MatrixData.sortY[y]][m];
            if(!dat[1]){                                                                  // no data = no display
              cell.append($('<i/>'));
            }else if(dataviz[m][1] == 'no' && dataviz[m][2] == 'no'){                     // produce, but hide data (display in cell: no + display in totals column: no : generate for tooltip)
              cell.append($('<p class="hidden"/>').append(getCell(dat[0],m,x,y,o.ttoc)));
            }else if(types[m] == 'posccy'){
              cell.append($('<i/>').text('\20'));
            }else if(dataviz[m][1] != 'no'){
              cell.append(getCell(dat[0],m,x,y,o.ttoc));
            }else{
              cell.append($('<i/>').text('\20'));
            }
          }
          newRow.append(cell);
        }
        var cell = $('<td/>').addClass('mtotal');                                         // --- build Matrix body totals column cell ---
        for(var m=0; m < measures.length; m++){
          var dat  = MatrixData.totY[MatrixData.sortY[y]][m];
          if(!dat[1]){                                                                    // no data = no display
              cell.append($('<i/>'));
          }else if(dataviz[m][1] == 'no' && dataviz[m][2] == 'no'){                       // produce, but hide data (display in cell: no + display in totals column: no : generate for tooltip)
            cell.append($('<p class="hidden"/>').append(getCell(dat[0],m,x,y,o.tttc)));
          }else if(item == 0 && ZeroVals[1] == 'no'){
            cell.append($('<i/>').text('\20'));
          }else if(dataviz[m][2] != 'no'){
            cell.append(getCell(dat[0],m,x,y,o.tttc));
          }else{
            cell.append($('<i/>').text('\20'));
          }
        }
        newRow.append(cell);
        mbody.append(newRow);
      }
      if(MyOArray.length){              // add Others row
        var newRow = $('<tr/>').addClass('other');
        var cell = $('<td/>').text(o.OLabelY);
        cell.addClass('mlabel other').attr('ref', MyOArray.join('|'));
        if(o.xtt == 'Y'){cell.attr('xtt', JSON.stringify({'head':titleY+': '+MyOArray.join(', '),'data':''}))}    // extended tooltip handling
        else{cell.attr('title', titleY + ' : ' + MyOArray.join(', '))}                    // normal tooltip
        newRow.append(cell);
        for(var x=0; x<MxLArray.length; x++){
          var cell = $('<td/>');
          cell.addClass('other').attr('ref', MyOArray.join('|'));
          if(o.xtt == 'Y'){cell.attr('xtt', JSON.stringify({'head':titleY+': '+MyOArray.join(', '),'data':''}))}    // extended tooltip handling
          for(var m=0; m < measures.length; m++){
            var dat = MatrixData.otherY[MatrixData.sortX[x]][m];
            if(!dat[1]){
              cell.append($('<i/>'));
            }else if(dataviz[m][1] == 'no' && dataviz[m][2] == 'no'){
              cell.append($('<p class="hidden"/>').append(getCell(dat[0],m,x,y,o.ttor)));
            }else if(dataviz[m][1] != 'no'){
              cell.append(getCell(dat[0],m,x,y,o.ttor));
            }else{
              cell.append($('<i/>').text('\20'));
            }
          }
          newRow.append(cell);
        }
        if(MxOArray.length){            // add Others row Others column data
          var cell = $('<td/>').addClass('Other').attr('refx', MxOArray.join('|')).attr('refy', MyOArray.join('|'));
          for(var m=0; m < measures.length; m++){
            var dat = MatrixData.otherXYSum[m];
            if(!dat[1]){
              cell.append($('<i/>'));
            }else if(dataviz[m][1] == 'no' && dataviz[m][2] == 'no'){
              cell.append($('<p class="hidden"/>').append(getCell(dat[0],m,x,y,o.ttor)));
            }else if(types[m] == 'posccy'){
              cell.append($('<i/>').text('\20'));
            }else if(dataviz[m][1] != 'no'){
              cell.append(getCell(dat[0],m,x,y,o.ttor));
            }else{
              cell.append($('<i/>').text('\20'));
            }
          }
          newRow.append(cell);
        }
        var cell = $('<td/>').addClass('mtotal other').attr('ref', MyOArray.join('|')); // add total column
        for(var m=0; m < measures.length; m++){
          var item = MatrixData.otherYSum[m][0];
          if(dataviz[m][1] == 'no' && dataviz[m][2] == 'no'){
            cell.append($('<p class="hidden"/>').append(getCell(item,m,x,y,o.tttc)));
          }else if(item == 0 && ZeroVals[0] == 'no' && dataviz[m][2] != 'no'){
            cell.append($('<i/>').text('\20'));
          }else if(dataviz[m][2] != 'no'){
            cell.append(getCell(item,m,x,y,o.tttc));
          }else{
            cell.append($('<i/>').text('\20'));
          }
        }
        newRow.append(cell);
        mbody.append(newRow);
      }
    }
    function buildMatrixFooter(){       // build Matrix footer row
      // --- build Matrix totals row ---
      var mfoot = Matrix.find('tfoot');
      var totRow = $('<tr/>').addClass('totals');
      if(o.TLabelX.length){
        totRow.append($('<td/>').addClass('mlabel').text(o.TLabelX));
      }else{
        totRow.append($('<td/>').addClass('nolabel'));
      }
      for(var x=0; x<MxLArray.length; x++){
        var cell = $('<td/>');
        for(var m=0; m < measures.length; m++){
          var item = MatrixData.totX[MatrixData.sortX[x]][m][0];
          if(!MatrixData.totX[MatrixData.sortX[x]][m][1]){
            cell.append($('<i/>'));
          }else if(dataviz[m][3] == 'no'){
            cell.append($('<p class="hidden"/>').append(getCell(item,m,x,0,o.tttr)));
          }else if(item == 0 && ZeroVals[0] == 'yes' && !MatrixData.totX[MatrixData.sortX[x]][m][1]){
            cell.append($('<i/>').text('\20'));
          }else if(item == 0 && ZeroVals[2] == 'no' && dataviz[m][3] != 'no'){
            cell.append($('<i/>').text('\20'));
          }else if(dataviz[m][3] != 'no'){
            cell.append(getCell(item,m,x,0,o.tttr));
          }else{
            cell.append($('<i/>').text('\20'));
          }
        }
        totRow.append(cell);
      }
      if(MxOArray.length){
        var cell = $('<td/>').addClass('other').attr('ref', MxOArray.join('|'));
        for(var m=0; m < measures.length; m++){
          var item = MatrixData.otherXSum[m][0];
          if(dataviz[m][3] == 'no'){
            cell.append($('<p class="hidden"/>').append(getCell(item,m,x,0,o.tttco)));
          }else if(types[m] == 'posccy'){
            cell.append($('<i/>').text('\20'));
          }else if(item == 0 && ZeroVals[0] == 'no' && dataviz[m][3] != 'no'){
            cell.append($('<i/>').text('\20'));
          }else if(dataviz[m][1] != 'no'){
            cell.append(getCell(item,m,x,0,o.tttco));
          }else{
            cell.append($('<i/>').text('\20'));
          }
        }
        totRow.append(cell);
      }
      // --- build Matrix grand totals cell ---
      var cell = $('<td/>').addClass('mtotal alltotal');
      for(var m=0; m < measures.length; m++){
        var item = MatrixData.totXY[m][0];
        if(dataviz[m][3] == 'no' && dataviz[m][4] == 'no'){
          cell.append($('<p class="hidden"/>').append(getCell(item,m,x,0,o.ttgt)));
        }else if(item == 0 && ZeroVals[0] == 'yes' && !MatrixData.totXY[m][1]){
          cell.append($('<i/>').text('\20'));
        }else if(item == 0 && ZeroVals[3] == 'no' && dataviz[m][4] != 'no'){
          cell.append($('<i/>').text('\20'));
        }else if(dataviz[m][4] != 'no'){
          cell.append(getCell(item,m,x,0,o.ttgt));
        }else{
          cell.append($('<i/>').text('\20'));
        }
      }
      totRow.append(cell);
      $(mfoot).append(totRow);
    }
  }

  function getCell(item,m,x,y,TT){                          // build cell item from measure
    var neg =(item < 0) ? ' class="neg"' : '';              // mark negative values (may be handled in css if wanted)
    var tt = o.xtt != 'Y' ? getToolTip(m,x,y,TT) : '';      // handle tooltips
    var citem = '';                                         // cell item
    if(types[m] == 'percent'){
      citem = ($('<i'+neg+'/>').text(FormatNumber((item*100).toFixed(dataviz[m][5]))+' %'));
    }else if(types[m] == 'Percent'){
      citem = ($('<i'+neg+'/>').text(FormatNumber(item.toFixed(dataviz[m][5]))+' %'));
    }else if(types[m] == 'refccy'){
      citem = ($('<i'+neg+'/>').text(FormatNumber(item.toFixed(dataviz[m][5]))+' '+o.refccy));
    }else if(types[m] == 'posccy'){
      var ccy = ' '+MatrixData.sortX[x];
      if(ccy.length != 4) ccy = '';
      citem = ($('<i'+neg+'/>').text(FormatNumber(item.toFixed(dataviz[m][5]))+ccy));
    }else{
      citem = ($('<i/>'));
    }
    if(o.xtt != 'Y') citem.attr('title',tt);
    return citem;
  }
  function getToolTip(m,x,y,TT){                            // get normal HTML tooltips for cell items
    var keys = TT.split(' ');                               // text to display (set in edge widget) (e.g.: "-self for -y in -x")
    var tt = '';
    for(var k = 0; k < keys.length; k++){
      if(keys[k].indexOf('-self') == 0){
        tt += ' ' + titles[m];
      }else if(keys[k].indexOf('-x') == 0){                 // get text of items column heading
        tt += ' ' + MatrixData.sortX[x];
      }else if(keys[k].indexOf('-y') == 0){                 // get text of row label
        tt += ' ' + MatrixData.sortY[y];
      }else if(keys[k].indexOf('-ox') == 0){                // get text of other x labels
        tt += ' ' + MxOArray.join(', ');
      }else if(keys[k].indexOf('-oy') == 0){                // get text of other y labels
        tt += ' ' + MyOArray.join(', ');
      }else if(keys[k].indexOf('#') == 0){                  // get text of item with corresponding id on page
        tt += ' ' + $(keys[k]).text().trim();
      }else{                                                // get translated text element
        tt += ' '+keys[k];
      }
    }
    return tt;
  }
  function getCellXTT(){                                    // get extended tooltips for cells
    $(Matrix).find('td:not(.mlabel)').each(function(i,c){   // get all cells without labels and header (already hard-coded in generation)
      if($(c).text().trim().length > 8){                    // if cell content
        var xtt = {};                                       // init eXtended TollTip object
        var iX = $(c).closest('table.matrixResult').find('thead th').eq($(c).index()).text().trim();          // get header text (X axis)
        var iY = $(c).closest('tr').find('td.mlabel').text().trim();                                          // get row label text (Y axis)
        xtt.head = $(c).hasClass('alltotal') ? o.xttgt :                                                      // XTT header info text
                   $(c).parent().hasClass('totals') &&  $(c).hasClass('other') ? o.xttot + ' ' + iX + '\n(' + MxOArray.join(', ') + ')' :
                   $(c).parent().hasClass('totals') ? o.xttt + ' ' + iX :
                   $(c).hasClass('other') ? iY + ' ' + o.xttoc + ' ' + iX + '\n(' + MxOArray.join(', ') + ')' :
                   $(c).hasClass('mtotal') ? o.xttt + ' ' + iY :
                   iY+' '+o.xttc+' '+iX;                    // default
        xtt.data = [];                                      // init data items
        $(c).find('i').each(function(a,item){               // loop data items
          if($(item).text().trim().length){
            xtt.data[a] = {};                               // declare item as object in array
            xtt.data[a][titles[a]] = $(item).text();        // set key + value
          }
        })
        $(c).attr('xtt', JSON.stringify(xtt));              // set xtt attribute with JSON string
      }
    })
  }

  function postMatrixDisplay(){                             // Rectify display and positions of total row/column
    if(AllTot == 'none') Matrix.find('td.alltotal').html('').removeClass('alltotal').addClass('nototal');
    if(RowTot == 'none') Matrix.find('tfoot').hide();
    else if(RowTot == 'before') Matrix.find('tbody').prepend(Matrix.find('tfoot tr'));
    else if(RowTot == 'both') Matrix.find('tbody').prepend(Matrix.find('tfoot tr').clone());
    if(ColTot == 'none') Matrix.find('td.mtotal').hide();
    else if(ColTot == 'before'){Matrix.find('tr').each(function(i,r){$(r).find('td.mlabel').after($(r).find('td.mtotal'))})}
    else if(ColTot == 'both'){Matrix.find('tr').each(function(i,r){$(r).find('td.mlabel').after($(r).find('td.mtotal').clone())})}
  }
}

/* ----- Start HTML eXtended ToolTip handling ----- */
function XTT(o,a){                                          // build eXtended ToolTip in HTML DOM (with argument: clicked cell)
  var xtt = JSON.parse($(o).attr('xtt'));                   // get JSON from cell
  var tt = $('<div class="xtt"/>');                         // start building
  tt.append($('<div class="head"/>').text(xtt.head))        // set header
  for(var x=0; x < xtt.data.length; x++){                   // loop through data
    for(key in xtt.data[x]){
      tt.append($('<div class="data"/>')
          .append($('<i/>').text(key+': '))
          .append($('<b/>').text(xtt.data[x][key]))
      );
    }
  }
  $('.matrixContainer > div.xtt').remove();                 // delete last XTT
  $(o).closest('.matrixContainer').append(tt);              // insert xtt in DOM as last element of matrixContainer
//  var mhght = $(o).closest('.matrixResult').height();
  var mhght = $(o).closest('.matrixContainer').height();
  var mwdth = $(o).closest('.matrixResult').width();
  var pos   = $(o).position(), cellh = $(o).height();       // get position and dimension of cell
  var ptop  = mhght-pos.top < (a?60:50) ? pos.top-cellh+'px' : pos.top+cellh-8+'px';
  var pleft = mwdth - pos.left < $(o).width()+20 ? parseInt(pos.left)-45+'px' : parseInt(pos.left)+25+'px';
  tt.css({'top': ptop, 'left': pleft})                      // position xtt
  if(a) $('.matrixContainer > div.xtt').delay(2000).fadeOut("slow");
}
function XTTremove(o){                                      // remove eXtended ToolTip from HTML DOM on mouseout
  $('.matrixContainer > div.xtt').remove();
}
/* ----- End HTML eXtended ToolTip handling ----- */


/* ----- Start Matrix-Filtering ----- */
function setMatrixFilterSS(Matrix, cip, filterssX, filterssY, filterssB){
  $(Matrix).attr('data-filterx', cip+filterssX)
           .attr('data-filtery', cip+filterssY)
           .attr('data-filterb', cip+filterssB);
  $(Matrix).find('th, td').each(function(i,c){
    if($(c).find('i').length || $(c).hasClass('mlabel') || $(c).hasClass('clabel'))       // check if content
      $(c).attr('onclick', 'MatrixFilter_ss(this)');
  })
}
function MatrixFilter_ss(o){                                // code for server-side filtering
  var Matrix = $(o).closest('table.matrixResult'),          // get matrix object
      xval = $(o).closest('table').find('thead th').eq($(o).index()).text().trim(),
      yval = $(o).closest('tr').find('td.mlabel').text().trim(),
      dX   =  Matrix.data('filterx'),
      dY   =  Matrix.data('filtery'),
      dB   =  Matrix.data('filterb');
  $('#'+dX).closest('.hidden').find('input').val('');       // clear value fields
  if($(o).hasClass('alltotal')){
  }else if($(o).hasClass('mtotal') && $(o).closest('tr').hasClass('totals')){
  }else if($(o).closest('thead').length || $(o).closest('tfoot').length){
    if($(o).hasClass('other')){
      var ref = $(o).attr('ref');
      addItem(ref, dX);
    }else{
      addItem(xval, dX);
    }
  }else if($(o).hasClass('mlabel') || $(o).hasClass('mtotal')){
    if($(o).hasClass('other')){
      var ref = $(o).attr('ref');
      addItem(ref, dY);
    }else{
      addItem(yval, dY);
    }
  }else if($(o).hasClass('Other')){
    addItem($(o).attr('refx'), dX);
    addItem($(o).attr('refy'), dY);
  }else if($(o).hasClass('other')){
//console.log('case 4');
    addItem(yval, dY);
    addItem($(o).attr('ref'), dX);
  }else if($(o).find('i').text().trim().length > 8){
    addItem(yval, dY);
    addItem(xval, dX);
  }
//  its = $('.tc-tab-section .tc-tab-header-selected').length ? $('.tc-tab-section .tc-tab-header-selected').attr('id') : '';
  its = $(o).closest('.tc-tab-section').find('.tc-tab-header-selected').length ? $('.tc-tab-section .tc-tab-header-selected').attr('id') : '';
  $('#'+dB).click();
  function addItem(val, v){
    $('#'+v).val(val);
  }
}
function setMatrixFilterLinks(Matrix, tid, lX, lY){
  var t = $('#'+tid);
  $(Matrix).attr('data-filterTable', tid)
           .attr('data-filterx', lX)
           .attr('data-filtery', lY);
  $(Matrix).find('th, td').each(function(i,c){
    if($(c).find('i').length || $(c).hasClass('mlabel') || $(c).hasClass('clabel'))       // check if content
      $(c).attr('onclick', 'MatrixFilter_cs(this)');
  })
}
function  MatrixFilter_cs(o) {                              // code for client-side filtering
  Component_ID_Prefix = o.Component_ID_Prefix;              // re-initiate global variable
  var Matrix = $(o).closest('table.matrixResult'),          // get matrix object
      xval = $(o).closest('table').find('thead th').eq($(o).index()).text().trim(),
      yval = $(o).closest('tr').find('td.mlabel').text().trim(),
      filtertable = Matrix.data('filtertable'),
      lX  =  Matrix.data('filterx'),
      lY  =  Matrix.data('filtery'),
      placeToBe = $('#AssetFilters ul');
  if($(o).hasClass('alltotal')){                                      // delete Filter items (show complete table data)
//    placeToBe.empty();                                              // not accepted by product management, keeping for customization
  }else if($(o).closest('thead').length || $(o).closest('tfoot').length){   // handle header and footer (add only x-axis links)
    if($(o).hasClass('mlabel') || ($(o).hasClass('mtotal'))){         // ignore labels and totals in header/footer
    }else if($(o).hasClass('other')){                                 // if multiple items
      var others = $(o).attr('ref').split(',');
      for(var i=0; i<others.length; i++){
        addItem(others[i], lX);
      }
    }else{                                                            // default
      addItem(xval, lX);
    }
  }else if($(o).hasClass('mlabel') || $(o).hasClass('mtotal')){       // handle body, starting with labels and totals
    addItem(yval, lY);
  }else if($(o).hasClass('other')){                                   // if multiple items
    addItem(yval, lY);
    var others = $(o).attr('ref').split(',');
    for(var i=0; i<others.length; i++){
      addItem(others[i], lX);
    }
  }else if($(o).find('i').text().trim().length > 8){                  // handle normal cells, ignoring blank ones
    addItem(yval, lY);
    addItem(xval, lX);
  }
  function addItem(val, col){
    if(!$(placeToBe).find('li[data-val="'+val+'"]').length){
      var item = $('<li />')
        .attr('data-mon', 'from Matrix')
        .attr('data-val', val)
        .attr('data-col', col)
        .text(val)
        .append($('<i/>')
          .addClass('icon BlueMagic cancel')
          .on('tap', function(e){
            $(this).parent().remove();
            af1(filtertable);
            e.preventDefault();
//            UpdateFilterFields();
          })
        )
      placeToBe.append(item);
    }
  }
  af1(filtertable);
//  UpdateFilterFields();
}
/* ----- End Matrix Filtering ----- */

/* ----- Start Matrix-Linking ----- */
function setMatrixPageLinks(Matrix, o){                               // Link Matrix cells to other pages with 2 parameters
  var mxblock  = $('#'+o.Component_ID_Prefix+o.mxblk);                // get Matrix Action Block containing 2 textfields and on button with edge rules
  if(!mxblock.length) return false;                                   // if no Matrix Action Block found, abort
  var param1 = mxblock.find('input').eq(0),                           // get first input field
      param2 = mxblock.find('input').eq(1),                           // get second input field
      mxbtn  = mxblock.find('button').eq(0);                          // get submit button
  Matrix.attr('data-pv1', param1.attr('id'))                          // Add the three elements to the Matrix as data attributes
        .attr('data-pv2', param2.attr('id'))
        .attr('data-act', mxbtn.attr('id'));
  $(Matrix).find('th, td').each(function(i,c){                        // loop through Matrix cells
    if($(c).find('i').length || $(c).hasClass('mlabel') || $(c).hasClass('clabel'))       // check if content
      $(c).attr('onclick', 'MatrixLink(this)');                       // add onclick to cell
  })
}
function MatrixLink(o){                                               // execute Matrix Cell Link (argument o is cell)
  var Matrix = $(o).closest('table'),                                 // get Matrix DOM object
      xval = $(o).hasClass('mtotal') ? '' :
             $(o).attr('ref') ? $(o).attr('ref') :
             Matrix.find('thead th').eq($(o).index()).text().trim(),  // get X-Value from header
      yval = $(o).parent().hasClass('totals') ? '' :
             $(o).attr('ref') ? $(o).attr('ref') :
             $(o).closest('tr').find('td.mlabel').text().trim(),      // get Y-Value from label
      pv1  = $('#'+Matrix.data('pv1')),                               // get first input field object
      pv2  = $('#'+Matrix.data('pv2')),                               // get second input field object
      act  = $('#'+Matrix.data('act'));                               // get submit button object
  pv1.val('').val(xval);                                              // set value to first text field
  pv2.val('').val(yval+'|'+Matrix.data('mxtype'));                    // set value to second text field + matrix data type (region | ccy)
  act.trigger('click');                                               // trigger submit button click
/*                                                                    // shortcut for all above :
  var Matrix = $(o).closest('table');
  $('#'+Matrix.data('pv1')).val('').val($(o).hasClass('mtotal') ? '' : $(o).attr('ref') ? $(o).attr('ref') : Matrix.find('thead th').eq($(o).index()).text().trim());
  $('#'+Matrix.data('pv2')).val('').val($(o).parent().hasClass('totals') ? '' : $(o).attr('ref') ? $(o).attr('ref') : $(o).closest('tr').find('td.mlabel').text().trim());
  $('#'+Matrix.data('act')).trigger('click');
*/
}
/* ----- End Matrix-Linking ----- */
