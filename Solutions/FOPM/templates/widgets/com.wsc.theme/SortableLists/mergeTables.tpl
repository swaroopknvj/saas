<style>
tr.bggrey {background-color:#f1f1f1;font-weight:700}
div.cindent {text-indent:1rem}
i.tc-icon.sh {vertical-align:top}
.PrimeTable .formAnswer textarea.formItem {height: auto !important; line-height: 2rem;}
td.rowSpanOnSub {background-color:#fff;}
td.rowSpanOnSub div {padding-left: 1rem;}
td.rowSpanOnSub div.header {background-color:#f1f1f1; border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; margin: -2px -1px 2px; line-height: 3rem;}
</style>
<script>
var table1 = $('.PrimeTable');
var table2 = $('.SecondaryTable');
function mergeTables(){
  table1.find('tbody tr').each(function(i,r){
    $(r).addClass('bggrey bold');
    $(r).attr('data-si', $(r).find('.si').text().trim() || $(r).find('.si').val());
    $(r).attr('data-rank', $(r).find('.rank').text().trim() || $(r).find('.rank').val());
    $(r).find('td:visible').first().find('>div').prepend($('<span class="tc-icon icon-chevron-down sh" onclick="sh(this)"><span></span></span>'));
  });
  table2.find('tbody tr').each(function(i,r){
    $(r).attr('data-id', $(r).find('.id').text().trim() || $(r).find('.id').val());
    $(r).attr('data-si', $(r).find('.si').text().trim() || $(r).find('.si').val());
    $(r).attr('data-rank', $(r).find('.rank').text().trim() || $(r).find('.rank').val());
    $(r).find('td:visible').first().find('>div').addClass('cindent')
  });
  reorderTableRows($(table1).attr('id'),'rank','asc');
  reorderTableRows($(table2).attr('id'),'rank','desc');

  table2.find('tbody tr').each(function(i,r){
    var si = $(r).data('si'),
        srow = table1.find('tbody tr[data-si="'+si+'"]').eq(0);
    srow.after($(r));
  });
  table2.hide();
  table1.find('tbody td.rowSpanOnSub').each(function(i,c){
    var rows = $(c).closest('tbody').find('tr[data-si='+$(c).closest('tr').data('si')+']').length;
    $(c).attr('rowspan', rows)
        .attr('data-rowspan', rows)
        .prepend($('<div class="header"/>').text($(c).closest('table').find('thead th').eq($(c).index()).text().trim()))
    $(c).find('textarea').attr('rows', rows)
  })
};
function sh(o){
  $(o).closest('tr').nextUntil('tr.bggrey').toggle();
  if($(o).closest('tr').find('td[rowspan]')) toggleRowSpan($(o));
  $(o).toggleClass('icon-chevron-down icon-chevron-right')
}
function toggleRowSpan(o){
  var obj = $(o).closest('tr').find('.formItem[rows]');
  obj.parent().toggle();
  var cell = obj.closest('td');
  if(cell.attr('rowspan')==1) {
    cell.attr('rowspan',cell.data('rowspan'))
  }else{
    cell.attr('rowspan',1)
  }
}
$(document).ready(function(){
  mergeTables();
//  setTimeout("mergeTables()", 200);
})
</script>