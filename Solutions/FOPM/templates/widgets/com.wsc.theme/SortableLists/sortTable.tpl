<script>
$(document).ready(function(){
  if(jQuery.ui){
    $('.sortTable').find('tbody').sortable({
      forcePlaceholderSize:true,
      handle:'a.icon-drag',
      stop: rowdrop
    })
    $('td:visible').each(function(){$(this).css('width', $(this).width() +'px')});
  }
})
function rowdrop(){
  $('.sortTable tbody tr').each(function(i,r){
    $(r).find('.rank').val(i+1);
  })
}
</script>
<style>
td a.icon-drag {cursor:n-resize}
tr.ui-sortable-helper {display: table-row;}
tr.ui-sortable-helper td {display: inline-block;}
</style>
