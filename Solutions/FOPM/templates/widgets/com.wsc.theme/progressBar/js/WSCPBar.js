var WSC_progressBar = (function() {
	var bar_id;

	function _getWrapper() {
		return $("#" + bar_id + "_wrapper");
	}

	function _moveMarker() {
        if ($("#"+bar_id+" .wsc-progressstep-current").length == 0) {
            return;
        }
        
    	var curr = $("#"+bar_id+" .wsc-progressstep-current").position().left + ($("#"+bar_id+" .wsc-progressstep-current").width() / 2) + 16;
		$("#"+bar_id+"_wrapper .wsc-progressbar-marker-wrapper").animate( {
            left: curr
        }, 600, "easeInOutCubic", function() {

            //turn into %;
            var percValue = 100 * parseInt($("#"+bar_id+"_wrapper .wsc-progressbar-marker-wrapper").css("left"), 10) / parseInt($("#"+bar_id+"_wrapper .wsc-progressbar-marker-wrapper").parent().width(), 10);
            $("#"+bar_id+"_wrapper .wsc-progressbar-marker-wrapper").css("left", percValue + "%");

            if ( !$("#"+bar_id+".wsc-progressbar-wrapper").is(".xs")) {
            	$.cookie(bar_id+"_progressBarLeftPos", percValue + "%");
            }
        });        	
    }

    function _markPBAsResponsive() {
    	var $wrapper = _getWrapper();
    	if ($(window).width() < 768 ) {
    		if (!$wrapper.is(".xs")) {
    			$wrapper.addClass("xs")
    			_redrawForXS(true);
    		}
    	} else {
    		if ($wrapper.is(".xs")) {
    			$wrapper.removeClass("xs")
    		}
   			_redrawForXS(false);
    	}
    }

    function _redrawForXS(isXS) {
        if ($("#"+bar_id+" .wsc-progressstep-current").length == 0) {
            return;
        }
        
		var newLeft = "";
		if (isXS) {
			var currentCenter = $("#"+bar_id+" .wsc-progressstep-current").position().left + $("#"+bar_id+" .wsc-progressstep-current").outerWidth() / 2;
			newLeft = $("#"+bar_id+".wsc-progressbar").width() / 2 - currentCenter;
			newLeft = (100 * (newLeft)/ parseInt($("#"+bar_id+".wsc-progressbar").width(), 10) )  + "%";
		} 
		$.cookie(bar_id+"_progressBarTextPos", newLeft);
   		$("#"+bar_id+".wsc-progressbar").animate({
   			left: newLeft 
   		}, 600, "easeInOutCubic");
    }

	function init(args) {
		bar_id = args.id;
        $("#"+bar_id+"_wrapper .wsc-progressbar-marker-wrapper").css("left", $.cookie(bar_id+"_progressBarLeftPos") );
        $("#"+bar_id+".wsc-progressbar").css("left", $.cookie(bar_id+"_progressBarTextPos") );
        $("#"+bar_id+" .wsc-progressstep").css("width", 100/($("#"+bar_id+" .wsc-progressstep:visible").length) + "%")
        _getWrapper().css("opacity", "1").removeClass("xs");

        setTimeout(function() {
        	redraw(false);
        }, 100)
	}

	function redraw(fullInd) {
		if (fullInd){
			$("#"+bar_id+" .wsc-progressstep").css("width", 100/($("#"+bar_id+" .wsc-progressstep:visible").length) + "%")
			_getWrapper().removeClass("xs")
		}

		_markPBAsResponsive();
        _moveMarker();
	}

	return {
		init: init,
		redraw: redraw
	}
})();