/* ------------------------------------------------- */
/* ----- Dynamic Views Phase 2 Column selector ----- */
/* ------------------------------------------------- */

/* ----- Analyze Table and set column data to Column Selector widget ----- */
function wsc_CardConfig(){              // call from button/icon click
  const o = event.target,
        card = $(o).closest('.tc-card'),
        t = card.find('.wsc-table');
  if(t.length && !t.find('.tc-empty-message').length) {
    newCM(card,t)
  }else{
    card.find('.nCM-container').hide();           // if empty table hide Column Selector list and Add Column dropdown
//    card.find('.wsc-config-numRows').hide();      // also hide Number of rows selector ?????
  }
}
function newCM(card,t){
  const tid = $(t).attr('id'),
        prefix = tid.split('__')[0]+'_',
        dropdown = card.find('.nCM-container .wsc-PM-CM-add');
  card.find('div.colmunger').hide();    // hide old column selector icon
  card.find('.nCM').remove();
  dropdown.empty().append($('<option class="nCM-add1"/>').text(dropdown.data('first')).attr('value',''));
  var CMcols = $('<ul class="nCM" data-table-ref="CM_'+$(card).attr('id')+'">');
  t.find('thead th:not(.col-hidden)').each(function(i,c){
    dropdown                            // add item to add-column-selectbox
      .append($('<option/>')
        .attr('value', $(c).attr('id').split('_p1_')[1]).text(getCellText(c))
        .addClass(
             !$(c).is(':visible')
          && $(c).text().trim() != ''
          && $(c).text().trim() != 'th'
          && !$(c).hasClass('wsc-col-cm-locked')
          && !$(c).hasClass('wsc-exclude-cm')
          && !$(c).hasClass('col-hidden')
          ? '' : 'tc-hidden')
      );
    addtoList(c);                                                     // add column to list
  })
  card.find('.nCM-container .widget-placeholder').append(CMcols);     // add list to DOM

/*
// start sorting Selector options
  dropdown.find('option:not(.nCM-add1)').sort(function(a, b){
    return a.text == b.text ? 0 : a.text < b.text ? -1 : 1;
  })
// end sorting Selector options
*/

  card.find('.nCM-container .wsc-PM-CM-add-button').off('click').on('click', function(){ // click on add button
//    let addval = card.find('.nCM-container .wsc-PM-CM-add').val();
    let addval = dropdown.val();
    if(addval == '') return false;
    let th = $(t).find('thead th#'+prefix+'_p1_'+addval);
    toggleCol(addval);
    colpref('save');
  })
  function getCellText(c){    // get header cell text
    return $(c).find('br').length ? $(c).find('span').eq(0).text().trim() :
           $(c).find('div').length > 1 ? $(c).find('div').eq(0).text().trim() :
           $(c).text().trim();
  }
  function addtoList(c){      // generate column list items
    CMcols.append(
      $('<li>')
        .attr('data-ref', $(c).attr('id').split('_p1_')[1])
        .hover(function(){t.find('th#'+$(c).attr('id')).addClass('nCMhover')},function(){t.find('th#'+$(c).attr('id')).removeClass('nCMhover')})
        .addClass($(c).is(':visible') ? '' : 'tc-hidden')
        .addClass($(c).text().trim() != '' && $(c).text().trim() != 'th' && !$(c).hasClass('wsc-exclude-cm') ? '' : 'tc-hidden')
        .addClass($(c).hasClass('wsc-col-cm-locked') ? 'locked' : '')
        .append($('<span/>').text(getCellText(c)))
        .prepend($('<span/>').addClass('tc-icon icon-'+($(c).hasClass('wsc-col-cm-locked')?'lock':'drag')).append($('<span/>')))
        .append($('<span/>').addClass('tc-icon tc-float-right'+($(c).hasClass('wsc-col-cm-locked')?'':' icon-trash')).append($('<span/>')).on('click', function(){removeCol(this)}))
    )
  }
  function toggleCol(c){
    t.find('tr').each(function(j,r){$(r).find('th[id*='+c+'], td[id*='+c+']').toggle()});           // toggle column visibility by id
    $(card).find('.nCM-container .wsc-PM-CM-add option[value='+c+']').toggleClass('tc-hidden');     // toggle select option
    $(card).find('.nCM-container .nCM li[data-ref='+c+']').toggleClass('tc-hidden');                // toggle list item
    $(card).find('.nCM-container .wsc-PM-CM-add').val('');                                          // clear select options
  }
  function removeCol(o){      // remove column on trash icon click
    toggleCol($(o).closest('li').data('ref'));
    colpref('save')
  }
  function moveCol(tg,cid){   // tg = next target columnID / cid = columnID to move
    $(t).find('tr').each(function(i,r){$(r).find('th[id*='+tg+'], td[id*='+tg+']').before($(r).find('th[id*='+cid+'], td[id*='+cid+']'))});
    colpref('save')
  }
  function colpref(act){
    const ColPref = card.find('.CM-colpref');
    let tcolsS = ColPref.val(),
        cpArray = [];
    switch(act){
      case 'save':
        $(t).find('th:visible').each(function(i,th){cpArray.push($(th).attr('id').split('_p1_')[1])});
        ColPref.val(cpArray.join(','));
        break;
      case 'read':
        return ColPref.val();
        break;
      default:
        return false;
    }
  }
  $('ul.nCM').sortable({
    items: 'li:not(.locked)',
    handle: 'span.icon-drag',
    forcePlaceholderSize: true
  })
  .bind('sortupdate', function(e, ui){
    moveCol(ui.item.next().data('ref'), ui.item.data('ref'));
  });
//  card.find('.PM-config-flyout').draggable({handle: card.find('.PM-config-flyout-header')});
}

/* ------------------------------------------- */
/* ----- Apply user pref column settings ----- */
/* ------------------------------------------- */
function init_nCM_Cols(){
  $('.PM-config-flyout .CM-colpref').each(function(i,c){
    let ColprefVal = $(c).val().split(',');
    if(ColprefVal.length < 2) return false;
    let card = $(c).closest('.tc-card'),
        t = card.find('table.wsc-table'),
        oCols = getAllColsArray($(t).attr('id'));
    for(var idx=0; idx < oCols.length; idx++){
      t.find('tr').each(function(ii,r){$(r).find('th[id*='+oCols[idx]+'], td[id*='+oCols[idx]+']').hide()});  // hide all columns
      var pos = $.inArray(oCols[idx], ColprefVal)
      if(pos >= 0){
        t.find('tr').each(function(ii,r){$(r).find('th[id*='+ColprefVal[pos]+'], td[id*='+ColprefVal[pos]+']').show()});// show only columns in saved Array
      }
    }
    for(let j = 0; j < ColprefVal.length-1; j++){ // parse saved list
      $(t).find('tr').each(function(k,r){$(r).find('th, td').eq(j+1).before($(r).find('th[id*='+ColprefVal[j]+'], td[id*='+ColprefVal[j]+']'))}); // rearrange visible columns
    }
  })
}
let getAllColsObj = function(tid){
  var AllCols = {};
  $('#'+tid).find('thead th').each(function(i,th){AllCols[$(th).attr('id').split('_p1_')[1], $(th).text().trim(), $(th).is(':visible'),th.className]})
  return AllCols;
}
let getAllColsArray = function(tid){
  var AllCols = [];
  $('#'+tid).find('thead th').each(function(i,th){AllCols.push($(th).attr('id').split('_p1_')[1])})
  return AllCols;
}

Hi.addHook('postProcessResponses', init_nCM_Cols);
$(document).ready(function(){
  init_nCM_Cols();
})