function conversationView(t) {
    var alphaClassArray = [];
    var subjectArray = [];
    var count = 0;
    if (!t.length) {
        setTimeout("conversationView(t)", 100)
    } // delay for MSIE
    $(t).addClass('conversationView');

    var column = '2',
        oldchar = '';
    $(t).find('.alphaClass').remove();
    $(t).find('tbody tr').each(function() {
        var char = $(this).find('td').eq(column).text().trim();

        var split = char.split("RE:");
        var result = split[split.length - 1];
		
		
		
        if (result.trim() != oldchar.trim()) {
			var partId = $(this).attr("id").substring($(this).attr("id").indexOf("_R"))
            $(this)
                .before($('<tr id="dummy' + partId + '"></tr>')
                    .addClass('alphaClass')
                    .append(
						$('<td class="tc-table-answer"><div class="tc-float-left CheckBoxIcon tc-checkbox"><input  class="parentClass  tc-form-control tc-checkbox-horiz tc-table-icon tc-icon-checkbox" name="parentCheckBox" type="checkbox" id="alphaClass' + partId + '" onclick="childCheck(this)"></div></td>')
						.prepend($('<div style="float: left;" class="messagearrow"><a class="collapseClass" onclick="collapseRow(this)"><i class="tc-no-prop tc-icon tc-table-icon icon-chevron-right"><span></span></i></a></div>')))
                    .append($('<td class="from"/>'))
                    .append($('<td class="subject" style="padding:.3rem .5rem;"/>')
                        .attr('colspan', $(this).find('td').length)
                        .text(result)
                    )
                )
            subjectArray.push(result);
            if (count != 0)
                alphaClassArray.push(count);
            count = 0;
        }
        count += 1;
        oldchar = result;
    });
	
    alphaClassArray.push(count);
    var hideFlag = 0;
    $(t).find('tbody tr').each(function() {
        $(this).show();
        var sentFlag = $(this).find('td').eq(4).text().trim();
        if ($(this).hasClass('alphaClass')) {
            var subjectText = $(this).closest("tr") // Finds the closest row <tr> 
                .find(".subject")
                .text();
            var i;
            for (i = 0; i < subjectArray.length; ++i) {
                if (subjectText == subjectArray[i] && alphaClassArray[i] == 1) {					
                    $(this).hide();
                    hideFlag = 1;
                    break;
                } else {
                    hideFlag = 0;
                }
            }
        } else {
            if (hideFlag == 0 || sentFlag == 'True') {
                $(this).not('.alphaClass').hide();
            }
        }
    });
    $(t).find('thead tr th').each(function() {
        $(this).removeAttr('onclick')
    });
}

function collapseRow(o) {
    var selectedcolumn = $(o), //selected column
        selectedicon = $(o).children(), //chevron icon
        parentrow = $(o).parent().parent().parent(); //parent row
    $(selectedicon).toggleClass('icon-chevron-down').toggleClass('icon-chevron-right');
    $(parentrow).nextAll('tr').each(function() {
        if ($(this).hasClass('alphaClass')) {
            return false;
        }
        $(this).toggle();
    });
}

function childCheck() {
    $('.parentClass').change(function() {
        if ($(this).is(":checked")) {
            var trRef = $(this).closest('tr');
            $(trRef).nextAll('tr').each(function() {
                $(this).closest('tr').find('input[type="checkbox"]').each(function() {
                    if ($(this).hasClass('parentClass')) {
                        return false
                    } else {
                        $(this).prop("checked", true);
                    }
                });
                if ($(this).hasClass('alphaClass')) {
                    return false;
                }
            });
            checkBoxIcons();
        } else {
            var trRef = $(this).closest('tr');
            $(trRef).nextAll('tr').each(function() {
                $(this).closest('tr').find('input[type="checkbox"]').each(function() {
                    if ($(this).hasClass('parentClass')) {
                        return false
                    } else {
                        $(this).prop("checked", false);
                    }
                });
                if ($(this).hasClass('alphaClass')) {
                    return false;
                }
            });
            checkBoxIcons();
        }
    });
}

function checkBoxSelectAll(t){
    $(t).find('tbody tr input[type="checkbox"]').each(function() {
        $(this).prop("checked", true);
    }); 
}

function checkBoxIcons(o){
  var selector = false;
	selector = o ? $(o).closest('.CheckBoxIcon') : $('.tc-table.conversationView');
    selector.each(function(i,o){
      var checkbox = $(o).find('input[type="checkbox"]');
      if($(checkbox).is(':checked')) $(o).addClass('checked');
      else $(o).removeClass('checked');
      $(checkbox).on('change', function(){
        if($(this).is(':checked')) $(this).parent().addClass('checked');
        else $(this).parent().removeClass('checked');
      })
  });
}

