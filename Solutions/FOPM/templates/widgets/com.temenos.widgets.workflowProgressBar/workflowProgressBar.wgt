<style type="text/css">

/* Form progressBar */
.progressBar {
  font-family: "Roboto" !important;	
  font-size: 14px;
  width: 100%;
  list-style: none;
  list-style-image: none;
  align:left;
}

.progressBar .bar,
.progressBar .rightBar,
.progressBar .leftBar {
  display: inline-block;
  background: #e6e6e6;
  width: 92px; height: 32px;
  border: 0px solid #b6b6be;
}

.progressBar .rightBar {
  width: 8px;
  height: 32px;
  margin: 0px -5px -13px -5px;
  border-top: 15px solid white;
  border-bottom: 15px solid white;
  border-left: 7px solid #e6e6e6;
}

.progressBar .leftBar {
  width: 8px;
  height: 32px;
  margin: 0px -4px -13px 0px;
  border-top: 14px solid #e6e6e6;
  border-bottom: 14px solid #e6e6e6;
  border-left: 7px solid white;
}

.progressBar .bar .label {
  display: inline-block;
  width: 75px;
  height: 25px;
  line-height: 25px;
  border-radius: 0px;
  color: #444444;
  font-size: 12px;
  text-align:middle;
  font-family: "Roboto" !important;	  
  font-weight:normal; 
}

/* Done / Active */
.progressBar .rightBar.done,
.progressBar .leftBar.done,
.progressBar .bar.done {
  background: #b6b6be;
}

.progressBar .leftBar.done {
  margin: 0px -4px -13px 0px;
  border-top: 14px solid #b6b6be;
  border-bottom: 14px solid #b6b6be;
  border-left: 7px solid white;
}

.progressBar .rightBar.done {
  margin: 0px -5px -13px -5px;
  border-top: 15px solid white;
  border-bottom: 15px solid white;
  border-left: 7px solid #b6b6be;
}

.progressBar .bar.done .label {
  color: #fff;
  background: #b6b6be;
  font-weight:bold;  
}

.progressBar .bar.done .title {
  color: #b6b6be;
}

.progressBar .rightBar.active,
.progressBar .leftBar.active,
.progressBar .bar.active {
  background: #7aa2c7;
}

.progressBar .bar.active .label {
  color: #fff;
  background: #7aa2c7;
}

.progressBar .bar.active .title {
  color: #7aa2c7;
  font-weight:bold; 
}

.progressBar .leftBar.active {
  margin: 0px -4px -13px 0px;
  border-top: 14px solid #7aa2c7;
  border-bottom: 14px solid #7aa2c7;
  border-left: 7px solid white;
}

.progressBar .rightBar.active {
  margin: 0px -5px -13px -5px;
  border-top: 15px solid white;
  border-bottom: 15px solid white;
  border-left: 7px solid #7aa2c7;
}

.progressBar .bar.leftRadius {
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
}

.progressBar .bar.rightRadius {
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
}

.progressInfoBar {
    visibility: hidden;
	opacity:0.9;
	background: #F2F2F2;
	padding: 2px;
	border-radius: 1px;
	border: 0.3px solid rgb(195, 215, 230);
    position: absolute;
    z-index: 1;
	text-align:left;
	box-shadow: 1px 1px 1px rgb(195, 215, 229) ;
	font-size:12px;
	color:#000;
	font-family: "Roboto" !important;
}

.progressBar g:hover div {
    visibility: visible;
}

.progressBar g div {
   display: inline-block;
	/*pointer-events: none;*/
	pointer-events: auto;
    color: #444444;
    line-height: 15px;
    margin: 0;
}
	
</style>

<!-- Labels to show them in information bar for each stage. 5 attributes can be configured. -->

$%IF ITEM.LABEL_FOR_INFO1 != ''$
	$%SET LABEL1 := ITEM.LABEL_FOR_INFO1$ $%ENDSET$
$%ENDIF$
$%IF ITEM.LABEL_FOR_INFO2 != ''$
	$%SET LABEL2 := ITEM.LABEL_FOR_INFO2$ $%ENDSET$
$%ENDIF$
$%IF ITEM.LABEL_FOR_INFO3 != ''$
	$%SET LABEL3 := ITEM.LABEL_FOR_INFO3$ $%ENDSET$
$%ENDIF$
$%IF ITEM.LABEL_FOR_INFO4 != ''$
	$%SET LABEL4 := ITEM.LABEL_FOR_INFO4$ $%ENDSET$
$%ENDIF$
$%IF ITEM.LABEL_FOR_INFO5 != ''$
	$%SET LABEL5 := ITEM.LABEL_FOR_INFO5$ $%ENDSET$
$%ENDIF$

<!-- Suffix will be displayed in information bar for each label. Suffix can be configured for 5 attributes. -->

$%IF ITEM.SUFFIX_FOR_INFO1 != ''$
	$%SET SUFFIX1 := ITEM.SUFFIX_FOR_INFO1$ $%ENDSET$
$%ENDIF$
$%IF ITEM.SUFFIX_FOR_INFO2 != ''$
	$%SET SUFFIX2 := ITEM.SUFFIX_FOR_INFO2$ $%ENDSET$
$%ENDIF$
$%IF ITEM.SUFFIX_FOR_INFO3 != ''$
	$%SET SUFFIX3 := ITEM.SUFFIX_FOR_INFO3$ $%ENDSET$
$%ENDIF$
$%IF ITEM.SUFFIX_FOR_INFO4 != ''$
	$%SET SUFFIX4 := ITEM.SUFFIX_FOR_INFO4$ $%ENDSET$
$%ENDIF$
$%IF ITEM.SUFFIX_FOR_INFO5 != ''$
	$%SET SUFFIX5 := ITEM.SUFFIX_FOR_INFO5$ $%ENDSET$
$%ENDIF$

<!-- Final Stage can be displayed as "Final" instead of the stage description. This is configurable. -->
$%IF ITEM.FINAL_STAGE_TEXT != ''$
	$%SET FINAL_STAGE_TXT := ITEM.FINAL_STAGE_TEXT$ $%ENDSET$
$%ENDIF$

$%SET STAGES := ITEM.NUMBER_OF_ROWS()$ $%ENDSET$

</br>

<div class="progressBar">
	$%FOREACH TABLE_ROW IN TABLE$
		$%IF TABLE_ROW.ROW_NUM() <= ITEM.NUMBER_OF_ROWS()$		
			$%IF TABLE_ROW.ROW_NUM() == ITEM.NUMBER_OF_ROWS()$
				$%SET FINALSTAGE := WM[1].Entities[1].wm_ProgressBarStageDetails[C].stage$ $%ENDSET$
			$%ENDIF$
		
			$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "COMPLETED" || WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "CURRENT"$
				$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].stage != 1$
					<span class="leftBar 				
						$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "COMPLETED"$done$%ENDIF$  
						$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "CURRENT"$active$%ENDIF$ "></span>
				$%ENDIF$
			$%ENDIF$
			$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status = ""$
				$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].stage != 1$
					<span class="leftBar"></span>
				$%ENDIF$
			$%ENDIF$

				<g style="cursor:pointer;">
				<div class="bar 
				$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "COMPLETED"$done$%ENDIF$  
				$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "CURRENT"$active$%ENDIF$
				$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].stage == 1$leftRadius$%ENDIF$
				$%IF ("##WM[1].Entities[1].wm_ProgressBarStageDetails[C].stage#") == "##!STAGES#"$rightRadius$%ENDIF$	" >
				<span class="label" >
					$%FOREACH TABLE_CHILD IN TABLE$
						$%IF TABLE_CHILD.CHILD_INDEX() == 0$
							$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "COMPLETED"$	
								&#10003;
							$%ELSE$
								$%IF ("##WM[1].Entities[1].wm_ProgressBarStageDetails[C].stage#") == "##!STAGES#" && !FINAL_STAGE_TXT != ""$ 						
									$$!FINAL_STAGE_TXT$
								$%ELSE$									
									$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].shortDescription != ""$ 						
										$$WM[1].Entities[1].wm_ProgressBarStageDetails[C].shortDescription$
									$%ELSE$
										$$WM[1].Entities[1].wm_ProgressBarStageDetails[C].description$
									$%ENDIF$									
								$%ENDIF$				
							$%ENDIF$
						$%ENDIF$
					$%ENDFOREACH$				
				</span>
				</div>

				<div id="FMT_STAGE_INFO$$TABLE_ROW.ROW_NUM()$" class="progressInfoBar">
					$%FOREACH TABLE_CHILD IN TABLE$
						$%IF TABLE_CHILD.CHILD_INDEX() == 0 $
							<b>$$WM[1].Entities[1].wm_ProgressBarStageDetails[C].description$</b></br>							
							$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].info1 != "" $
								$%IF !LABEL1 != '' && WM[1].Entities[1].wm_ProgressBarStageDetails[C].info1 != "" $
									$$!LABEL1$:
										<b>$$WM[1].Entities[1].wm_ProgressBarStageDetails[C].info1$											
										$%IF !SUFFIX1 != '' $
											$$!SUFFIX1$
										$%ENDIF$
										</b></br>
									$%ENDIF$
									$%IF !LABEL2 != '' && WM[1].Entities[1].wm_ProgressBarStageDetails[C].info2 != ""$
										$$!LABEL2$:
										<b>$$WM[1].Entities[1].wm_ProgressBarStageDetails[C].info2$
											$%IF !SUFFIX2 != '' $
												$$!SUFFIX2$
											$%ENDIF$												
										</b></br>
									$%ENDIF$
									$%IF !LABEL3 != '' && WM[1].Entities[1].wm_ProgressBarStageDetails[C].info3 != ""$
										$$!LABEL3$:
										<b>$$WM[1].Entities[1].wm_ProgressBarStageDetails[C].info3$
										$%IF !SUFFIX3 != '' $
											$$!SUFFIX3$
										$%ENDIF$												
										</b></br>
									$%ENDIF$
									$%IF !LABEL4 != '' && WM[1].Entities[1].wm_ProgressBarStageDetails[C].info4 != ""$
										$$!LABEL4$:
										<b>$$WM[1].Entities[1].wm_ProgressBarStageDetails[C].info4$
										$%IF !SUFFIX4 != '' $
											$$!SUFFIX4$
										$%ENDIF$												
										</b></br>
									$%ENDIF$
									$%IF !LABEL5 != '' && WM[1].Entities[1].wm_ProgressBarStageDetails[C].info5 != ""$
										$$!LABEL5$:
										<b>$$WM[1].Entities[1].wm_ProgressBarStageDetails[C].info5$
										$%IF !SUFFIX5 != '' $
											$$!SUFFIX5$
										$%ENDIF$												
										</b></br>
									$%ENDIF$
								$%ENDIF$
						$%ENDIF$						
					$%ENDFOREACH$
				</div>
			$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "COMPLETED" || WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "CURRENT"$
				$%IF ("##WM[1].Entities[1].wm_ProgressBarStageDetails[C].stage#") != "##!STAGES#"$
					<span class="rightBar
						$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "COMPLETED"$done$%ENDIF$  
						$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == "CURRENT"$active$%ENDIF$ "></span>
				$%ENDIF$
			$%ENDIF$
			$%IF WM[1].Entities[1].wm_ProgressBarStageDetails[C].status == ""$
				$%IF ("##WM[1].Entities[1].wm_ProgressBarStageDetails[C].stage#") != "##!STAGES#"$
					<span class="rightBar"></span>
				$%ENDIF$
			$%ENDIF$				
			</g>		
		$%ENDIF$

	$%ENDFOREACH$
</div>
</br>