<script type="text/javascript">
$(function(){
  Highcharts.setOptions({
	  colors: ['#3498db','#e74c3c','#34495e','#95a5a6','#1abc9c','#f1c40f','#e67e22','#9b59b6','#2ecc71','#bdc3c7','#c0392b','#2980b9','#f39c12','#16a085']
  });
  $('#$$ITEM.ID()$_chart').highcharts({
    chart: {
      renderTo: '$$ITEM.ID()$_chart',
      borderWidth: 0,
      zoomType: 'xy',
	  inverted: 'true'
    },
    credits: {
      enabled: false
    },

   legend: {
      itemHoverStyle: {
         color: '#CCC'
      },
      itemHiddenStyle: {
         color: '#CCC'
      }
    },
    title: {text: '$$ITEM.Title$'},
    subtitle: {text: '$$ITEM.SubTitle$'},
    $%IF ITEM.CustomTT == "Y"$
    tooltip:{
      shared: true,
      formatter: function() {
        var key = "";
        if (Array.isArray(this.points)) {
          var TtTitle1 = this.points[0].point.TtTitle1;
          var TtTitle2 = this.points[0].point.TtTitle2;
          var TtTitle3 = this.points[0].point.TtTitle3;
          var TtTitle4 = this.points[0].point.TtTitle4;
          var TT1 = this.points[0].point.TT1;
          var TT2 = this.points[0].point.TT2;
          var TT3 = this.points[0].point.TT3;
          var TT4 = this.points[0].point.TT4;
        } else {
          var TtTitle1 = this.point.TtTitle1;
          var TtTitle2 = this.point.TtTitle2;
          var TtTitle3 = this.point.TtTitle3;
          var TtTitle4 = this.point.TtTitle4;
          var TT1 = this.point.TT1;
          var TT2 = this.point.TT2;
          var TT3 = this.point.TT3;
          var TT4 = this.point.TT4;
         }
        if(typeof TtTitle1 !== 'undefined') {
           TtTitle1=TtTitle1.replace('&#x2F;', '/');
          key = key + '<b>' + TtTitle1 +'</b>';
        }
        if(typeof TT1 !== 'undefined') {
          key = key + ': ' + TT1;
        }
        if(typeof TtTitle2 !== 'undefined') {
          key = key + '<br/>' + TtTitle2;
        }
        if(typeof TT2 !== 'undefined') {
          key = key + ': <b>' + TT2 + '</b><br/>';
        }
        if(typeof TtTitle3 !== 'undefined') {
          key = key + '<br/>' + TtTitle3;
        }
        if(typeof TT3 !== 'undefined') {
          key = key + ': <b>' + TT3 + '</b><br/>';
        }
        if(typeof TtTitle4 !== 'undefined') {
          key = key + '<br/>' + TtTitle4;
        }
        if(typeof TT4 !== 'undefined') {
          key = key + ': <b>' + TT4 + '</b><br/>';
        }
        return '<span style="font-family: Roboto;">'+key+'</span>';
      }
    },
    $%ENDIF$
	xAxis: {
	  gridLineColor: '#888888',
	  gridLineWidth: 0,
	  title: {text: '$$ITEM.XAxisTitle$'},
	  categories: [$%FOR 1 to ITEM.NoOfData ITEM.XAxisDataGroup$
		  '$$ITEM.XAxisData$',
		  $%ENDFOR$]
	},
	yAxis: {
	  gridLineColor: '#888888',
	  gridLineWidth: 1,
	  minorGridLineWidth: 0,
	  gridLineDashStyle: 'Dot',
	  title: {
  		text: '$$ITEM.YAxisTitle$',
  		style: {size:  '$$ITEM.YAxisMarkerFontSize$px'},
	  },
	  plotLines: [{
		value: 0,
  		width: 1,
  		color: '#808080',
		dashStyle: 'Dot'
	  }],
	  format: '{value} %',
	  labels: {
			y: 20,
	  }
	},
    series: [{
      name: '$$ITEM.YAxisLegend1$',
      type: '$$ITEM.YAxisType1$',
      marker: {
      	radius: 8,
      	lineWidth: 1,
      	lineColor: '#FFFFFF'
      },
      data: [$%FOR 1 to ITEM.NoOfData ITEM.XAxisDataGroup$
			{
        name: '$$ITEM.XAxisData$',
        y: $$ITEM.YAxisData1$,
        $%IF ITEM.CustomTT == "Y" $
          $%IF ITEM.TtTitle1 != "" $
            TtTitle1:'$$ITEM.TtTitle1$',
            $%IF ITEM.TT1 != "" $
              TT1:'$$ITEM.TT1$',
            $%ENDIF$
          $%ENDIF$

          $%IF ITEM.TtTitle2 != "" $
            TtTitle2:'$$ITEM.TtTitle2$',
            $%IF ITEM.TT2 != "" $
              TT2:'$$ITEM.TT2$',
            $%ENDIF$
          $%ENDIF$

          $%IF ITEM.TtTitle3 != "" $
            TtTitle3:'$$ITEM.TtTitle3$',
            $%IF ITEM.TT3 != "" $
              TT3:'$$ITEM.TT3$',
            $%ENDIF$
          $%ENDIF$

          $%IF ITEM.TtTitle4 != "" $
            TtTitle4:'$$ITEM.TtTitle4$',
            $%IF ITEM.TT4 != "" $
              TT4:'$$ITEM.TT4$',
            $%ENDIF$
          $%ENDIF$
        $%ENDIF$
      },
			$%ENDFOR$],
    }, {
      type: 'errorbar',
      stemWidth: 2,
      stemColor: '#444',
      whiskerWidth: 2,
      whiskerColor: '#444',
      data: [$%FOR 1 to ITEM.NoOfData ITEM.XAxisDataGroup$
			[$$ITEM.YAxisErrorMin1$, $$ITEM.YAxisErrorMax1$],
			$%ENDFOR$],
    }, {
      name: '$$ITEM.YAxisLegend2$',
      type: '$$ITEM.YAxisType2$',
      marker: {
      	radius: 6,
      	lineWidth: 1,
      	lineColor: '#FFFFFF'
      },
	  data: [$%FOR 1 to ITEM.NoOfData ITEM.XAxisDataGroup$
			{
        name: '$$ITEM.XAxisData$',
        y: $$ITEM.YAxisData2$,
        $%IF ITEM.CustomTT == "Y" $
          $%IF ITEM.TtTitle1 != "" $
            TtTitle1:'$$ITEM.TtTitle1$',
            $%IF ITEM.TT1 != "" $
              TT1:'$$ITEM.TT1$',
            $%ENDIF$
          $%ENDIF$

          $%IF ITEM.TtTitle2 != "" $
            TtTitle2:'$$ITEM.TtTitle2$',
            $%IF ITEM.TT2 != "" $
              TT2:'$$ITEM.TT2$',
            $%ENDIF$
          $%ENDIF$

          $%IF ITEM.TtTitle3 != "" $
            TtTitle3:'$$ITEM.TtTitle3$',
            $%IF ITEM.TT3 != "" $
              TT3:'$$ITEM.TT3$',
            $%ENDIF$
          $%ENDIF$

          $%IF ITEM.TtTitle4 != "" $
            TtTitle4:'$$ITEM.TtTitle4$',
            $%IF ITEM.TT4 != "" $
              TT4:'$$ITEM.TT4$',
            $%ENDIF$
          $%ENDIF$
        $%ENDIF$
      },
			$%ENDFOR$],
	}]
  });
});
</script>
<div id="$$ITEM.ID()$_chart" style="min-width: $$ITEM.Width$; height: $$ITEM.Height$; margin: 0 auto"></div>
