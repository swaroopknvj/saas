function conversationView(t) {
    var alphaClassArray = [];
    var subjectArray = [];
    var count = 0;
    if (!t.length) {
        setTimeout("conversationView(t)", 100)
    } // delay for MSIE
    $(t).addClass('conversationView');
    var column = '2',
        oldchar = '';
    $(t).find('.alphaClass').remove();
    $(t).find('tbody tr').each(function() {
        var char = $(this).find('td').eq(column).text().trim();
        var split = char.split("RE:");
        var result = split[split.length - 1];
        if (result.trim() != oldchar.trim()) {
            $(this)
                .before($('<tr/>')
                    .addClass('alphaClass')
                    .append($('<td class="List formAnswer bool"><div class="formItem CheckBoxIcon"><input id="parentCheckBox" class="parentClass" type="checkbox" onclick="childCheck(this)" name="parentCheckBox"/></div></td>')
                        .prepend($('<span class="messagearrow"><a class="collapseClass" onclick="collapseRow(this)"><i class="icon BlueMagic right "></i></a></span>')))
                    .append($('<td class="from"/>'))
                    .append($('<td class="subject"/>')
                        .attr('colspan', $(this).find('td').length)
                        .text(result)
                    )
                )
            subjectArray.push(result);
            if (count != 0)
                alphaClassArray.push(count);
            count = 0;
        }
        count += 1;
        oldchar = result;
    });
    alphaClassArray.push(count);
    var hideFlag = 0;
    $(t).find('tbody tr').each(function() {
        $(this).show();
        var sentFlag = $(this).find('td').eq(4).text().trim();
        if ($(this).hasClass('alphaClass')) {
            var subjectText = $(this).closest("tr") // Finds the closest row <tr> 
                .find(".subject")
                .text();
            var i;
            for (i = 0; i < subjectArray.length; ++i) {
                if (subjectText == subjectArray[i] && alphaClassArray[i] == 1) {
                    $(this).hide();
                    hideFlag = 1;
                    break;
                } else {
                    hideFlag = 0;
                }
            }
        } else {
            if (hideFlag == 0 || sentFlag == 'True') {
                $(this).not('.alphaClass').hide();
            }
        }
    });
    $(t).find('thead tr th').each(function() {
        $(this).removeAttr('onclick')
    });
}

function collapseRow(o) {
    var selectedcolumn = $(o), //selected column
        selectedicon = $(o).children(), //chevron icon
        parentrow = $(o).parent().parent().parent(); //parent row
    $(selectedicon).toggleClass('down').toggleClass('right');
    $(parentrow).nextAll('tr').each(function() {
        if ($(this).hasClass('alphaClass')) {
            return false;
        }
        $(this).toggle();
    });
}

function childCheck() {
    $('.parentClass').change(function() {
        if ($(this).is(":checked")) {
            var trRef = $(this).closest('tr');
            $(trRef).nextAll('tr').each(function() {
                $(this).closest('tr').find('input[type="checkbox"]').each(function() {
                    if ($(this).hasClass('parentClass')) {
                        return false
                    } else {
                        $(this).prop("checked", true);
                    }
                });
                if ($(this).hasClass('alphaClass')) {
                    return false;
                }
            });
            checkBoxIcons();
        } else {
            var trRef = $(this).closest('tr');
            $(trRef).nextAll('tr').each(function() {
                $(this).closest('tr').find('input[type="checkbox"]').each(function() {
                    if ($(this).hasClass('parentClass')) {
                        return false
                    } else {
                        $(this).prop("checked", false);
                    }
                });
                if ($(this).hasClass('alphaClass')) {
                    return false;
                }
            });
            checkBoxIcons();
        }
    });
}

function checkBoxSelectAll(t){
    $(t).find('tbody tr input[type="checkbox"]').each(function() {
        $(this).prop("checked", true);
    }); 
}