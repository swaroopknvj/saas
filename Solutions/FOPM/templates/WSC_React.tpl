<!DOCTYPE html>
<html dir="$$PRESENTATIONTYPE.LAYOUTDIRECTION$"
      xmlns="http://www.w3.org/1999/xhtml"
      xml:lang="en"
      class="$$!FOOTER_LOCKED$ $$!HEADER_LOCKED$ $$!BOX_VIEW$ $$!FULL_SCREEN$ $$!SHOW_SIDEBAR$ $$!FONT_SIZE$ $$!DENSITY$ $$!HIGH_CONTRAST$ tc-show-breakpoint-hint tc-show-grid ">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="author" content="Temenos"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Temenos WealthSuite</title>
    <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon-32x32.png">
    <link rel="stylesheet" href="./templates/widgets/com.temenos.theme/jquery-ui/css/Temenos/jquery-ui-1.10.4.custom.css" />
    <link id="override_css" type="text/css" rel="stylesheet" href="html/css/StyleOverridesWSC.css" media="screen"/>
    
    <script type="text/javascript">
        var LANGUAGE = "$$LANGUAGE_MAP_ALIAS$";
        var SelectedSubMenu = "$$TAP[1].WorkingElements[1].SelectedSubMenu$";
        var SelectedTab = "$$TAP[1].WorkingElements[1].SelectedSearchTabs$";
        var tSep = "$$PRESENTATIONTYPE.DIGITGROUPSYMBOL$";
        var dSep = "$$PRESENTATIONTYPE.DECIMALSYMBOL$";
        var profurl = "ClientPerformanceResult";
        var correlationId = "$%XCORRELATIONID$";
        var theme = "$$THEME$";
      </script>

      $$HEADCONTENT$
      <link rel="stylesheet" href="./html/css/WSC_CSR.css" />

      <!-- loading chosen here - should it be a theme resource? -->
      <script src="./templates/widgets/com.temenos.widgets.jquery/chosen/chosen.jquery.min.js" type="text/javascript" ></script>
  </head>
  <body class="BrowserWindow CSR $$THEME$" >
    <link rel="stylesheet" href="./html/css/T-BaseReactExt.css" />
    <form name="form1" id="form1" method="post" action="servletcontroller"></form>
      $$FORMCONTENT$


    <script src="./html/js/widgets/com.temenos.theme/temenos-theme.js" type="text/javascript" ></script>
    <script src="./html/js/widgets/wsc/wsc.js" type="text/javascript" ></script>

    $%INCLUDE SessionTimeout.tpl$ $%ENDINCLUDE$
    <script type="text/javascript"> 
      function postProcessResponse(store) {
          const phase = store.getMetaValue("phase", "", "")
          phase != "PreLogin.Login" &&
            TimeOutManager.configure({ 
              sessionTimeout:       "$$SESSIONTIMEOUT$",
              dialogClasses:        "tc-global-font tc-global-color tc-normal-weight",
              dialogStyles:         "padding: 0 20px 20px",
              dialogTitleClasses:   "tc-card-header tc-card-title tc-fs-p2 tc-bold ecDIB",
              dialogTitleStyles:    "margin-bottom: 10px; width: calc(100% + 40px); margin-left: -20px;",
              dialogRefreshClasses: "tc-primary-bg tc-button-color tc-button tc-rounded-1 tc-uppercase tc-normal-icon-with-text col-full-xs",
              dialogRefreshStyles:  "border: 0; color: white",
              dialogCancelText:     "Logout",
              dialogCancelClasses:  "tc-button tc-button-tertiary tc-rounded-1 tc-uppercase tc-normal-icon-with-text",
              dialogTimeoutClasses: "tc-light tc-bold tc-fs-p2 tc-center-align",
              animationDuration:    1
            })
      } 
    </script>
  </body>
</html>
