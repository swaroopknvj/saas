﻿<ul class="monlist"></ul>
<script>
  $(document).ready(function(){
    init_tableFilterItems('$$COMPONENT_ID_PREFIX$TBL_MONITORING', 'cat', 'hp_monCat', 'catlist')
    init_tableFilterItems('$$COMPONENT_ID_PREFIX$TBL_MONITORING', 'mon', 'hp_monItem', 'monlist')

    if (requestParam.length && requestValue.length) {
      var params = requestParam.split('|');
      $(".monlist li.tableFilterItem[data-col='"+params[1]+"'][data-moni='"+requestValue+"']").trigger('tap');
    }
  });
</script>
