﻿<pdf baseFont="Helvetica,Cp1252,false" charset=UTF-8>
<!-- Homepage -->
<page>
<footer page="y"></footer>
<right><img src="$$PROJECTHOME$/images/modelbank/brand/TemenosLogo.jpg" width="100" height="35"></img>
<center>
<br />
<br />
$%if LANGUAGE_MAP_ALIAS == 'English'$
<font size="15">Your Portfolios</font>
$%else$
<font size="15">Sommaire des actifs</font>
$%endif$
<br />
<br />
<font size="8">
$%if TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].lastInstance() > 0$
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
  <tr>
$%if LANGUAGE_MAP_ALIAS == 'English'$
    <td bgcolor="DBE5F1"><b>Portfolio Code<br>Portfolio Name</b></td>
  	<td bgcolor="DBE5F1"><b>Client</b></td>
  	<td bgcolor="DBE5F1"><b>Service Type<br>Management Type</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Market Value<br>Profit / Loss</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Cash Portion<br>Performance</b><br></td>
$%else$
    <td bgcolor="DBE5F1"><b>Code de portefeuille<br>Nom</b></td>
  	<td bgcolor="DBE5F1"><b>Client</b></td>
  	<td bgcolor="DBE5F1"><b>Type de service<br>Type de gestion</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Valeur de marché<br>Gain / Perte</b><br></td>
  	<td bgcolor="DBE5F1" align="right"><b>Portion en liquidité<br>Performance</b><br></td>
$%endif$
  </tr>
$%for 1 to TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].Code$<br>$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].Name$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].Client$</td>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ServiceType$<br>$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ManagementType$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].MarketValue$ $$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ValuationCurrency$<br>$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ProfitLoss$ $$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ValuationCurrency$<br></td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].CashPercentage$ %<br>$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].Performance$ %<br></td>
  </tr>
$%endfor$
</table>
$%endif$

<center>
<br />
<br />
$%if LANGUAGE_MAP_ALIAS != 'English'$
<font size="15">Sommaire consolidé</font>
$%else$
<font size="15">Your Consolidated Summary</font>
$%endif$
<br />
<br />
<font size="8">
$%if TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].lastInstance() > 0$
<table width="100%" border="1" bordercolor="ffffff" cellspacing="2" cellpadding="2">
$%if LANGUAGE_MAP_ALIAS != 'English'$
  <tr>
    <td bgcolor="DBE5F1"><b>Catégorie d'actifs</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Valeur de marché</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Pondération</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Gain / Perte</b></td>
  </tr>
$%else$
  <tr>
    <td bgcolor="DBE5F1"><b>Asset Class</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Market Value</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Weight</b></td>
  	<td bgcolor="DBE5F1" align="right"><b>Profit / Loss</b></td>
  </tr>
$%endif$
$%for 1 to TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].lastInstance()$
  <tr>
    <td bgcolor="F0F0F0">$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].AssetCode$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].MVAssetClass$&nbsp;$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].ValuationCurrency$</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].WeightAssetClass$&nbsp;%</td>
    <td bgcolor="F0F0F0" align="right">$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].UnrealAssetClass$&nbsp;$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].ValuationCurrency$</td>
  </tr>
$%endfor$
</table>
$%endif$
</pdf>