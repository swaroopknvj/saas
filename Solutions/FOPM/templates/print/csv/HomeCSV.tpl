﻿$%if LANGUAGE_MAP_ALIAS != 'English'$
Sommaire des actifs

Code de portefeuille,Nom de portefeuille,Client,Type de service,Type de gestion,Valeur de marché,monnaie,Gain / Perte,monnaie,Portion en liquidité,Performance
$%else$Your Portfolios

Portfolio Code,Portfolio Name,Client,Service Type,Management Type,Market Value,ccy,Profit / Loss,ccy,Cash Portion,Performance
$%endif$$%for 1 to TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].lastInstance()$$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].Code$,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].Name$,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].Client$,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ServiceType$,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ManagementType$,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].MarketValue$,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ValuationCurrency$,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ProfitLoss$,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].ValuationCurrency$,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].CashPercentage$%,$$TAP[1].Entities[1].Tcib_ValSummaryByPortfolio[C].Performance$%
$%endfor$



$%if LANGUAGE_MAP_ALIAS != 'English'$
Sommaire consolidé

Catégorie d'actifs,Valeur de marché,monnaie,Pondération,Gain / Perte,monnaie
$%else$Your Consolidated Summary

Asset Class,Market Value,ccy,Weight,Profit / Loss,ccy
$%endif$$%for 1 to TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].lastInstance()$$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].AssetCode$,$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].MVAssetClass$,$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].ValuationCurrency$,$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].WeightAssetClass$ %,$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].UnrealAssetClass$,$$TAP[1].Entities[1].Tcib_ValConsolidatedSummary[C].ValuationCurrency$
$%endfor$
