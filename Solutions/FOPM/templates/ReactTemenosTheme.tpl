<!DOCTYPE html>
<html dir="$$PRESENTATIONTYPE.LAYOUTDIRECTION$" 
      xmlns="http://www.w3.org/1999/xhtml" 
      xml:lang="en"  
      class="$$!FOOTER_LOCKED$ $$!HEADER_LOCKED$ $$!BOX_VIEW$ $$!FULL_SCREEN$ $$!SHOW_SIDEBAR$ $$!FONT_SIZE$ $$!DENSITY$ $$!HIGH_CONTRAST$ tc-show-breakpoint-hint tc-show-grid ">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="author" content="Temenos"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Temenos Theme React Template</title>
    <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon-32x32.png">
      $$HEADCONTENT$
  </head>
  <body class="BrowserWindow" >
    <link rel="stylesheet" href="./html/css/T-BaseReactExt.css" />
    <form name="form1" id="form1" method="post" action="servletcontroller"></form>
      $$FORMCONTENT$
    <script src="./html/js/widgets/com.temenos.theme/temenos-theme.js" type="text/javascript" ></script>
  </body>
</html>
