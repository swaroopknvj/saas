<style>
  #timeoutDialog {z-index: 2010; transform: translateY(-500px) translateX(-50%); position: fixed; top: 0;width: 25%; min-width: 300px; left: 50%; background-color: white}
  #timeoutDialog.show {transform: translateY(40vh) translateX(-50%)}
  
  #timeoutMask { z-index: 2000; width: 100vw; height: 100vh; position: fixed; opacity: 0; top: 0; background-color: #000; pointer-events: none  }
  #timeoutMask.show {opacity: 0.5; pointer-events: auto}

  #timeoutRefresh { float: right }
  #timeoutRefresh:hover { cursor: pointer }
</style>

<script type="text/javascript">

var TimeOutManager = (function () {
  
  var opts = {}
  var countdownInterval = null;
  var defaultOpts = {
    sessionTimeout: "1800",
    sessionTimeoutCountdown: "120",
    dialogStyles: "",

    dialogStyles: "",
    dialogClasses: "Card GlobalFont",
    
    dialogTitleStyles: "",
    dialogTitleClasses: "Bold Color2 LargeFont",
    dialogTitle: "Session Timeout",
    
    dialogMessageStyles: "",
    dialogMessageClasses: "Bold",
    dialogMessage: "Due to inactivity, your session is about to expire. Click 'Continue Session' if you wish to stay logged in.",
    dialogExpiredMessage: "Session Expired",
    
    dialogTimeoutStyles: "padding: 10px",
    dialogTimeoutClasses: "Bold Color2 LargeFont",

    dialogCancelStyles: "",
    dialogCancelClasses: "Color1 RightAlignedImage ActionArrow MediumFont ActionTertiary",
    dialogCancelButtonText: "Cancel",
    
    dialogRefreshStyles: "",
    dialogRefreshClasses: "Color1 RightAlignedImage ActionArrow MediumFont Underline",
    dialogRefreshButtonText: "Continue Session",
    dialogExpiredButtonText: "Restart",

    autoClickOnTimeout: false,
    animationDuration: 1
  }

  var setupOpts = function(p_opts) {
    opts = Object.assign({}, defaultOpts, p_opts);
  }

  var createStructure = function() {
    var body = document.body;
    var ecp = document.getElementById("EDGE_CONNECT_PROCESS")
    if (ecp == null || document.getElementById("timeoutDialog") != null) return;
    
    var mask = document.createElement("div")
        mask.setAttribute("style", (opts.animationDuration > 0 ? "; transition: opacity " + opts.animationDuration + "s" : ""))
        mask.setAttribute("id", "timeoutMask")

    body.prepend(mask);

    var dialog = document.createElement("div")
        dialog.setAttribute("id", "timeoutDialog")
        dialog.setAttribute("style", opts.dialogStyles + (opts.animationDuration > 0 ? "; transition: all " + opts.animationDuration + "s" : ""))
        dialog.classList.add(...opts.dialogClasses.split(" "))

    var title = document.createElement("div")
        title.setAttribute("id", "timeoutTitle")
        title.setAttribute("style", opts.dialogTitleStyles)
        title.classList.add(...opts.dialogTitleClasses.split(" "))
        title.innerText = opts.dialogTitle
        dialog.appendChild(title)

    var message = document.createElement("div")
        message.setAttribute("id", "timeoutMessage")
        message.setAttribute("style", opts.dialogMessageStyles)
        message.classList.add(...opts.dialogMessageClasses.split(" "))
        message.innerText = opts.dialogMessage
        dialog.appendChild(message)


    var timeout = document.createElement("div")
        timeout.setAttribute("id", "timeout")
        timeout.setAttribute("style", opts.dialogTimeoutStyles)
        timeout.classList.add(...opts.dialogTimeoutClasses.split(" "))
        dialog.appendChild(timeout)

    var cancel = document.createElement("button")
        cancel.setAttribute("id", "timeoutCancel")
        cancel.setAttribute("style", opts.dialogCancelStyles)
        cancel.classList.add(...opts.dialogCancelClasses.split(" "))
        cancel.innerText = opts.dialogCancelButtonText
        dialog.appendChild(cancel)

    var refresh = document.createElement("button")
        refresh.setAttribute("id", "timeoutRefresh")
        refresh.setAttribute("style", opts.dialogRefreshStyles)
        refresh.classList.add(...opts.dialogRefreshClasses.split(" "))
        refresh.innerText = opts.dialogRefreshButtonText
        dialog.appendChild(refresh)

    body.appendChild(dialog);
  }


  var startCountdown = function() {
    let to = opts.sessionTimeout;

    document.getElementById("timeoutRefresh").onclick = function()  {
      //window.location.reload()
      window.location.href = window.location.href + ( window.location.href.indexOf("?") > 0 ? "&" : "?") + "MODE=REDISPLAY"
    }

    if (countdownInterval) {
      clearInterval(countdownInterval)
    }

    countdownInterval = setInterval(function() {
      createStructure()

      let totalSeconds = parseInt(to);
      let hours = (Math.floor(totalSeconds / 3600)).toString().padStart(2, "0");
      totalSeconds %= 3600;
      let minutes = (Math.floor(totalSeconds / 60)).toString().padStart(2, "0");
      let seconds = (totalSeconds % 60).toString().padStart(2, "0");
      document.getElementById("timeout").innerText = hours + ":" + minutes + ":" + seconds
      

      //threshold at which to show
      if (to <= parseInt(opts.sessionTimeoutCountdown)) {
        var timeoutDialog = document.getElementById("timeoutDialog");
        var refreshButton = document.getElementById("timeoutRefresh");
        var cancelButton = document.getElementById("timeoutCancel");
        cancelButton.onclick = function()  {
          window.location = location.pathname + "?MODE=XX"
        }

        timeoutDialog.classList.add("show")
        timeoutMask.classList.add("show")
        refreshButton.focus();
      }

      if (to <= 0) {
        to = 0;
        cancelButton.style.display = "none"
        document.getElementById("timeoutMessage").innerText = opts.dialogExpiredMessage
        refreshButton.innerText = opts.dialogExpiredButtonText
        refreshButton.onclick = function()  {
          window.location = location.pathname + "?MODE=XX"
        }

        setTimeout(function() {
          if (opts.autoClickOnTimeout)
            refreshButton.click();
        }, 1000)
        
      }
      //limit to 0;
      to = Math.max(--to, 0);
    }, 1000)
  }


  var configure = function(opts) {
    setupOpts(opts);
    createStructure();
    startCountdown();
  };


  return {
    configure: configure
  }
})();

</script>
