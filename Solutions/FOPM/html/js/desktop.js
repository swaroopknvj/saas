if($('body[data-section="assets"]').length){
  var multiorder = ss.get('multiorder') ? JSON.parse(ss.get('multiorder')) : {};
}else{
  ss.clear('multiorder'); multiorder = {};
  ss.clear('mocont');     mocont = '';
}
var mocont = '';
var motp = -1;      // init variable table page
var its = '';
var exprow = '';

var LANGUAGE_ENGLISH  = 'English';
var LANGUAGE_FRENCH   = 'French';

var LANG_IDX_EN = 0;                              // Language: English
var LANG_IDX_FR = 1;                              // Language: French

var LANG_IDX_DEFAULT = LANG_IDX_EN;

function setCollapseStatus(o){
  $.each($('.collapsibleHeader'), function(i,h){
    var head = $(h).closest('div[id^=p1_GRP]');
    if(head.length){
      var state = !!$(h).hasClass('activePanelHeader');
      ss.set('ec_'+head.attr('id'), state);
    }
  })
}
function applyCollapseStatus(){
  $.each($('.collapsibleHeader'), function(i,h){
    var head = $(h).closest('div[id^=p1_GRP]'),
        state = ss.get('ec_'+head.attr('id'));
    if(head.length && state == "true"){
      $(h).find('.collapsibleLink').addClass('bt_collapse').removeClass('bt_expand')
          .parent().addClass('activePanelHeader')
      $(h).find('a[data-toggle]').trigger('click')
    }
  })
}

/* ----- Hover messages on the collapsible card ----- */
var hoverMsgT = {
  'CHEVRON-DOWN' :  ['Show more details about this client','Afficher plus de d\u00E9tails concernant ce client'],
  'CHEVRON-UP' :    ['Show less details about this client','Afficher moins de d\u00E9tails concernant ce client']
}
function showCollapsibleCard(buttonId, sectionId, downTextTagId, upTextTagId){
  onToggle(buttonId, sectionId);
  $('#'+buttonId).toggleClass('icon-chevron-down');
  $('#'+buttonId).toggleClass('icon-chevron-up');

  if(typeof downTextTagId != 'undefined' && typeof upTextTagId != 'undefined'){
	if ($('#'+buttonId).hasClass('icon-chevron-down')){
		$('#'+buttonId).attr('title',hoverMsgT['CHEVRON-DOWN'][getLangIdx(LANGUAGE)]);
		$('#'+downTextTagId).show();
		$('#'+upTextTagId).hide();
	}else {
		$('#'+buttonId).attr('title',hoverMsgT['CHEVRON-UP'][getLangIdx(LANGUAGE)]);
		$('#'+upTextTagId).show();
		$('#'+downTextTagId).hide();
	}
  }
}

$(document).ready(function(){
  errorLbl();
  $('body').addClass(has.touch?'touch':'no-touch');
  addBrowser();
  autoTrigger();
  ResetPos();
  BMMenuHandler();
  checkBoxIcons();
  //transformTitles();
  setTabbedPaneNew();
  if($('.progressBar').length) StepsProgressBarWatch();
  familyTreeProcessing();
  $('table.TAPalike.addTotalRow').each(function(i,t){addTotalRow($(t).attr('id'))});
  $('table.TAPalike .formItem.readOnly').prop('readOnly',true);
  if($('body').hasClass('IE')) $.ajaxSetup({ cache: false });
  if(typeof(childCheck)=='function') setTimeout("childCheck()",100);    // TCIB-2611
  $('input.searchItem[title]').each(function(){$(this).attr('placeholder', $(this).attr('title'))});
  if(!$('.InnerTabSelected').length && $('.InnerTabUnselected').length){$('.InnerTabUnselected').eq(-1).click()}
  handleRequestParams();
  if($('body').data('section') != 'login' && (window.innerHeight > $('body').height())) $('body').addClass('sticky');
  $('.formItem.required').prop('required', true);
  $('.chosen-select').chosen({placeholder_text_multiple: " ", placeholder_text_single: " ", width: "auto"});
  setTimeout('delay_dp_minmax()',200);
  largeTDTextTooltip();
  addTooltipOnEllipsis();
  FixIEnoprofiledefined();
  if(typeof uniqueVal != 'undefined') uniqueVal();
  $('body').find('.TAPalike').each(function(i){handleTAPalike($(this).attr('id'));})
  logperf();
  if($('body[data-section="login"]').length){setTimeout('checkBoxIcons()',300);}
  $('body').on('change', '.changePortfolioDropdown', function(){$("button.changePortfolioButton").click();})
//  $('.Logo').on('click', function(){$('li#MNU_W_home').trigger('click')})
//  $('div.filterItem').on('click', function(){$(this).find('input').trigger('click')})
});

function StepsProgressBarWatch(){
  $('.progressBar>li').each(function(i,li){
    var itemWidth = $(li).width();
    var stext = $(li).find('.name');
    var textWidth = $(this).text().trim().length*11;
    if(textWidth > itemWidth) $(stext).addClass('long');
  })
}
function BMMenuHandler(){                         // -- Handle BlueMagic dropdown menu --
  $('.navXmenu li[id$="'+SelectedSubMenu+'"]').addClass('active')
    .closest('li[id^=MNU_]').addClass('active');
  if(!$('#menuIcon').is(':visible')){
    $('.navXmenu').each(function(i,o){            // parse all root menus
      var items = $(this).find('>li>ul>li');      // get sub-menus
      items.css('width', 100/items.length+'%');   // scale sub-menus width depending on amount
      if(items.length == 1 && $(this).find('>li').hasClass('SingleSubMenu')){                      // if single sub-menu
        $(this).find('>li').attr('onclick', $(this).find('>li>ul>li').attr('onclick'))    // move click action to main menu
        $(this).find('>li>ul').remove();          // remove sub-menu
        $(this).find('i.icon').remove();          // remove root menu icon (menucaret)
      }
      if($(o).find('>li').attr('id') == 'MNU_W_home'){      // if home menu
        $(o).find('>li').text('').append($('<i />').addClass('icon BlueMagic home'));     // remove text and replace with home icon
      }
      if(items.length > 1 && items.length < 4){
        items.css('width', '25%');
        items.css('left', (100/items.length/items.length)+'%')
      }
    })
    var activemenu = $('li[id^=MNU_].active');
    if(activemenu.length){
      $('.BMMenu').append($('<div/>').addClass('mwrapper').append($('<div/>').addClass('mhilite')));
      setTimeout(function(){$('.mhilite').css({'width': $(activemenu).parent().width(), 'margin-left':$(activemenu).parent().position().left - $('.Logo').width()})}, 250);
      $('ul.navXmenu > li').hover(function(){$('.mwrapper').hide()}, function(){$('.mwrapper').show()});
    }
  }else{                                          // small screen accordion menu
    $('#menuIcon').on('tap', function(){
      $('.nav_menu').toggleClass('active');
    })
    $('.navXmenu').each(function(i,o){            // parse all root menus
      var items = $(this).find('>li>ul>li');      // get sub-menus
      $(this).find('i.icon.menucaret').remove();  // remove root menu icon (menucaret)
      if(items.length == 1){                      // if single sub-menu
        $(this).find('>li').attr('onclick', $(this).find('>li>ul>li').attr('onclick'))    // move click action to main menu
        $(this).find('>li>ul').remove();          // remove sub-menu
      }
    })
    $('ul.navXmenu>li').on('tap', function(){
      $(this).closest('div[id^=row_MNU_]').siblings().find('.active').removeClass('active')
      $(this).addClass('active');
    })
    $('ul.navXmenu>li>ul>li').on('tap', function(){
      $(this).addClass('active');
      $(this).closest('li').addClass('active');
    })
  }
}
function ResetPos(){
  $('.highcharts-container').css('width','auto');
}

function checkBoxIcons(o){
  var selector = false;
      selector = o ? $(o).closest('.CheckBoxIcon') : $('.formItem.CheckBoxIcon');
  selector.each(function(i,o){
    if($(o).closest('.tableContainer').length){
      $(o).closest('td').addClass('tdwcbicon');
    }
    var checkbox = $(o).find('input[type="checkbox"]');
    if($(checkbox).is(':checked')) $(o).addClass('checked');
    else $(o).removeClass('checked');
    $(checkbox).on('change', function(){
      if($(this).is(':checked')) $(this).parent().addClass('checked');
      else $(this).parent().removeClass('checked');
    })
  });
}


function gbm(){
  $('.gbmSelection .formRow:visible').find('.formItem').each(function(i,o){
    $(o).on('change', function(){
      if($(o).val() != '') $('.LayerBtnRow').find('*').removeClass('edgeConnectDisabled')
    })
  })
}


function tapCallback(){
//  console.log('tapCallback from ' , tapCallback.caller);
  ResetPos();
  if($('.ui-widget:visible').length && $(window).scrollTop()>100){
    $('.ui-widget:visible').css('top', ($(window).scrollTop()+($(window).height()/8))+'px');
  }
  checkBoxIcons();
  familyTreeProcessing();
  if($('.progressBar').length) StepsProgressBarWatch();
  $('.chosen-select').chosen({placeholder_text_multiple: " ", placeholder_text_single: " ", width: 'auto'});
  errorLbl();
  tfootersum();
  $.each($('table[data-sum]'), function(i,t){addSumRow(t)})           // TCIB-3906
  if(typeof(childCheck)=='function') setTimeout("childCheck()",100);  // TCIB-2611
  gbm();
  $('table.TAPalike.addTotalRow').each(function(i,t){addTotalRow($(t).attr('id'))});
  $('table.TAPalike .formItem.readOnly').prop('readOnly',true);
  largeTDTextTooltip();
  addTooltipOnEllipsis();
  $('body').find('.TAPalike').each(function(i){handleTAPalike($(this).attr('id'));})
  if(srv.length) setTimeout("getsubrowvis()", 5);
  if(multiorder.toString().length) stickySummary();
  if(its.length && its != $('.InnerTabSelected').attr('id')) $('#'+its).trigger('click');
  if(typeof expandRows == 'function') expandRows();
  if($('body').data('section') != 'login' && (window.innerHeight > $('body').height())) $('body').addClass('sticky');
  else $('body').removeClass('sticky');
  setTabbedPaneNew();
  if(typeof uniqueVal != 'undefined') uniqueVal();
  if(typeof cm != 'undefined') {setTimeout("cm.setup(cm.tid)", 1)};
  logperf(1);
}

function tfootersum(){
  $('table.withClickableFooter:not(.hidden) tbody').find('tr').last().addClass('totalRow');
}
function setTabbedPane(){                         // mark tabs as selected on pageload
  var tabbedPane = $('.PaddedSection'); //CONNECT_ACTIVE_TAB
  var tabbedTabs = tabbedPane.find('div[id^="TAB_"]');
  var tabCont = tabbedPane.find('.InnerTabPane');
  if(!tabCont.is(':visible')){
    tabCont.eq(0)
      .css('display','block')
    if(!tabbedTabs.hasClass('InnerTabSelected'))
      tabbedTabs.eq(0)
        .toggleClass('InnerTabSelected')
        .toggleClass('InnerTabUnselected');
    if(!$('.tetris_tabHeader').hasClass('InnerTabSelected'))
      $('.tetris_tabHeader').eq(0)
        .toggleClass('InnerTabSelected')
        .toggleClass('InnerTabUnselected');
  }
  $('.TabButtonSelectedNew').removeAttr('title');           // remove tooltips from all selected tabs
  $('.TabButtonUnselectedNew').removeAttr('title');         // remove tooltips from all unselected tabs
}
function setTabbedPaneNew(){                      // mark tabs as selected on pageload
  $('.tabbed-pane').find('.tab').each(function(i,tab){$(tab).removeAttr('title')});
}
function setAcDetPane(){
  var subtabs = $('.InnerTab:visible');
  if(subtabs.length){
    var seltab = $(subtabs).parent().find('.InnerTabSelected')
    if(!seltab.length){
      $(subtabs).eq(0).trigger('click');
    }
  }
}
function newMenu(){
  var newmenu = '<nav class="container">';
  newmenu += '<i class="icon tapt24 menu"></i>';
  newmenu += '<ul class="menu">';
  $('.fullmenu ul.nav').each(function(i,o){
    if(!$(o).find('ul li').length) return;
    var hassub = $(o).find('ul').length;
    var togsub = hassub ? ' onclick="toggleSub(this)"' : '';
    var menusel = $('body').attr('data-section');
    var clickable = !hassub ? ' onclick="'+$(o).find('a').attr('onclick')+'"' : '';
//    var isselected = ($(o).attr('id') == menusel) ? ' class="active"' : '';
    var isselected = $(o).attr('id').indexOf(menusel)>-1 ? ' class="active"' : '';
    newmenu += '<li'+isselected+clickable+'><b'+togsub+'>'+trim($(o).find('a').eq(0).text())+
      (hassub ? '<b class="caret"></b>' : '')+
      '</b>';
    $(o).find('ul').each(function(j,s){
      var ncc = 0;
      var isactive = isselected.length ? '' : ' style="display: none;"';
      newmenu += '<ul'+isactive+'>';
        $(s).find('li').each(function(k,sm){
          newmenu += '<li id="'+$(sm).attr('id')+'" onclick="ssm(this);'+$(sm).find('a').attr('onclick')+'">'+
            '<b>'+trim($(sm).find('span').eq(0).text())+'&nbsp;</b>'+
            '<i>'+trim($(sm).find('p').eq(0).text())+'&nbsp;</i>'+
          '</li>';
          ncc++;
        });
        if(ncc%2) newmenu += '<li class="ncce"><b>&nbsp;</b><i>&nbsp;</i></li>';
      newmenu += '</ul>';
    });
    newmenu += '</li>';
  });
  newmenu += '</ul></nav>';
  $('.fullmenu').empty();
  $('.fullmenu').append(newmenu);
  $('#'+ss.get('submenu')).addClass('active');
  $('nav i.icon.menu').on('tap', function(){$('ul.menu').toggleClass('active')});
  $('nav li[onclick]').on('tap', function(){$('ul.menu').toggleClass('active')});
}
function toggleSub(o){
  $('nav ul ul').hide();
//  $(o).next('ul').toggle();
//  $(o).next('ul').show();
  $(o).next('ul').attr('style','');
}
function ssm(o){
//  console.log('clicked on menu id: '+$(o).attr('id'));
  ss.set('submenu', $(o).attr('id'));
}

function autoTrigger(){
  $('#QUE_CHANGE_PORTFOLIO_SELECT').on('change', function(){
    if($(this).val()) {
      $('#BUT_CHANGE_PORTFOLIO').trigger('click');
    }
  });
  $('#QUE_CHANGE_ACCOUNT_SELECT').on('change', function(){$('#BUT_CHANGE_ACCOUNT').trigger('click')});
}

/* ----- Override/modify default edge core functions ----- (needed for HiCharts interactive chart widgets) */
function PerformDefaultButtonAction(p_defaultButton, ns, p_doEvenWhenHidden){
  if(p_defaultButton != null && p_defaultButton != ""){
    var form = getForm(ns);
    if(!PerformedDefaultButtonAction(p_defaultButton, form.getElementsByTagName("button"), ns, p_doEvenWhenHidden)){
      if(!PerformedDefaultButtonAction(p_defaultButton, form.getElementsByTagName("input"), ns, p_doEvenWhenHidden)){
        PerformedDefaultButtonActionOnLinks(p_defaultButton, getLinks(document, true, ns), ns, p_doEvenWhenHidden);
      }
    }
  }
}
function PerformedDefaultButtonAction(p_defaultButton, p_elements, ns, p_doEvenWhenHidden){
  for (var i =0; i < p_elements.length; i++){
    if  ( p_elements[i].name && p_elements[i].name == p_defaultButton && p_elements[i].onclick ){
      if (p_doEvenWhenHidden || !isHidden(p_elements[i])){
        execute(p_elements[i], "onclick", DEFAULT_BUTTON_ACTION_TRIGGER);
      }
      return(true);
    }
  }
  return(false);
}
function PerformedDefaultButtonActionOnLinks(p_defaultButton, p_elements, ns, p_doEvenWhenHidden){
  var buttonClickedCheck = "buttonClicked('" + p_defaultButton + "'";
  var buttonClickedOfflineCheck = "buttonClickedOffline('" + removeSpaces(p_defaultButton) + "'";
  var buttonClickedCheckDbl = "buttonClicked(\"" + p_defaultButton + "\"";
  var buttonClickedOfflineCheckDbl = "buttonClickedOffline(\"" + removeSpaces(p_defaultButton) + "\"";
  for (var i =0; i < p_elements.length; i++){
    if  ( p_elements[i].onclick ){
      var onclickString = p_elements[i].onclick.toString();
      if  ( onclickString.indexOf(buttonClickedCheck) > -1 || onclickString.indexOf(buttonClickedOfflineCheck) > -1 || onclickString.indexOf(buttonClickedCheckDbl) > -1 || onclickString.indexOf(buttonClickedOfflineCheckDbl) > -1 ){
        if (p_doEvenWhenHidden || !isHidden(p_elements[i])){
          execute(p_elements[i], "onclick", DEFAULT_BUTTON_ACTION_TRIGGER);
        }
        return(true);
      }
    }
  }
  return(false);
}

/* ----- Start Initiate Multiple Orders ----- */
function cbIcon(tid){                                                           // Replace checkbox by icon for buy/sell initiate order
  var t = $('#'+tid);                                                           // get related table
  motp = t.data('page') ? t.data('page') : '-1';                                // get table page
  var cprefix = tid.split('__')[0];
  if($(t).closest('#'+cprefix+'__TableFilterGroup').length)
    tid = cprefix+'__TableFilterGroup';
  var checkboxes = $(t).find('td.List.boolean input[type="checkbox"]');         // collection of all checkboxes matched in table
  if(!checkboxes.length) return;                                                // abort if none
  checkboxes.each(function(i,o){
    if($(o).is(':checked')){$(o).closest('.icon.cbr').addClass('active');}      // initialize state of icons
    $(o).on('click', function(){                                                // action to take when checkbox is clicked
console.log('checkbox clicked',o.id);
      var cidx = $(o).closest('td').index();                                    // get clicked cell index
      var cbinrow = $(o).closest('tr').find('input[type="checkbox"]');          // all checkboxes in same row
console.log('cbinrow',cbinrow);
      cbinrow.each(function(a,x){                                               // loop in same row checkboxes
        var xidx = $(x).closest('td').index();                                  // get cell index of now handled checkbox
        if(xidx == cidx && $(x).is(':checked')){                                // if cell index matches selected checkbox
          $(x).closest('.icon.cbr').addClass('active')                          // mark icon as active
        }else{                                                                  // else
          $(x).prop('checked', false);                                          // uncheck not clicked checkboxes in same row
          $(x).closest('.icon.cbr').removeClass('active');                      // and mark corresponding icon as not active
        }
      });
      stickyVal(this, tid, 1)                                                   // add/remove checkbox to stickyVal list
      stickySummary(tid);                                                       // initiate sticky Summary display on click
      stickyNumberText();
      setsubrowvis(t);
    });
  });
  stickyVal('group', tid, 0)
}

function stickySummary(tid){                                                    // sticky Summary display
  var mocont = ss.get('mocont');
  var t = $('#'+tid);                                                           // get ancestor item (table or table-group)
  var stickySum = $('#stickySum');                                              // get Summary display box element
  stickyNumberText();
  if(stickySum.length){
    if(Object.keys(multiorder).length){
      t.find('input[type=checkbox]:checked').each(function(i,o){
        stickyVal(o, tid, 1)
      });
    }else if(t.find('input[type=checkbox]:checked').length == 0){
      stickySum.hide();
    }
  }
  var scrollTimer = null;
  posSticky();                                              // keep stickySum in viewport
  stickySum.on('click', function(){                         // make sticky Summary clickable
    $(initMultiOrders.iobtn).trigger('click');
  })
  stickySum.hover(function(){
    stickySum.find('.spacerdiv > i').show();
    var detSum = $('div.detSum');
    if(detSum.length) detSum.remove();
    detSum = $('<div class="detSum"/>');
    stickySum.append(detSum);
    for(key in multiorder){
      detSum.append($('<div />').text(multiorder[key].text))
    }
    detSum.append($('<div class="action"/>').append($(initMultiOrders.iobtn).clone()))
  }, function(){
    $('div.detSum').remove();
    stickySum.find('.spacerdiv > i').hide();
  })
  $(window).scroll(function(){
    if(scrollTimer){clearTimeout(scrollTimer)}              // clear any previous pending timer
    scrollTimer = setTimeout(posSticky, 100);               // call posSticky function 100ms after last scroll event.
  });
  function posSticky(){                                     // position of stickySum in order to keep it in viewport next to table.
    if ($('#stickySum .scont').length){                     // if stickySum exists
      var wst = parseInt($(window).scrollTop()),            // how many pixels did we scroll the page down from top
          prv = $('#stickySum').prev().prop("tagName"),
          tot = prv!='TABLE' ? parseInt($('.TableFilterGroup').position().top) :// the original top position of the corresponding TableFilterGroup
            parseInt($('#stickySum').prev().position().top) ,                   // the original top position of the corresponding table
          tblpos = prv == 'TABLE' ? parseInt($('#stickySum').prev().offset().top) :
            parseInt($('.TableFilterGroup').find('.tableContainer').offset().top),
          whg = parseInt(window.innerHeight / 2),
          mst = wst - tot + tblpos + 25;
      if(prv == 'TABLE'){
        if(wst > tot)     {$('#stickySum').stop().animate({marginTop: (wst-100)+'px'}, 100);}
        else if(whg > wst){$('#stickySum').stop().animate({marginTop: 0}, 100);}
        else              {$('#stickySum').stop().animate({marginTop: (wst - whg + 150) + 'px'}, 100);}
      }else{
        if(tot > wst){
          $('#stickySum').stop().animate({marginTop: mst + 'px'}, 100);
        }else{
          $('#stickySum').stop().animate({marginTop: (wst + 150) + 'px'}, 100);
        }
      }
    }
  }
}
function stickyVal(o,tid,L){
  var t = $('#'+tid);                                       // get related table
  if(o == 'group'){
    if(!$('#stickySum').length){                                                                    // if it does not exist yet: create it
      $(t).parent().append($('<div id="stickySum" />'));                                            // add sticky Summary to DOM
      $('#stickySum').append($('<div class="spacerdiv" />').append($('<i />').append($('<i />')))); // add spacer div on left side to set left pointing caret
      $('#stickySum').append($('<i class="icon BlueMagic transfer" />'));
    }
    stickyNumberText();
  }else{
    var imo_code = $(o).closest('tr').find('span.imo-instr-denom').text().trim(),
/*
    var imo_code = $(o).closest('tr').hasClass='subrow' ?
          $(o).closest('tr').find('span.imo-instr-code').text().trim():
          $(o).closest('tr').find('span.imo-code').text().trim(),
*/
        imo_ptf = $(o).closest('tr').find('span.imo-ptf').text().trim();
    if($(o).is(':checked')){
      if(checkMultiOrderText(stickyDisplayItemText(o))){
        multiorder[imo_code+'__'+imo_ptf] = {
          'type': o.name.split('.').pop(),
          'op': $(o).closest('table').find('thead th').eq($(o).closest('td').index()).text().trim(),
          'text': stickyDisplayItemText(o)
        }
      }
    }else{
      if(L!=0)
      delete multiorder[imo_code+'__'+imo_ptf];
    }
  }
  ss.set('multiorder', JSON.stringify(multiorder));
  if(Object.keys(multiorder).length) $('#stickySum').show();
  else  $('#stickySum').hide();
}
function checkMultiOrderText(t){
  for(key in multiorder){
    if(multiorder[key].text == t) {
      return false;
    }
  }
  return true;
}
function stickyNumberText(){
  var cbb = 0, cbs = 0, cbbop, cbsop;
  for(key in multiorder){
         if(multiorder[key].type == 'BUY')  {cbb++;  cbbop = multiorder[key].op;}
    else if(multiorder[key].type == 'SELL') {cbs++ ; cbsop = multiorder[key].op;}
  }
  $('#stickySum .scont').remove();                                    // remove content area
  if(cbb + cbs > 0)
    $('#stickySum').append($('<div class="scont" />'));
  if(cbb > 0)                                                         // handle buy items
    $('#stickySum .scont').append($('<div />').text(cbb+' '+cbbop));
  if(cbs > 0)                                                         // handle sell items
    $('#stickySum .scont').append($('<div />').text(cbs+' '+cbsop));
}
function stickyDisplayItemText(o){
  if(typeof initMultiOrders == 'undefined')           // MSIE10 bug after AJAX action
    eval($('#init_'+tid).text());                     // execute the script (declare variable 'initMultiOrders')
  if(typeof initMultiOrders == 'undefined' && $('body').hasClass('MSIE10'))     // MSIE10 bug after AJAX action
    initMultiOrders={"iotid":$(this).prev().find('table').attr('id'),"iobtn":".iobtnadp","iotxt":"~nat .imo-qty .imo-instr on .imo-ptf"};
  var keys = initMultiOrders.iotxt.split(' ');        // text to display (set in edge widget) (e.g.: "~nat .imo-qty .imo-instr on .imo-ptf")
  var txt = '';
  for(var k = 0; k < keys.length; k++){
    if(keys[k].indexOf('~') == 0){                    // get text of items column heading
      txt += ' ' + $(o).closest('table').find('thead th').eq($(o).closest('td').index()).text().trim();
    }else if(keys[k].indexOf('.') == 0){              // get text of cell item with same className in items row
      txt += ' ' + $(o).closest('tr').find(keys[k]).eq(0).text().trim();
    }else if(keys[k].indexOf('#') == 0){              // get text of item with corresponding id on page
      txt += ' ' + $(keys[k]).text().trim();
    }else{                                            // get translated text element
      txt += ' '+keys[k];
    }
  }
  return txt.trim();
}
var srv = [];                           // init array of visible subrows
function setsubrowvis(t){               // set  array of visible subrows
  srv = [];                             // reinit array of visible subrows
  motp = t.data('page') ? t.data('page') : '-1';
  $(t).find('tr.subrow:visible').each(function(i,r){srv.push($(r).attr('id')+'__Page'+motp)})
}
function getsubrowvis(){                // get visible subrows and display them
  var t = $('table[data-page]')
  motp = t.data('page') ? t.data('page') : '-1';
  for(var i=0; i<srv.length; i++){
    if(motp == srv[i].split('__Page')[1]){
      $('#'+srv[i].split('__Page')[0])
        .show()
        .prevAll('tr.groupedRow:first')
          .find('td .icon:first')
            .removeClass('right')
            .addClass('down');

    }
  }
}
/* ----- End Initiate Multiple Orders ----- */

function transformTitles(){
  $('[title]').hover(
    function(){
      $(this).attr('oldtitle', $(this).attr('title'));
      $(this).attr('title', '');
    },
    function(){
      $(this).attr('title', $(this).attr('oldtitle'));
      $(this).removeAttr('oldtitle');
    }
  )
}

function handleRequestParams(){
  if(! $('body').attr('data-section')=="assets") return false;
  var MxParams = requestParam.split('|');
  if(MxParams.length != 3) return false;
  var filtertable = 'div[id$=TableFilterGroup]';
  var placeToBe = $('#AssetFilters ul');
  if(MxParams[0].length) addItem(MxParams[0], 'chartfilter-'+MxParams[2]);
  if(MxParams[1].length) addItem(MxParams[1], 'chartfilter-assets');

  function addItem(val, col){       // copy from matrix.js
    if(!$(placeToBe).find('li[data-val="'+val+'"]').length){
      var item = $('<li />')
        .attr('data-mon', 'from Matrix')
        .attr('data-val', val)
        .attr('data-col', col)
        .text(val)
        .append($('<i/>')
          .addClass('icon BlueMagic cancel')
          .on('tap', function(e){
            $(this).parent().remove();
            af1(filtertable);
            e.preventDefault();
          })
        )
      placeToBe.append(item);
    }
  }
  af1(filtertable);
}
function ChartLink(o,b,d){
  var d = d || '#QUE_ChartValueRedirector';
  $('#'+d).val(o.name)
  $('#'+b).trigger('click');
}

function LineChartLink(o,b,d,e){
  var d = d || '#QUE_ChartValueRedirector';
  var e = e || '#QUE_ChartValueRedirector1';
  $('#'+d).val(o.category)
  $('#'+e).val(o.series.name)
  $('#'+b).trigger('click');
}

/* ----- Helper Functions ----- */
String.prototype.width = function(fontSize, fontFamily, fontWeight){
  var fs = fontSize || '14px',
      ff = fontFamily || 'Roboto',
      fw = fontWeight || 'normal',
      o = $('<div>' + this + '</div>')
        .css({'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font-size': fs, 'font-family': ff, 'font-weight': fw})
        .appendTo($('body')),
      w = o.width();
  o.remove();
  return w;
}


function debounce(func, wait, immediate){
  var timeout;
  return function(){
    var context = this, args = arguments;
    clearTimeout(timeout);
    timeout = setTimeout(function(){
      timeout = null;
      if(!immediate) func.apply(context, args);
    }, wait);
    if(immediate && !timeout) func.apply(context, args);
  };
};

jQuery.expr.filters.offscreen = function(el) {
  var rect = el.getBoundingClientRect();
  return (
           (rect.x + rect.width) < 0
             || (rect.y + rect.height) < 0
             || (rect.x > window.innerWidth || rect.y > window.innerHeight)
         );
};

function largeTDTextTooltip(){
  $('.tableContainer table:visible').each(function(i,t){
    var tdvlen = $(t).find('th:visible').length
    if(!tdvlen) return true;
    var tdtxt  = $(t).find('tbody tr').find('td.Text'),
        tdtxtr = $(t).find('tbody tr').eq(0).find('td.Text'),
        twidth = $(t).width(),
        tdtw   = parseInt(twidth/(tdtxtr.length+1)); // '250'
    $(t).find('tbody tr td.Text:visible').each(function(j,o){
      var textWidth = $(this).text().trim().length*9;
      if(tdtw < textWidth && $(this).text().trim().indexOf(' ')<0){
        $(tdtxt).addClass('large').find('span').css('max-width', tdtw+'px');
      }
    });
  });
  $('.tableContainer td.Text span').hover(function(){
	if (this.clientWidth<this.scrollWidth) {
      $(this).attr('title', $(this).text().trim());
    }else{
	  $(this).attr('title', '');
	}
  });
};

function delay_dp_minmax(){                       // delay datepicker min and max options
  var dpmin = $('input.date-picker[data-min]'),
      dpmax = $('input.date-picker[data-max]');
  if(dpmin.length == 1 && dpmax.length == 1){
    dpmin.each(function(i,o){$(o).datepicker("option", "minDate" , $(dpmax).val())});
    dpmax.each(function(i,o){$(o).datepicker("option", "maxDate" , $(dpmin).val())});
  }
}
var getValue = function(o){                       // get value for form element
//  console.log('getValue from ',o.id);
  switch(o.type){                                 // switch on form element's type
    case 'select-one':                            // if dropdown
    case 'select-multiple':
      var sel="";                                 // declare empty selected string
      if(!o.options) return 0;                    // if no items, return 0
      for(var i=0; i<o.options.length; i++){      // loop through items
        if(o.options[i].selected)                 // if item is selected
          sel+=(sel.length?'; ':'')+              // add item to selected string
                o.options[i].value;
      }
      return sel.length?sel:'';                   // return selected string
      break;
    case 'checkbox':                              // if checkbox
    case 'radio':                                 // or radiobutton
      return o.checked?'true':'false';            // return true if checked or false if not
      break;
    default:                                      // all other form element types
      return o.value || '';                       // return value or empty string
  }
}

function setTxtfromDropdown(o,refName){
  var rowidx    = $(o).closest('tr').index()+1;
  var txtfield  = $('#'+refName+'_R'+rowidx);
  var selTxt    = o.options[o.selectedIndex].text;
  txtfield.val(selTxt);
}

function getLangIdx(language){                    // Get the index for the selected language
  switch(language){
    case LANGUAGE_ENGLISH:                        // English
      return LANG_IDX_EN;
      break;
    case LANGUAGE_FRENCH:                         // French
      return LANG_IDX_FR;
      break;
    default:                                      // Default language
      return LANG_IDX_DEFAULT;
  }
}

function addTooltipOnEllipsis(){
	$('.showTooltipOnEllipsis').hover(function(){
	 if (this.clientWidth<this.scrollWidth) {
      $(this).attr('title', $(this).text().trim());
    }else{
	    $(this).attr('title', '');
	  }
  });
}

function FixIEnoprofiledefined(){
  if(($('body').hasClass('Trident') || $('body').hasClass('Edge')) && $('.PHASE_TapRiskDetails').length){
    $('.noProfileDefined:visible').each(function(i,o){
      $(o).closest('.card').find('.profileGraph').remove();
    });
  }
}
