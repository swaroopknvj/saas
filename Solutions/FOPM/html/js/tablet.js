﻿/*
 * Copyright (c) 2016 Temenos, All rights reserved.
 *
 * This source code is protected by copyright laws and international copyright treaties,
 * as well as other intellectual property laws and treaties.
 *
 * Access to, alteration, duplication or redistribution of this source code in any form
 * is not permitted without the prior written authorisation of Temenos.
 */

$(document).ready(function(){
  init();
});

var init = function(){                            // Run this when the DOM is loaded but not yet displayed
  checkLogin();
  addBrowser();
//  if(!('ontouchstart' in document.documentElement)) $('body').addClass('no-swipe');
  BMMenuHandler();
  errorLbl();
  questionnaire();
  checkPG();
  checkLock();
  checkBoxIcons();
  familyTreeProcessing();
  showLargeRowItems();
  thinner();
//  ellipsis();
  $('.chosen-select').chosen({placeholder_text_multiple: " ", placeholder_text_single: " ", width: "100%"});
  // Chosen touch support.
  if ($('.chosen-container').length > 0) {
    $('.chosen-container').on('touchstart', function(e){
      e.stopPropagation(); e.preventDefault();
      // Trigger the mousedown event.
      $(this).trigger('mousedown');
    });
  }
//  if(!$('.InnerTabSelected').length && $('.InnerTabUnSelected').length){$('.InnerTabUnselected').eq(0).click()};
  if($('.contactData').length){$('.contactData').height($('.contactData > div').height()+'px');}    // WSC-3497
  if(!$('.InnerTabSelected').length && $('.InnerTabUnSelected').length){$('.InnerTabUnselected').last().click()};
  $('input.searchItem[title]').each(function(){$(this).attr('placeholder', $(this).attr('title'))});
  if(typeof(childCheck)=='function') setTimeout("childCheck()",100);
  logperf();
}

window.onbeforeunload = function(){
  $('#menuShim').addClass('active');
}

function checkLogin(){
  if($('div.login').length){
    $('body')
      .attr('class', '')
      .addClass('login');
  }else{
    $('body').removeClass('login')
    if(!('ontouchstart' in document.documentElement)){$('body').addClass('no-touch')}
  }
}
function tapCallback(){
  if($('div.tree').length){
    var width = $('div.tree > ul > li').width();
    $('div.tree').width(width+100);
  }
//  if(!$('.InnerTabSelected').length && $('.InnerTabUnSelected').length){$('.InnerTabUnSelected').eq(0).click()};
  if(!$('.InnerTabSelected').length && $('.InnerTabUnSelected').length){$('.InnerTabUnSelected').last().click()};
  familyTreeProcessing();
  errorLbl();
  checkBoxIcons();
  showLargeRowItems();
//  ellipsis();
  if(typeof(childCheck)=='function') setTimeout("childCheck()",100);
  logperf(1);
}
function BMMenuHandler(){                                   // -- Handle BlueMagic dropdown menu --
  var SelectedSubMenuItem = $('.navXmenu li[id$="'+SelectedSubMenu+'"]');
  $(SelectedSubMenuItem).addClass('active');
  $(SelectedSubMenuItem).parent().closest('li[id^=ITM_]').addClass('active');
  $(SelectedSubMenuItem).closest('li[id^=MNU_]').addClass('active');

  $('body>form').prepend($('<div/>').attr('id', 'menuShim'))

  $('#menuIcon').on('tap', function(){
    $('.nav_menu').toggleClass('active');
    $('#menuShim').toggleClass('active');
  })
  $('#menuShim').on('tap', function(){
    if($('.nav_menu').hasClass('active')){
      $('.nav_menu').removeClass('active');
      $('#menuShim').removeClass('active');
    }
  })
  $('ul.navXmenu > li > ul > li').on('tap', function(){     // click on menu level 2 (bottom)
    if($(this).find('ul')){
      $(this).siblings().find('ul').removeClass('active');  // close all other level 3 menu popouts
      $(this).find('ul').toggleClass('active');             // open this level 3 menu popout
      $('#menuShim').toggleClass('active');                 // toggle overlay
    }
  })
  $('ul.navXmenu > li > ul > li > ul > li').on('tap', function(e){    // click on menu level 3 (popout)
    $(this).trigger('click');
  })
  $('ul.navXmenu > li').on('tap', function(e){              // click on menu level 1 (left)
    $('ul.navXmenu > li').removeClass('active');
    $(this).toggleClass('active');
    $('.nav_menu').removeClass('active');
    $('#menuShim').removeClass('active');
    if(e.target.id==$(this).attr('id') || $(e.target).parent().attr('id')==$(this).attr('id')){
      if($(this).attr('onclick')){$(this).trigger('click')}
      else{$(this).find('li').eq(0).trigger('click')}
    }
  })
  $('.navXmenu').each(function(i,o){              // parse all root menus
    var items = $(this).find('>li>ul>li');        // get sub-menus
    var itemsL = $(this).find('>li>ul>li>ul');    // get leaf-menus
    items.css('width', 100/items.length+'%');     // scale sub-menus width depending on amount of items
    if(items.length){
      if(itemsL.length) $(this).find('>li').attr('data-click','true')        // no click action to main menu, mark to trigger first child
      else              $(this).find('>li').attr('onclick', $(this).find('>li>ul>li').eq(0).attr('onclick'))    // move click action to main menu
    }
    if(items.length == 1){                        // if single sub-menu
      $(this).find('>li>ul').remove();            // remove sub-menu
      $(this).find('i.icon').remove();            // remove root menu icon (menucaret)
    }else if(items.length < 4){
      items.css('left', (100/items.length/items.length)+'%');
    }
  })
}

function getViewport(){
  var vh = window.innerHeight;
  var vw = window.innerWidth;
  return {
    vh: vh,
    vw: vw,
    vmax: Math.max(vw, vh),
    vmin: Math.min(vw, vh)
  };
}
function thinner(){
  $('.th-inner').each(function(){
    $(this).css('width', ($(this).parent().width()-8)+'px');
  })
}
var questionnaire = function(){
  $('#BUT_QUESTIONNAIRE_STEP1_NEXT').on('§tap', function(event){
    $("#QUESTIONNAIRE_STEP1_POPUP").dialog("close");
    $("#QUESTIONNAIRE_STEP2_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#BUT_QUESTIONNAIRE_STEP2_BACK').on('tap', function(event){
    $("#QUESTIONNAIRE_STEP2_POPUP").dialog("close");
    $("#QUESTIONNAIRE_STEP1_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#BUT_QUESTIONNAIRE_STEP2_NEXT').on('tap', function(event){
    $("#QUESTIONNAIRE_STEP2_POPUP").dialog("close");
    $("#QUESTIONNAIRE_STEP3_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#BUT_QUESTIONNAIRE_STEP3_BACK').on('tap', function(event){
    $("#QUESTIONNAIRE_STEP3_POPUP").dialog("close");
    $("#QUESTIONNAIRE_STEP2_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#BUT_QUESTIONNAIRE_STEP3_NEXT').on('tap', function(event){
    ajaxButtonAction('', '__C3F5DD4AEB6C97B8 FormButton 130', 'BUT_C3F5DD4AEB6C97B860312', false, null, '', 'servletcontroller', '', false, true, '');
    $("#QUESTIONNAIRE_STEP3_POPUP").dialog("close");
    $("#QUESTIONNAIRE_STEP4_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#BUT_QUESTIONNAIRE_STEP4_BACK').on('tap', function(event){
    $("#QUESTIONNAIRE_STEP4_POPUP").dialog("close");
    $("#QUESTIONNAIRE_STEP3_POPUP").dialog("open");
    event.preventDefault();
  });
}
function checkPG(){
  // Report creation popup
  $("#BUT_3C55BA1197968811128553").on('tap', function(event){
    var isOpen = $("#REPORT_CREATION_POPUP").dialog( "isOpen" );
    if (isOpen) {
      $('#REPORT_CREATION_POPUP').dialog("close");
    } else {
      ajaxButtonAction(null, '__A0AE6D5424208DE1 FormButton 58', 'BUT_A0AE6D5424208DE1147628', false, null, '', 'servletcontroller', '', false, true, '');
      $('#REPORT_CREATION_POPUP').dialog("open");
    }
    event.preventDefault ? event.preventDefault() : event.returnValue = false;
  });
}

function checkBoxIcons(){
  $('.formItem.CheckBoxIcon').each(function(i,o){
    var checkbox = $(o).find('input[type="checkbox"]');
    if($(checkbox).is(':checked')) $(o).addClass('checked');
    else $(o).removeClass('checked');
    $(checkbox).on('click', function(){
      if($(this).is(':checked')) $(this).parent().addClass('checked');
      else $(this).parent().removeClass('checked');
    })
  });
}

function ChartLink(o,d){
  $('#QUE_ChartValueRedirector').val(o.name);
  $('#'+d).trigger('click');
}

/* ----- Helper Functions ----- */
function debounce(func, wait, immediate){
  var timeout;
  return function(){
    var context = this, args = arguments;
    clearTimeout(timeout);
    timeout = setTimeout(function(){
      timeout = null;
      if(!immediate) func.apply(context, args);
    }, wait);
    if(immediate && !timeout) func.apply(context, args);
  };
};

jQuery.expr.filters.offscreen = function(el) {
  var rect = el.getBoundingClientRect();
  return (rect.left > window.innerWidth);
};

function showLargeRowItems(){
  $('.tableContainer table:visible').each(function(i,t){
    var tdvlen = $(t).find('th:visible').length
    if(!tdvlen) return true;
    var tdtxtr = $(t).find('tbody tr').eq(0).find('td.Text:not(.hidden)'),
        tdall  = $(t).find('tbody tr').eq(0).find('td:not(.hidden)'),
        twidth = $(t).width(),
        tcmwt  = $(t).data('cwtm') || 8,
        tdtw   = 200;
    if(!showLargeRowItems.caller || showLargeRowItems.caller.name != 'tapCallback'){     // calculate text length only when not coming from AJAX (else pagination takes 8 sec)
      $(t).find('tbody tr td.Text:visible').each(function(j,o){
        var textWidth = $(this).text().trim().length*tcmwt; // $(this).text().trim().width($(this).css("font-size"), $(this).css("font-family"), $(this).css("font-weight"));
        var maxWidth = $(this).find('span[style]').css('max-width');
        if(maxWidth != undefined && parseInt(maxWidth) < textWidth){
          $(this).find('span')
            .addClass('large')
        }else if(tdtw < textWidth && $(this).text().trim().indexOf(' ')<0){
          $(this).find('span')
            .addClass('large')
            .css('max-width', tdtw+'px')
        }
      })
    }
    if($(t).find('td.hidden, span.large').length && !$(t).hasClass('showMoreEnabled')){
      $(t).addClass('showMoreEnabled')
      $(t).find('tr.rowClick, tr.subrow').each(function(j,r){
        $(r).find('td.Text:not(.hidden)').eq(0).addClass('showmore').on('click', function(){showRowInfo(this)});
      })
    }
    if($(t).hasClass('treetable')){
      $(t).addClass('showMoreEnabled');
      $(t).find('tr.leaf').each(function(i,r){    // enable showmore only on leafs (???)
        var firstCell = $(r).find('td:first');
        var chars = $(firstCell).text().trim().length;
        var rclass = $(this).attr('class') || '',
            lpos = rclass.indexOf('level'),
            level = parseInt(rclass.substr(lpos+5,2));
        if(chars > 40-level*8){
          $(firstCell).addClass('showmore ttcctt');
          var indentwidth = level*20-5+'px';
          $(firstCell).find('span.indenter').css('padding-left', indentwidth);
          $(firstCell).on('click', function(){showRowInfo(this,1)})
        }
      })
    }
  });
};
function showRowInfo(tti){
  var tr = (tti.tagName == 'TD') ? $(tti).closest('tr') :  $(tti).closest('tr').prev();
  if(!$(tr).next().hasClass('CTT')){
    o = $('<tr/>')
      .attr('id', 'CTT_'+$(tr).attr('id').split('_').pop())
      .addClass('CTT')
      .append($('<td colspan="60"/>')
        .html(getCTT)
      );
    o.insertAfter(tr);
    $(tr).next().toggleClass('open');
    $(tr).next().find('div').slideToggle();
    moveCTT();
  }else{
    $(tr).next().toggleClass('open');
    $(tr).next().find('div').slideToggle();
    moveCTT();
  }
  function getCTT(){
    var ctt = $('<div style="display: none;" onclick="showRowInfo(this)"/>'),
        header = tr.closest('table').find('thead tr th');
//    tr.find('td').each(function(a,c){                                 // get info for all cells in row
    tr.find('td.hidden, td.Text, td.ttcctt').each(function(a,c){      // get info for text and hidden cells
      if($(c).hasClass('CTThide')) return;
      var tdix = $(c).index();
//      var thtxt = header.eq(tdix).text().trim();
      var thtxt = header.eq(tdix).find('span.ttt').length ? header.eq(tdix).find('span.ttt').text().trim() : header.eq(tdix).text().trim();
      var celltxt = $(c).text().trim();
      if(thtxt!='' && celltxt!='') ctt.append('<label>'+thtxt+'</label><span>'+celltxt+'</span><br/>');
    })
    return ctt;
  }
  function moveCTT(d){
    var theight = parseInt($(tr).closest('.tableContainer').css('max-height')),
        CTTtop = $(tr).next().offset().top;
    if(!$(tr).next().hasClass('open')){
       $(tr).closest('table').animate({ "marginTop": 0 }, 300);
    }else if(CTTtop > theight+75){
      $(tr).closest('table').animate({ "marginTop": -108+"px" }, 300);
    }
  }
}

function showCTT(tti){
  $('tr.CTT').remove();
  var td = $(tti).closest('td'),
      tr = $(tti).closest('tr');
  var txt = td.find('span').text().trim();
  o = $('<tr/>')
    .addClass('CTT')
    .append($('<td colspan="60"/>')
      .text(txt)
      .on('click', function(){$(this).parent().remove()})
    )
    .insertAfter(tr);
}

String.prototype.width = function(fontSize, fontFamily, fontWeight){
  var fs = fontSize || '14px',
      ff = fontFamily || 'Roboto',
      fw = fontWeight || 'normal',
      o = $('<div>' + this + '</div>')
        .css({'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font-size': fs, 'font-family': ff, 'font-weight': fw})
        .appendTo($('body')),
      w = o.width();
  o.remove();
  return w;
}

function ellipsis(){
  $('div.ellipsis').bind('mouseenter', function(){
    console.log('ellipsis found');
    var $this = $(this);
    if(this.offsetWidth < this.scrollWidth && !$this.attr('title')){
      $this.attr('title', $this.text());
    }
  });
}
