
		// override value in connect ajax for WSC-2370
		function getTimeoutForAjaxQuestionAction( elem )
		{
			return 200;
		}

		function ajaxButtonAction(ids, btn, buttonId, p_valMand, p_rowId, ns, controllerName, context, disableInput, inlineErrors, buttonConfirmMsg)
{
	//bail out if button is disabled...
	if (jscss('check',document.getElementById(buttonId),DISABLED_CLASS))
	{
		return false;
	}

	var compID = getCompID(ns, btn);
	if ( compID != null )
	{
	    btn = compID + btn.substring(compID.length+2);
	}
	else
	{
	    btn = btn.substring(2);
	}

	    ids = findIdsToValidate(buttonId, ids, ns);
    if (!validateDependentItems(ids, btn, p_valMand, p_rowId, ns, inlineErrors)){
		return false;
	}

	if (!this["beforeAjaxButtonActionService"] || beforeAjaxButtonActionService(controllerName, ns, context, btn, disableInput, buttonId))
	{
		if (!displayConfirmMsg(buttonConfirmMsg)){
		    return false;
		}
		
		//call out to row clicks (if there are any) to set selectors...
		var obj = document.getElementById(buttonId);
		var aRow = obj;
		while((aRow = getParentRow(aRow)) != null){
			if(aRow.onclick){
				 execute(aRow, "onclick", CHANGED_SELECTION_TRIGGER);
			}				
		}
		
		urlProps = buildFormUrlParameters(ns, false, inlineErrors, false).value;
		incQ(ns);
		return postAjaxRunRulesRequest(context, ns, disableInput, 'AJXButtonAction', btn, buttonId, controllerName, urlProps);
	}
}