/* WSC custom JavaScript v 1.6.0 */

Hi.addHook('postProcessResponses', initChosen);
Hi.addHook('postProcessResponses', makeElementClickable);
Hi.addHook('postProcessResponses', errorLbl);
Hi.addHook('postProcessResponses', removeTheadOnEmptyData);
Hi.addHook('afterChangeTab',  function() {
  $(".wsc-setActiveTab").each(function() {
    if ($(this).is(':visible')) {
      $(this).click();
    }
  });
	return true;
});

var now = new Date().valueOf(), its = '';
var ClickElmnt, ClickPhase, FromMenu;
$(document).ready(function(){
  var readystamp = new Date().valueOf();
//  UtilModule.fixFooter();
  initChosen();
  errorLbl();
  removeTheadOnEmptyData();
  $('body').on('change', '.changePortfolioDropdown', function(){$("button.changePortfolioButton").click()})
  $('body').addClass(wsc.bua).attr('data-res', wsc.res()[2]);
  if(typeof uniqueVal != 'undefined') uniqueVal();
  menuHandling();
  setMenuHeight();
  if($('html').hasClass('PMS')){PMaddon(); if(ss.get('PMS_info') != null){$('body').addClass('aside-active')}}
  wsc_ellipsis();
  addTooltipOnEllipsis();
  logperf();
});

function wscCallback(){
  if(typeof cm != 'undefined' && $('#'+cm.tid).length){cm.setup(cm.tid)}
  if(typeof uniqueVal != 'undefined') uniqueVal();
  if(its.length && its != $('.tc-tab-header-selected').attr('id')){$('#'+its).trigger('click')}     // selected tab update
  wsc_ellipsis();
  initChosen();
  errorLbl();
  logperf(1);
}
function customCallback(r){
  logperf(1);
}
window.onclick = function(e){
  ss.set('ClickPhase',$(e.target).closest('[class^=Phase_]').attr('class')||'Page');
  ss.set('ClickElmnt',$(e.target).prop("tagName")+(e.id?' (#'+e.id+')':'')+(e.className?'.'+e.className:''));
  ss.set('FromMenu',!!$(e.target).closest('.cd-primary-nav').length);
};

function menuHandling(){                                    // ----- MENU handling -----
  $('a.activeMenuItem').removeClass('activeMenuItem');      // Active Menu Item display marker reset
  $('.menu_container li.active').removeClass('active');     // Active PM Menu Item display marker reset
  $('.menu_container li.tc-current-active').removeClass('tc-current-active');     // Active PM Menu Item display marker reset
  if(ss.get('FromMenu')!=='true'){
    var sM = $('li[id $= _'+$('body').data('phase')+']');   // find Active Menu Item
    var typeA = $(sM).closest('.tc-accordion-menu').length;
    while(sM.length){                                       // Add markers to item and parent items
      sM.addClass('active')
      if(typeA){
        sM=sM.parent().closest('li');
      }else{
        sM.find('a').first().addClass('activemenuitem');
        sM=sM.parent().closest('li.has-children');
        if(!$(sM).attr('style')==''){sM.find('a').first().addClass('activemenuitem');}
      }
    }
  }else{
    $('a[activemenuitem]').addClass('activemenuitem')
      .closest('li.has-children').find('a').eq(0).addClass('activemenuitem')    // mark activemenuitem parent as selected
      .parent().parent().closest('li.has-children').find('a').eq(0).addClass('activemenuitem')    // mark activemenuitem parents parent as selected
      .parent().parent().closest('li.has-children').find('a').eq(0).addClass('activemenuitem')    // mark activemenuitem parents parents parent as selected
  }
}
function setMenuHeight(){
  if(wsc.res()[1] < 1000) return false;
  var menuSubItems = [];
  $('.cd-secondary-nav').find('ul').each(function(i,m){if(!$(m).find('ul').length){menuSubItems.push($(m).find('li').length);}})
  $('.cd-secondary-nav').find('ul').each(function(i,m){if(Math.max(...menuSubItems)>5){$(m).css('min-height',Math.max(...menuSubItems)*42+4+'px')}})
}
function PMaddon(){
  if($('.aside').length){$('body').addClass('hasAside'); if(!!$('body.aside-active')){$('.aside').hover(function(){$('body').addClass('aside-hover')}, function(){$('body').removeClass('aside-hover')})}};
  if($('.Phase_SearchAccess').length) $('body').addClass('hasSearchAccess');
  $('.tc-card').each(function(i,c){var cid = $(c).attr('id'); if(ss.get(cid)=='closed'){toggleCard(c,1)}; if(ss.get(cid)=='hclosed'){toggleCardH(c,1)}});
  $('div.wsc-pm-tabs a.tc-tab-header.dragenabled').draggable({distance: 100, containment: 'document', stop: function(e,ui){$(e.target).nextAll('.tab-new:first').click()}});
//  $("#dropzone" ).droppable({accept:".dragenabled", drop: function(e,ui){console.log('Tab dropped')}});
}
function wsc_ellipsis(){
  if($('.wsc-ellipsis').length){setTimeout("$('.wsc-ellipsis').each(function(){$(this).attr('title', $(this).text().trim())})", 100)}
}

var perf = window.performance ||                  // performance API initialization
           window.msPerformance ||
           window.mozPerformance ||
           window.webkitPerformance;
var has = {};
has.console = !!window.console;
has.touch = 'ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;       // device with touchscreen?
has.ss = !!window.sessionStorage;                 // browser knows sessionStorage?
has.ls = !!window.localStorage;                   // browser knows localStorage?
has.svg = !!window.SVGElement;                    // browser supports svg?
has.perf = !!perf;                                // browser supports performance API?
has.formdata = !!window.FormData;                 // browser supports FormData API?
has.worker = !!window.Worker;                     // browser supports Worker?
has.postmessage = !!window.postMessage;           // browser supports Web Messaging API?
has.file = !!(window.File && window.FileReader && window.FileList && window.Blob); // file API

var headers = headers || [];
var dateReg = /^\d{2}[./-]\d{2}[./-]\d{4}$/
function jq(oid){return('#'+oid)}

function initChosen(){
  $('.chosen-select').chosen({placeholder_text_multiple: " ", placeholder_text_single: " ", width: "auto"});
}
function initTreeView(){
  $('.node.jsplumb-connected').on('click', function(e){$(this).find('.node-exec').trigger('click')});
}
function makeElementClickable(){
  $('.tc-table-answer.exec')
    .each(function(i,o){var titl = $(o).find('a.tc-icon').attr('title');$(o).attr('title', titl);$(o).find('.tc-form-control').attr('title', titl);}) // set tooltip
    .on('click', function(e){
      var clickItem = $(this).find('a.tc-icon');
      if(!$(e.target).is('.tc-icon') && e.target.nodeName != 'INPUT'){clickItem.trigger('click')}
    })
};
function removeTheadOnEmptyData(){                // removes table header if no data in rows.
  $('.tc-table tbody span.tc-empty-message , .tc-table tbody span.tc-empty-msessage').each(function(){
    $(this).closest('.tc-table').find('thead').hide();
  })
//  $('.tc-table').each(function(i,t){if($(t).find('td').length == 1 && $(t).find('td[colspan]'))$(t).find('thead').hide()})
}

/* ----- Handle TAP server messages as growl ----- */
var errorLblT = {
  'ERROR' :   ['ERROR','ERREUR'],
  'WARNING' : ['WARNING','AVERTISSEMENT'],
  'INFO' :    ['INFO','INFO'],
  'USER' :    ['Welcome,','Bienvenue,'],
  'CONFIRMATION' :    ['CONFIRMATION','CONFIRMATION']
}
function errorLbl(){          // use growl to display messages
  if($('.errorLabel').is(':visible')){
    $('#growls .growl').remove();
    $.each($('.errorLabel div[id]'), function(i,o){
      if(!$(o).text().length) return;
      var msgs = $(o).text().split('<br>');
      for(var m = 0; m < msgs.length; m++){
        var msg = msgs[m];
        if(msg.split('|').length == 1) msg = 'INFO|' + msg;
        var omsg = msg.split('|'),
            title = omsg[0].trim(),
            message = omsg[1].trim(),
            setting = {
              title: errorLblT[title][LANGUAGE=='French'?1:0],
              message: message,
              icon:  'icon-'+title.toLowerCase(),
              fixed: (title=='ERROR' || title=='WARNING'),
              duration: 3500,
              cb: function(){}
            }
        if(title=='ERROR') $.growl.error(setting);
        else if(title=='WARNING') $.growl.warning(setting);
        else $.growl.notice(setting);
      }
    })
    $('.errorLabel').hide();
  }
  if($('div.errorLabel div').text().trim().length == 0){
    $('#growls .growl').remove();                           // remove growls if no more message from TAP
  }
}
/* ----- End Handle TAP server messages ----- */

$.expr.filters.offcontentarea = function(el) {
  var rect = el.getBoundingClientRect();
  return (
           (rect.left + rect.width) < 0
             || (rect.top + rect.height) < 0
             || (rect.left > $('.tc-content-area').width() || rect.top > $('.tc-content-area').height())
         );
};

/* ----- ----- Helper Functions ----- ----- */
function FormatNumber(x){                         // Unformatted valid JS Number (en) to locale formatting
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, tSep);
  return parts.join(dSep);
}
function RawNumber(x, ds){                        // Remove country specific formatting of numbers and return valid JS unformatted number
  var parts = x.toString().split(ds||dSep);       // ds = decimal separator defined to override default, in case of always raw numbers or formatted in edge Connect with e.g.: xxx.format('#,##0.00'). If undefined, use default (made available in tpl file).
  parts[0] = parts[0].replace(/[^0-9\-]+/g, '');
  return parts.join(".");
}
function FormatDate(rawdate){
  var df = typeof dateFormat == 'string' ? dateFormat : 'dd/mm/yyyy';
}
function RawDate(date){
  var df = typeof dateFormat == 'string' ? dateFormat : 'dd/mm/yyyy';
  var separator = df.match(/[^dmy]/);
  var dsplit = df.split(separator[0]);
  var datesplit = date.split(separator[0]);
  var day = datesplit[dsplit.indexOf("dd")];
  var month = datesplit[dsplit.indexOf("mm")];
  var year = datesplit[dsplit.indexOf("yyyy")];
  var d = new Date(year,month,day);
  return d.valueOf();
}
function unique(array){
  return array.filter(function(el,index,arr){
    return index == arr.indexOf(el);
  });
}
var ls = {                                        // ----- localStorage (k=key, v=value) -----
  get: function(k){return localStorage.getItem(k);},
  set: function(k,v){localStorage.setItem(k,v);},
  clear: function(k){localStorage.removeItem(k);},
  clearall: function(){localStorage.clear();},
  list: function(){return window.localStorage;}
}
var ss = {                                        // ----- sessionStorage (k=key, v=value) -----
  get: function(k){return sessionStorage.getItem(k);},
  set: function(k,v){sessionStorage.setItem(k,v);},
  clear: function(k){sessionStorage.removeItem(k);},
  clearall: function(){sessionStorage.clear();},
  list: function(){return window.sessionStorage;}
}
jQuery.fn.htmlClean = function(){                 // Usage: e.g.: $('table:not(".minified")').htmlClean().addClass('minified');
  this.contents().filter(function(){
    if (this.nodeType != 3){
      $(this).htmlClean(); return false;
    }else{
      this.textContent = $.trim(this.textContent);
      return !/\S/.test(this.nodeValue);
    }
  }).remove();
  return this;
}
function reorderTableRows(tid,dataitem,order){
  var table = $('#'+tid);
  var rows = table.find('tbody tr').get();
  rows.sort(function(a,b){
    var keyA = $(a).data(dataitem);
    var keyB = $(b).data(dataitem);
    if (keyA < keyB) return (order=='asc')?-1:1;
    if (keyA > keyB) return (order=='asc')?1:-1;
    return 0;
  });
  $(rows).each(function(i,r){
    table.children('tbody').append(r);
  });
}
function reorderLists(lid,dataitem,order){
  var list = $(lid);
  var items = list.find('>li').get();
  items.sort(function(a,b){
    var keyA = $(a).data(dataitem);
    var keyB = $(b).data(dataitem);
    if (keyA < keyB) return (order=='asc')?-1:1;
    if (keyA > keyB) return (order=='asc')?1:-1;
    return 0;
  });
  $(items).each(function(i,r){
    list.append(r);
  });
}
var wsc = {
  res: function(){
    var scrwidth   = window.innerWidth;
    var reswidth   = $('.tc-content-area').eq(0).width();
    var resolution = reswidth<568?'xs':reswidth<768?'sm':reswidth<1024?'md':reswidth<1280?'lg':scrwidth<1600?'xl':'xxl';
    return [scrwidth,reswidth,resolution];
  },
  perf: function(){
    if(!window.__profiler||window.__profiler.scriptLoaded!==true){
      var h=document.getElementsByTagName("head")[0],
          s=document.createElement("script"),
          c=function(){
            window.__profiler=window.__profiler||new __Profiler();
            window.__profiler.init();
            __profiler.scriptLoaded=true;
          },
          t=new Date();
      s.type="text/javascript";
      s.src="./html/js/profiler.js";
      s.onload=c;
      s.onreadystatechange=function(){console.log(arguments);if(this.readyState=="loaded"){c()}};
      h.appendChild(s);
    }else if(window.__profiler instanceof __Profiler){
      window.__profiler.init()
    }
  },
  bua: function(ba){
    var ua = ba ? ba : navigator.userAgent;
    var UA = '';
    UA += ANDROID ? ' Android' : '';
    UA += IOS ? ' iOS' : '';
    UA += ua.match(/iPad/i) ? ' iPad' : '';
    UA += ua.match(/iPhone/i) ? ' iPhone' : '';
    UA += ua.match(/iPod/i) ? ' iPod' : '';
    UA += ua.match(/MSIE/i) ? ' MSIE' : '';
    UA += ua.match(/Trident/i) ? ' Trident' : '';
    UA += ua.match(/Firefox/i) ? ' Firefox' : '';
    UA += ua.match(/Safari/i) && !ua.match(/Chrome/i) && !ua.match(/Edge/i) && !ua.match(/OPR/i) ? ' Safari' : '';
    UA += ua.match(/Chrome/i) && !ua.match(/Edge/i) && !ua.match(/OPR/i) ? ' Chrome' : '';
    UA += ua.match(/Edge/i) ? ' Edge' : '';
    UA += ua.match(/OPR/i) ? ' Opera' : '';
    UA += ua.match(/Windows/i) ? ' Win' : '';
    UA += ua.match(/Macintosh/i) ? ' Mac' : '';
    UA += ua.match(/Linux/i) ? ' Linux' : '';
    return UA;
  }
}



if(!$('body[data-section="assets"]').length){
  ss.clear('multiorder'); multiorder = {};
  ss.clear('mocont');     mocont = '';
}


/* Start Performance Logger */
function logperf(ajx){
  if(!has.perf) return false;
  var pt  = perf.timing,
      pn  = perf.navigation,
      now = new Date().valueOf(),
      sta = [];
  if(!ajx){
    var Navigation      = (pn.type==0?'Navigation':pn.type==1?'Reload':pn.type==2?'Back/Forward':'unknown');
    var totalduration   = ((now-pt.navigationStart)/1000).toFixed(3),
        serverduration  = ((pt.responseEnd-pt.navigationStart)/1000).toFixed(3),
        loaddom         = ((pt.domInteractive-pt.responseEnd)/1000).toFixed(3),
        JSduration      = ((now-pt.domInteractive)/1000).toFixed(3);
  }else{
    if(typeof AJAXstartstamp != 'number' || typeof AJAXcompletestamp != 'number') return false;
    var Navigation      = 'AJAX';
    var totalduration   = ((now-AJAXstartstamp)/1000).toFixed(3),
        serverduration  = ((AJAXcompletestamp-AJAXstartstamp)/1000).toFixed(3),
        loaddom         = ((now-AJAXcompletestamp)/1000).toFixed(3),
        JSduration      = (0/1000).toFixed(3);
  }
  var stabs           = $('.tc-tab-header-selected:visible span:not(.tc-badge)').clone().children().remove().end().text().trim();
  var docsize         = (document.body.innerHTML.length/1024).toFixed(3);
  var totsize         = ($('html').html().length/1024).toFixed(3);
  var pstatus         = $('.Phase_ErrorPhase').length ? 'Error' : 'OK';

  if(has.ls && ls.get('debugmode')!=null){
    console.info(
        'Page Reference         : '+$('body').data('phase')+' on '+$('body').attr('class')+', res: '+wsc.res()[0]+' / '+wsc.res()[1]+' ('+wsc.res()[2]+')'+
      '\nLast clicked on        : '+ss.get('ClickPhase')+' / '+ss.get('ClickElmnt')+
      '\nStatus                 : '+pstatus+
      '\ncorrelationId          : '+correlationId+
      '\nNavigation             : '+Navigation+
      '\nTotal Request Duration : '+totalduration+' sec'+
      '\nServer Side Duration   : '+serverduration+' sec'+
      '\nDOM Content Loaded     : '+loaddom+' sec'+
      '\nJS Exec Duration       : '+JSduration+' sec'+
      '\nSelected Tab           : '+stabs+
      '\nDocument Size          : '+docsize+' KB  (inline total: '+totsize+' KB)'
    );
  }
  if(!!profurl){
    (new Image()).src = profurl+        // send performance data to logfile
        '?cid='+correlationId+          // unique token (correlationId)
        '&page='+$('body').data('phase')+
        '&b='+theme+wsc.bua()+          // Info about theme / browser / system
        '&trd='+totalduration+          // the duration from navigation start to now
        '&ssd='+serverduration+         // the duration server time
        '&ldc='+loaddom+                // the duration for DOM loading
        '&js='+JSduration+              // the duration of JavaScript execution
        '&stat='+pstatus+               // Page status
        '&size='+docsize+               // the size of the document
        '&stabs='+stabs+' '+pstatus+    // selected tabs + OK/Error
        '&ref='+Navigation+
        '&info='+window.location.pathname.split('/')[1]+' / '+wsc.res()[2];
  }
}
/* End Performance Logger */

function addTooltipOnEllipsis(){
	$('.showTooltipOnEllipsis').hover(function(){
	 if (this.clientWidth<this.scrollWidth) {
      $(this).attr('title', $(this).text().trim());
    }else{
	    $(this).attr('title', '');
	  }
  });
}
