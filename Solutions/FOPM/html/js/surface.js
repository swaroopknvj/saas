﻿/* ----- Surface JavaScript -----
 * Copyright (c) 2016 Temenos, All rights reserved.
 *
 * This source code is protected by copyright laws and international copyright treaties,
 * as well as other intellectual property laws and treaties.
 *
 * Access to, alteration, duplication or redistribution of this source code in any form
 * is not permitted without the prior written authorisation of Temenos.
 */

$(document).ready(function(){
  checkLogin();
  if(!('ontouchstart' in document.documentElement)) $('body').addClass('no-swipe');
  BMMenuHandler();
  handleJCarousel();
  errorLbl();
  checkPG();
  checkLock();
  checkBoxIcons();
  familyTreeProcessing();
  $('input.searchItem[title]').each(function(){$(this).attr('placeholder', $(this).attr('title'))});
//  $('.hslider, .tableContainer').on('scroll', function(e){$(this).toggleClass('notouch');})
  $('.chosen-select').chosen({placeholder_text_multiple: " ", placeholder_text_single: " ", width: "100%"});
  // Chosen touch support.
  if ($('.chosen-container').length > 0) {
    $('.chosen-container').on('touchstart', function(e){
      e.stopPropagation(); e.preventDefault();
      // Trigger the mousedown event.
      $(this).trigger('mousedown');
    });
  };
//  $('td.showmore').on('click', function(){showRowInfo(this,1)});
});

window.onbeforeunload = function(){
  $('#menuShim').addClass('active');
}

function checkLogin(){
  if($('body[data-section="login"]').length && $('#C1__FMT_login').length){
    $('body[data-section="login"]')
      .addClass('login')
      .find($('#C1__FMT_login'))
        .addClass('login');
  }
}
function tapCallback(){
  if($('div.tree').length){
    var width = $('div.tree > ul > li').width();
    $('div.tree').width(width+100);
  }
  familyTreeProcessing();
  errorLbl();
  handleJCarousel();
  checkBoxIcons();
}
function BMMenuHandler(){                                   // -- Handle BlueMagic dropdown menu --
  var SelectedSubMenuItem = $('.navXmenu li[id$="'+SelectedSubMenu+'"]');
  $(SelectedSubMenuItem).addClass('active');
  $(SelectedSubMenuItem).parent().closest('li[id^=ITM_]').addClass('active');
  $(SelectedSubMenuItem).closest('li[id^=MNU_]').addClass('active');

  $('body>form').prepend($('<div/>').attr('id', 'menuShim'))

  $('#menuIcon').on('tap', function(){
    $('.nav_menu').toggleClass('active');
    $('#menuShim').toggleClass('active');
  })
  $('#menuShim').on('tap', function(){
    if($('.nav_menu').hasClass('active')){
      $('.nav_menu').removeClass('active');
      $('#menuShim').removeClass('active');
    }
  })
  $('ul.navXmenu > li > ul > li').on('tap', function(){     // click on menu level 2 (bottom)
    if($(this).find('ul')){
      $(this).siblings().find('ul').removeClass('active');  // close all other level 3 menu popouts
      $(this).find('ul').toggleClass('active');             // open this level 3 menu popout
      $('#menuShim').toggleClass('active');                 // toggle overlay
    }
  })
  $('ul.navXmenu > li > ul > li > ul > li').on('tap', function(e){    // click on menu level 3 (popout)
    $(this).trigger('click');
  })
  $('ul.navXmenu > li').on('tap', function(e){              // click on menu level 1 (left)
    $('ul.navXmenu > li').removeClass('active');
    $(this).toggleClass('active');
    $('.nav_menu').removeClass('active');
    $('#menuShim').removeClass('active');
    if($(this).attr('data-click') && (e.target.id==$(this).attr('id') || $(e.target).parent().attr('id')==$(this).attr('id'))){
      $(this).find('li').eq(0).trigger('click');
    }
  })
  $('.navXmenu').each(function(i,o){              // parse all root menus
    var items = $(this).find('>li>ul>li');        // get sub-menus
    var itemsL = $(this).find('>li>ul>li>ul');    // get leaf-menus
    items.css('width', 100/items.length+'%');     // scale sub-menus width depending on amount of items
    if(items.length){
      if(itemsL.length) $(this).find('>li').attr('data-click','true')        // no click action to main menu, mark to trigger first child
      else              $(this).find('>li').attr('onclick', $(this).find('>li>ul>li').eq(0).attr('onclick'))    // move click action to main menu
    }
    if(items.length == 1){                        // if single sub-menu
      $(this).find('>li>ul').remove();            // remove sub-menu
      $(this).find('i.icon').remove();            // remove root menu icon (menucaret)
    }else if(items.length < 4){
      items.css('left', (100/items.length/items.length)+'%');
    }
  })
}

function checkPG(){           // PG target investment profile
  $('body').on('change', '.IP_SELECT', function(event){
    var isOpen = $("[id*=IP_CONFIRM_POPUP]").dialog( "isOpen" );
    if (isOpen) {
      $("[id*=IP_CONFIRM_POPUP]").dialog("close");
    } else {
      $("[id*=IP_CONFIRM_POPUP]").dialog("open");
    }
    event.preventDefault();
  });
}
function handleJCarousel(){
  if($('.jcarousel').length){
    var tabs = $('.jcarousel > ul > li').length;
    $('.jcarousel > ul').css('width', (tabs * 100 +'%'));
    $('.jcarousel > ul > li').css('width', (100/tabs +'%'))
    $('.jcarousel-wrapper').siblings('script').remove();
    $('.jcarousel-wrapper').siblings('style').remove();
    $('.jcarousel > ul > li').each(function(i,o){$(o).addClass('jcitem_'+i)});
    if (requestParam == 'SelectedSearchTabs' && $('body').data('section')=='home'){
      $('.jcarousel > ul').scrollLeft($('.jcarousel > ul > li:first').width());
    }else{
      $('.jcarousel > ul').css('left',0);
    }
  }
  if($('.hslider').length){
    var tabs = $('.hslider > ul > li').length;
    $('.hslider > ul').css('width', (tabs * 100 +'%'));
    $('.hslider > ul > li').css('width', (100/tabs +'%'))
  }
}

function getViewport(){
  var vh = window.innerHeight;
  var vw = window.innerWidth;
  return {
    vh: vh,
    vw: vw,
    vmax: Math.max(vw, vh),
    vmin: Math.min(vw, vh)
  };
}

function checkBoxIcons(){
  $('.formItem.CheckBoxIcon').each(function(i,o){
    var checkbox = $(o).find('input[type="checkbox"]');
    if($(checkbox).is(':checked')) $(o).addClass('checked');
    else $(o).removeClass('checked');
    $(checkbox).on('click', function(){
      if($(this).is(':checked')) $(this).parent().addClass('checked');
      else $(this).parent().removeClass('checked');
    })
  });
}

function ChartLink(o,d){
  $('#QUE_ChartValueRedirector').val(o.name);
  $('#'+d).trigger('click');
}

function showRowInfo(tti){
  var tr = (tti.tagName == 'TD') ? $(tti).closest('tr') :  $(tti).closest('tr').prev();
  if(!$(tr).next().hasClass('CTT')){
    o = $('<tr/>')
      .attr('id', 'CTT_'+$(tr).attr('id').split('_').pop())
      .addClass('CTT')
      .append($('<td colspan="60"/>')
        .html(getCTT)
      );
    o.insertAfter(tr);
    $(tr).next().toggleClass('open');
    $(tr).next().find('div').slideToggle();
    moveCTT();
  }else{
    $(tr).next().toggleClass('open');
    $(tr).next().find('div').slideToggle();
    moveCTT();
  }
  function getCTT(){
    var ctt = $('<div style="display: none;" onclick="showRowInfo(this)"/>'),
        header = tr.closest('table').find('thead tr th');
//    tr.find('td').each(function(a,c){                                 // get info for all cells in row
    tr.find('td.hidden, td.Text, td.ttcctt').each(function(a,c){      // get info for text and hidden cells
      if($(c).hasClass('CTThide')) return;
      var tdix = $(c).index();
//      var thtxt = header.eq(tdix).text().trim();
      var thtxt = header.eq(tdix).find('span.ttt').length ? header.eq(tdix).find('span.ttt').text().trim() : header.eq(tdix).text().trim();
      var celltxt = $(c).text().trim();
      if(thtxt!='' && celltxt!='') ctt.append('<label>'+thtxt+'</label><span>'+celltxt+'</span><br/>');
    })
    return ctt;
  }
  function moveCTT(d){
    var theight = parseInt($(tr).closest('.tableContainer').css('max-height')),
        CTTtop = $(tr).next().offset().top;
    if(!$(tr).next().hasClass('open')){
       $(tr).closest('table').animate({ "marginTop": 0 }, 300);
    }else if(CTTtop > theight+75){
      $(tr).closest('table').animate({ "marginTop": -108+"px" }, 300);
    }
  }
}


/* ----- Helper Functions ----- */
function formatDate(date){
  var df = 'dd/mm/yyyy';                          // "$$ITEM.DATEFORMAT()$"
  var separator = df.match(/[^dmy]/);
  var dsplit = df.split(separator[0]);
  var datesplit = date.split(separator[0]);
  var day = datesplit[dsplit.indexOf("dd")];
  var month = datesplit[dsplit.indexOf("mm")];
  var year = datesplit[dsplit.indexOf("yyyy")];
  var d = new Date(year,month,day);
  return d.valueOf();
}

function debounce(func, wait, immediate){
  var timeout;
  return function(){
    var context = this, args = arguments;
    clearTimeout(timeout);
    timeout = setTimeout(function(){
      timeout = null;
      if(!immediate) func.apply(context, args);
    }, wait);
    if(immediate && !timeout) func.apply(context, args);
  };
};

jQuery.expr.filters.offscreen = function(el) {
  var rect = el.getBoundingClientRect();
  return (rect.left > window.innerWidth);
};
