Hi.addHook('postProcessResponses', InlineEditCheck);
$(document).ready(function(){
  InlineEditCheck();
});

/* --------------------------- *\
   Widget related Functions
\* --------------------------- */

/* ----- wsc PopupInTable START ----- */

function PopupInTable(oid, head, b1, b2, lp){
  $('.PIT').hide();
  var RowNr = oid.split('_R').pop();
  var Oid = $('#'+oid).parent();
  if(!Oid.find('.PIT').length){
    Oid.css('position', 'relative');
    var PIT = $('<div id="PIT_'+oid+'" class="PIT tc-no-prop label-'+lp+'" style="display:none"/>');
    $('#'+oid).parent().append(PIT);
    PIT.append($('<div class="body"/>'));
    PIT.prepend($('<div class="head"/>').append($('<span/>').text(head)).append($('<a href="javascript:void(0)" class="tc-icon icon-cancel" onclick="$(\'.PIT\').hide()" style="float:right"/>').append($('<span/>'))));
    if(!!b1 || !!b2){
      PIT.append(
        $('<div class="foot"/>')
        .append(!!b1 ? $('<a href="javascript:void(0)" class="tc-popup-button tc-accent tc-icon" onclick ="ResetFieldsInPIT(this)"/>').text(b1) : '')
        .append(!!b2 ? $('<a href="javascript:void(0)" class="tc-popup-button tc-accent tc-icon" onclick="$(\'.PIT\').hide();"/>').text(b2) : '')
      )
    }
    ofieldVal[RowNr] = [];
    $('#'+oid).closest('tr').find('.inPopup').each(function(i,o){
      var olabel = $('#'+$(o).attr('headers')).text().trim();
      var ofield = $(o).find('.tc-form-control').parent().parent().children();
      if(!ofield.length) ofield = $(o).find('span').css('font-weight',700).parent();
      ofieldVal[RowNr].push($(o).find('.tc-form-control').val());
      PIT.find('.body').append($('<div class="item"/>').append($('<span class="label"/>').text(olabel)).append(ofield));
    });
  }
  $('#PIT_'+oid).toggle();
  if($('.PIT:visible').length){
    Oid.closest('.tc-table-wrapper').addClass('no-overflow');
  }else{
    Oid.closest('.tc-table-wrapper').removeClass('no-overflow');
  }
//  event.preventDefault();
//  $("body").mouseup(function(){$('.PIT').hide()});
};
/* ----- wsc PopupInTable END ----- */



/* ----- wsc Inline Edit START ----- */
function InlineEditCheck(){if($('.wsc-Inline-Edit').length) InlineEdit()}
function InlineEdit(){
  $('.wsc-Inline-Edit').each(function(i,o){
    var es =                                                // define editable section
      $(o).hasClass('tc-table-answer') ? $(o).find('div:first') : // for item(s) in table
      $(o).find('.tc-answer-part > div');                   // for normal question / answer type
    es.addClass('editable-section');                        // markup editable section
    if(!es.find('.inlineEdit-toggle').length) es.append($('<a href="javascript:void(0)" class="tc-icon icon-edit inlineEdit-toggle"><span></span></a>'));
    var ei = es.find('.tc-form-control');                   // get all editable items in editable section
    $(o).off('click').on('click', function(e){              // click on Inline Edit row
      if(!($(e.target).is('.tc-form-control'))){            // prevent toggle when clicking in editable fields
        $(ei).select();                                     // preselect text in editable item
        $(es).find('span.static').text(getVal(es));         // set values in static (text-only) span
        toggleEdit(es);                                     // toggle view on editable section
      }
      e.preventDefault();
    });
    es.find('span.static').remove();                        // remove existing text view
    es.prepend($('<span class="static"/>').text(getVal(es)));  // setup new text view with values from editable fields
  });
//  $("body").on('mouseup', function(){toggleEdit($('.tc-breakpoint-hint'))});
}
function getVal(x){                                         // get values from editable fields as text
  var result = '';                                          // init result text
  $(x).closest('.editable-section').find('.tc-form-control').each(function(i,o){
    if(!$(o).prop('disabled')){                             // do not handle disabled fields
      result += ($(o).prop('tagName') == 'SELECT' ? $(o).find('option:selected').text() : $(o).val().length ? $(o).val() : '---')+' '};
  })
  return result.trim();
}
function toggleEdit(es){                                    // toggle view
  $('.editable-section').not(es).removeClass('active');     // remove active class from all other editable sections on page
  if(es.hasClass('editable-section'))
    es.toggleClass('active');                               // toggle active class on clicked section
}
/* ----- wsc Inline Edit END ----- */

/* --------------------------- *\
   Widget related Functions END
\* --------------------------- */

wsc.timeout = null;
function debounce(func, wait, immediate){
  return function(){
    var context = this, args = arguments;
    clearTimeout(wsc.timeout);
    wsc.timeout = setTimeout(function(){
      wsc.timeout = null;
      if(!immediate) func.apply(context, args);
    }, wait);
    if(immediate && !wsc.timeout) func.apply(context, args);
  };
};

function tableDesc(tid){
  var allhcols = $('#'+tid).find('thead tr th'), showallhcols=[], firstRow = $('#'+tid).find('tbody tr td');
  for(var i=0; i < allhcols.length; i++){showallhcols.push([allhcols.eq(i).text().trim(), allhcols.eq(i).attr('id').split('_p1_')[1], firstRow.eq(i).find('span').text().trim(), allhcols.eq(i).attr('class'), allhcols.eq(i).is(':visible'), firstRow.eq(i).is(':visible')])}
  console.table(showallhcols);
}

function formContent(sel){
  $('#wsc_FormView').remove();
  var ccard  = $('<div id="wsc_FormView" class="tc-card-bg shadow-style-1 tc-card tc-card-top-margin"/>');
  ccard.append($('<div class="tc-card-header handle"/>')
    .append($('<div class="tc-card-title-text"/>').append($('<h2 class="tc-card-title"/>').text((sel?'Non-':'')+'Form Elements Inspector')))
    .append($('<span class="tc-float-right tc-icon icon-cancel"/>').append($('<span/>').on('click', function(){$('#wsc_FormView').remove()})))
  );
  var cbody  = $('<div class="tc-card-body"/>');
  var xtable = $('<table class="tc-table border-collapse-collapse tc-lined-table wsc-debug-table" style="width:100%"/>').append($('<thead/>')
    .append($('<th/>').text('#'))
    .append($('<th/>').text('Tag'))
    .append($('<th/>').text('Type'))
    .append($('<th/>').text('ID'))
    .append($('<th/>').text('Name'))
    .append($('<th/>').text('Label'))
    .append($('<th/>').text('Value'))
    .append($('<th/>').text('Ena'))
    .append($('<th/>').text('Edi'))
    .append($('<th/>').text('Sel'))
  );
  var xbody  = $('<tbody/>');
  $('.tc-form-control').each(function(i,o){
    if(!sel){
      if(!$(o).attr('name') && $(o).find('*[id][name]')){o = $(o).find('*[id][name]')}  // shim for radio
      if($(o).attr('name')){
        var xtag  = $(o).prop("tagName");
        var xtype = $(o).prop("type");
        var xchecked = ''; if('radio checkbox'.indexOf(xtype)<0){xchecked = 'circle-remove-hollow'}else{xchecked = $("#"+$(o).attr('id')).is(':checked')?'accept':'cancel'}
        var xrow = $('<tr/>')
          .append($('<td/>').text(i))
          .append($('<td/>').text(xtag))
          .append($('<td/>').text(xtype))
          .append($('<td/>').text($(o).attr('id')))
          .append($('<td/>').text($(o).attr('name')))
          .append($('<td/>').text($('label[for='+$(o).attr('id')+']').last().text().trim()))
          .append($('<td/>').text($(o).val()))
          .append($('<td/>').addClass('tc-icon icon-'+($(o).is(':disabled')?'cancel':'accept')).append($('<span/>')))
          .append($('<td/>').addClass('tc-icon icon-'+(o.readOnly?'cancel':'accept')).append($('<span/>')))
          .append($('<td/>').addClass('tc-icon icon-'+xchecked).append($('<span/>')))
          .addClass($(o).is(':visible')?'hilite':'lowlite');
        xbody.append(xrow);
      }
    }else{
      if(!$(o).attr('name')){
        var xrow = $('<tr/>')
          .append($('<td/>').text(i))
          .append($('<td/>').text($(o).prop("tagName")))
          .append($('<td/>').text($(o).prop("type")))
          .append($('<td/>').text($(o).attr('id')))
          .append($('<td/>').text($(o).attr('name')))
          .append($('<td/>').text($(o).text().trim()))
          .append($('<td/>').text($(o).val()))
          .append($('<td/>').addClass('tc-icon icon-cancel').append($('<span/>')))
          .append($('<td/>').addClass('tc-icon icon-cancel').append($('<span/>')))
          .addClass($(o).is(':visible')?'hilite':'lowlite');
        xbody.append(xrow);
      }
    }
  })
  var cfooter = $('<div class="tc-card-button-container"/>')
    .append($('<a class="tc-secondary-card-button tc-button tc-uppercase"/>').text('Close').attr('onclick',"$(this).closest('.tc-card').remove()"))
    .append($('<a class="tc-accent-bg tc-button tc-uppercase tc-float-right"/>').text('All').attr('onclick',"$(this).closest('.tc-card').find('tr').show()"))
    .append($('<a class="tc-secondary-card-button tc-button tc-uppercase tc-float-right"/>').text('Visible').attr('onclick',"$(this).closest('.tc-card').find('tr').show();$(this).closest('.tc-card').find('tr.lowlite').hide()"))
    .append($('<a class="tc-secondary-card-button tc-button tc-uppercase tc-float-right"/>').text('Not Visible').attr('onclick',"$(this).closest('.tc-card').find('tr').show();$(this).closest('.tc-card').find('tr.hilite').hide()"))
    .append($('<a class="tc-accent-bg tc-button tc-uppercase tc-float-right"/>').text('Form Elements').attr('onclick',"formContent(0)"))
    .append($('<a class="tc-accent-bg tc-button tc-uppercase tc-float-right"/>').text('Non Form Elements').attr('onclick',"formContent(1)"))
  $('#EDGE_CONNECT_PHASE').append(ccard.append(cbody.append(xtable.append(xbody))).append(cfooter));
  xtable.wrap($('<div class="tc-table-wrapper"/>'))
  $("#wsc_FormView").draggable({handle: '.tc-card-header'});
}


function getHeaders(tid){
  var t = $('#'+tid);
  var hr = t.find('thead tr th');
  hr.each(function(i,o){
    console.log(i , $(o).text().trim() , $(o).is(':visible'));
  })
}
function reorderTableRows(tid,dataitem,order){
  var table = $('#'+tid);
  var rows = table.find('tbody tr').get();
  rows.sort(function(a,b){
    var keyA = $(a).data(dataitem);
    var keyB = $(b).data(dataitem);
    if (keyA < keyB) return (order=='asc')?-1:1;
    if (keyA > keyB) return (order=='asc')?1:-1;
    return 0;
  });
  $(rows).each(function(i,r){
    table.children('tbody').append(r);
  });
}
function reorderLists(lid,dataitem,order){
  var list = $(lid);
  var items = list.find('>li').get();
  items.sort(function(a,b){
    var keyA = $(a).data(dataitem);
    var keyB = $(b).data(dataitem);
    if (keyA < keyB) return (order=='asc')?-1:1;
    if (keyA > keyB) return (order=='asc')?1:-1;
    return 0;
  });
  $(items).each(function(i,r){
    list.append(r);
  });
}
Array.prototype.getUnique = function(){
  var u = {}, a = [];
  for(var i = 0, l = this.length; i < l; ++i){
    if(u.hasOwnProperty(this[i])) continue;
    a.push(this[i]);
    u[this[i]] = 1;
  }
  return a;
}

/* ----- PM Profile addons ----- */
function toggleCard(){
  var o = event ? event.target : arguments[0],
      q = arguments[1],
      card = o.tagName=='SPAN' ? $(o).closest('.tc-card') : $(o),
      cardContent = $(card).find('.tc-card-body'),
      cardToolbar = $(card).find('.tc-card-toolbar.PM-card-toolbar'),
      toggleIcon = $(cardToolbar).find('a.card-content-toggler');
  if(q==1){
    $(cardContent).toggle()
    $(toggleIcon).toggleClass('icon-circle-down icon-circle-up')
  }else{
    $(cardContent).slideToggle()
    if($(toggleIcon).hasClass('icon-circle-down')){ss.clear($(card).attr('id'))}
    else{ss.set($(card).attr('id'),'closed')}
  }
  $(cardToolbar).find('a.tc-card-header-icon:not(.card-content-toggler)').each(function(i,c){$(c).toggle()});
  setTimeout(adjust_PMS_info_height,500)
}

function adjust_PMS_info_height(){
  $('.aside').css('max-height');
  var native_height = $('.aside').height(),
      new_height = $('.main-content-area').height() - 20;
  if(new_height > native_height){
    $('.aside').css('max-height', $('.main-content-area').height() - 20 + 'px')
  }
}
function toggleCardH(){
  var o = event ? event.target : arguments[0],
      q = arguments[1],
      card = o.tagName=='SPAN' ? $(o).closest('.tc-card') : $(o),
      thisCol = card.closest('.responsive-column'),
      nextCol = thisCol.next('.responsive-column');
  if(q==1){
    $(card).find('.card-content-toggler').toggleClass('icon-circle-left icon-circle-right');
    closeH();
  }else if($(o).parent().hasClass('icon-circle-left')){
    closeH();
  }else{
    openH();
  }
  function closeH(){
    card.addClass('hclosed');
    ss.set($(card).attr('id'), 'hclosed')
    $(thisCol).find('.tc-card-title-text').hide();
    $(thisCol).find('.tc-card-body').hide();
    $(thisCol).css('width', '0%');
    $(nextCol).css('width', '98%');
  }
  function openH(){
    card.removeClass('hclosed');
    ss.clear($(card).attr('id'))
    $(thisCol).css('width', '20%');
    $(nextCol).css('width', '80%');
    $(thisCol).find('.tc-card-title-text').show();
    $(thisCol).find('.tc-card-body').show();
  }
}

function PMS_info_toggle(){
  var o = event.target,
      aside = $(o).closest('.aside');
  if(ss.get('PMS_info') != null){
    $('body').removeClass('aside-active')//.removeClass('aside-hover')
    ss.clear('PMS_info')
  }else{
    $('body').addClass('aside-active')
    ss.set('PMS_info','open')
  }
  $('.tc-chart').each(function(i,c){
    var cwidth = $(c).closest(".tc-card-body, .tc-tab-container").width();
    $(c).css("width", cwidth+'px');
    $(c).find('div.highcharts-container').css("width",cwidth+'px');
    $(c).find('svg.highcharts-root').attr("width",cwidth);
  })
  setTimeout(adjust_PMS_info_height,500)
}

/*
$(document).ready(function(){
  $('.wsc-card-target')       //  $('#C5__p1_GRP_74E4568050AE5D29737138')
  .sortable({
    items: '>div',
    handle: '.tc-card-header',
    forcePlaceholderSize: true,
    connectWith: ".wsc-card-target",
  })
  .on('sortupdate', function(e, o){
    $(e.target).each(function(i,c){
      console.log('* ',$(c).attr('id'));
      $(c).find('div.tc-card > .tc-card-header').each(function(j,d){console.log('-- ',$(d).parent().attr('id'))});
      console.log('-');
    })
  })
});
*/

function hideFlyoutDelay(argObj){
  setTimeout(function(){hideFlyout(argObj)},5);
}