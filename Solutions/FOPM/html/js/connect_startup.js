/*
 * $RCSfile$
 * $Author$
 * $Revision$
 * $Date$
 *
 * Copyright (c) 2001-2006 edge IPK Limited, All rights reserved.
 *
 * This source code is protected by copyright laws and international copyright treaties,
 * as well as other intellectual property laws and treaties.
 *  
 * Access to, alteration, duplication or redistribution of this source code in any form 
 * is not permitted without the prior written authorisation of edge IPK.
 * 
 */
 
/* Method that iterates over the Modernizr library and builds it up into a format
   that the server then stores for use server side
   */
function fetchBrowserFeatures( p_formName )
{
	var form = document.forms[p_formName];
	if (form != null && form.BROWSER_FEATURES != null) {
		var m = Modernizr;
		if ( m != null ) {
			var c = "";
			for ( var f in m ) { 
				if ( f[0] == '_' ) {
					continue;
				}
				var t = typeof m[f];
				if ( t == 'function' ) {
					continue;
				}
				c += (c ? '|' : '') + f + ':';
				if ( t == 'object' ) {
					for (var s in m[f]) {
						c += '/' + s + ':' + (m[f][s]?'1':'0');
					}
				} else {
					c += m[f] ? '1' : '0';
				}
			}c += ';';
			document.forms[p_formName].BROWSER_FEATURES.value = c;
		}
	}
}
