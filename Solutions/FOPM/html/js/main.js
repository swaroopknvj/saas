$(document).ready(function(){
  if(typeof localStorage != 'undefined') {
    var elementId = localStorage.id;
    if (elementId != null) {
      $('#'+elementId+' .collapsibleLink').find('i.icon.BlueMagic').toggleClass('up').toggleClass('down');
      $('#'+elementId+' .collapsibleContent' ).each(function() {
        if ($( this ).hasClass('in')) {
          $( this ).collapse('toggle');
          $( this ).prev().find('i.icon.BlueMagic').removeClass('down').removeClass('up').addClass('down');
        }
      });
    }
  }

  $('.collapsibleLink').on('click', function(){
    $( this ).find('i.icon.BlueMagic').toggleClass('up').toggleClass('down');
    if(typeof localStorage != 'undefined') {
      if ($( this ).find('i.icon.BlueMagic').hasClass('down')) {
        localStorage.setItem("id", $(this).parent().parent().attr('id'));
      } else {
        localStorage.removeItem("id");
      }
    }
  });
});
