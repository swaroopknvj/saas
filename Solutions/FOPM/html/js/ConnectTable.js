/* ----- Edge Connect TCIB - TAPT24 Project addons ----- */
if(!Array.prototype.indexOf){           // add indexOf to Array methods
  Array.prototype.indexOf = function(obj, start){
    for(var i=(start||0), j=this.length; i<j; i++){if(this[i]===obj){return i;}}
    return -1;
  }
}
if(!String.prototype.startsWith){       // add startsWith to String methods
  String.prototype.startsWith = function(search){
    return this.indexOf(search) == 0;
  };
}
if(!String.prototype.endsWith){         // add endsWith to String methods
  String.prototype.endsWith = function(search){
    return this.lastIndexOf(search) == this.length - search.length;
  };
}
if(!String.prototype.trim){             // add trim to String methods
  String.prototype.trim = function(){
    return this.replace(/^\s+|\s+$/g, '');
  };
}

var has = has || {};
has.MutationObserver = has.MutationObserver || (!!(window.MutationObserver || window.WebKitMutationObserver));

$(document).ready(function(){
  var tabbedPane = $('.PaddedSection'); //CONNECT_ACTIVE_TAB
  var tabbedTabs = tabbedPane.find('div[id^="TAB_"]');
  var tabCont = tabbedPane.find('.InnerTabPane');
  if(!tabCont.is(':visible')){
    tabCont.eq(0)
      .css('display','block')
    tabbedTabs.eq(0)
      .toggleClass('InnerTabSelected')
      .toggleClass('InnerTabUnselected');
  }
  $('.InnerTabUnselected').on('tap', function(){$(window).resize();}) // responsive resize hidden hicharts
  $.each($('.TAPTable td[title]'), function(i,tct){
    $(tct).attr('title', trim($(tct).attr('title')))
  })
  $.each($('.TAPTable.sortN th'), function(i,th){
    $(th).removeAttr('onclick')
  })
  $.each($('.TAPTable.sortY th.sortN'), function(i,th){
    $(th).removeAttr('onclick')
  })
  $.each($('table[data-alpha]'), function(i,t){
    alphatize(t);
  })
  $.each($('table[data-sum]'), function(i,t){
    addSumRow(t);                                 // ATTN: no client side sum-up for paginated tables!!!
  })
  // Questionnaire wizard action buttons
  questionnaire();
  cssXsTdLabel();
  AJAX_reqOnClick();
  tfootersum();
});

// Global variables (should be retrieved automatically !!!)
//  var ts = ',',                                   // Thousands separator
//      dateFormat = 'dd/mm/yyyy';                  // Date Format used throughout application

function tfootersum(){
  $('table.withClickableFooter:not(.hidden) tbody').find('tr').last().addClass('totalRow');
}

// Sort columns client side - TAPTable version
function TAPTableSort(o){                                                       // Table sorting client side
  var table = $(o).closest('table[id]'),                                        // table object
      col   = $(o).index(),                                                     // column index to sort on
      column = $(table).attr('data-alpha'),
      trowg = table.find('tbody tr.groupedRow'),                                // table body grouped rows
      multi = trowg.length ?
        $(table).find('tbody tr.subrow td').eq(col).find('br').length ? 1 : 0 : // multiple items in grouped table cells?
        $(o).find('br').length ? 1 : 0,                                         // multiple items in non-grouped table cells?
      item  = multi && $(o).hasClass('desc') && !$(o).hasClass('s1') ? 1:       // sort on second item
              multi && $(o).hasClass('asc')  && $(o).hasClass('s1') ? 1 :       // sort on second item
              multi && $(o).hasClass('desc') && $(o).hasClass('s1') ? 0 :       // sort on first item
              0,                                                                // sort on first item
      dir   = $(o).hasClass('asc')?'desc':'asc',                                // sort direction
      trows = table.find('tbody tr').length;                                    // table body rows
  if(!$(trowg).eq(0).attr('data-group')){                                       // brand parent and child rows
    $(trowg).each(function(i,r){                                                // loop all group rows
      $(r).attr('data-group', i);                                               // mark groupedrows with data-group attribute and number
      $(r).nextUntil(':not(.subrow)').attr('data-parentgroup', i);              // mark subrows with data-parentgroup attribute and parent reference
    })
  }
  $(table).find('thead tr th').removeClass('ASC DESC');                         // remove sorting column icon for previously sorted multiple pages (AJAX)
  $(o).find('i').remove();                                                      // remove sorting icon
  $(o).removeClass('desc asc s0 s1').addClass(dir+' s'+item);                   // set class on th
  if(multi && !item){$(o).find('br').before($('<i/>').addClass('icon'))}        // set sorting icon on last item
  else{$(o).append($('<i/>').addClass('icon'))}                                 // set sorting icon on first item
  $(o).siblings().removeClass('desc asc s0 s1').find('i').remove();             // remove sorting icon on sibling th's
  if(trows > 1) $SORTrows(table,col,item,dir);                                  // call sorting action
  alphaviz(table);
}
function $SORTrows(table,col,item,dir){
  var tb = $(table).find('tbody'),                                              // get table body
      tgr = $(tb).find('tr.groupedRow'),                                        // do we have grouped rows?
      tr = [],                                                                  // init table rows array
      reverse = dir=='asc' ? 1 : -1;                                            // ascending or descending
  if(tgr.length){                                                               // if grouped table
    var ocol = col;                                                             // save original column index
    col = col - $(table).find('thead th').eq(col).prevAll('.hidden').length;    // recalculate column index for grouped rows
    tr = Array.prototype.slice.call($(tb).find('tr.groupedRow'), 0); $SORT(tr); // sort grouped rows

    col = ocol;                                                                 // set back column index for subrows
    reverse = reverse == 1 ? -1 : 1;                                            // invert sorting direction for subrows
    tr = Array.prototype.slice.call($(tb).find('tr.subrow'), 0); $SORT(tr);     // sort subrows

    $(tb).find('tr.subrow').each(function(s, sr){                                         // loop subrows
      $(tb).find('tr.groupedRow[data-group='+$(sr).data('parentgroup')+']').after($(sr))  // assign subrows to parent rows in reverse order (hence inverted sorting on subrows)
    });
  }else{                                                                        // if table not grouped
    tr = Array.prototype.slice.call($(tb).find('tr'), 0);                       // make array of table body rows
    $SORT(tr);                                                                  // sort non-grouped table rows
  }
  function $SORT(tr){
    tr = tr.sort(function(a, b){                                                // get rows sort index
      var ditem = item == 0 ? $(a.cells[col]) :                                 // data item object to sort on : first item (th object)
                              $(a.cells[col]).find('br').next();                // second item (first i object after br)
      var ctype = $(ditem).hasClass('Decimal') ? 1 :                            // data type is Number
                  $(ditem).hasClass('Date') ? 2 :                               // data type is Date
                  0;                                                            // fallback to data type String for all other
      var AVal = $(ditem).text();                                               // text content of first item (or whole cell if multi)
      var aval =
        (ctype==0) ? AVal.trim() :                                              // first value is Text
        (ctype==1) ? parseFloat(RawNumber(AVal)) || 0 :                         // first value is Decimal
        (ctype==2) ? get_Date(AVal.trim()) :                                    // first value is Date
                     AVal.trim();                                               // else handle as text
      var ditem = item == 0 ? $(b.cells[col]) :                                 // data item object to sort on : first item (th object)
                              $(b.cells[col]).find('br').next();                // second item (first i object after br)
      var BVal = $(ditem).text();                                               // text content of first item (or whole cell if multi)
      var bval =
        (ctype==0) ? BVal.trim() :                                              // second value is Text
        (ctype==1) ? parseFloat(RawNumber(BVal)) || 0 :                         // second value is Decimal
        (ctype==2) ? get_Date(BVal.trim()) :                                    // second value is Date
                     BVal.trim();                                               // else handle as text
      if(ctype==0){                                                             // sort as Text
        return reverse * (aval.localeCompare(bval));
      }else if(ctype==1){                                                       // sort as Number
        return reverse * (aval-bval);
      }else if(ctype==2){                                                       // sort as Date
        return reverse * (aval-bval);
      }
    });
    for(var i = 0; i < tr.length; ++i){                                         // move/append each existing row in sorting order to table body
      tb.append(tr[i]);
    }
    if(tb.find('tr.totalRow')){                                                 // move total row to end of table body
      tb.append(tb.find('tr.totalRow'));
    }
  }
}
var get_Date = function(d){
  var stamp = 0;
  var dat = d.trim().split(' ')[0].split('/');
  stamp = new Date(dat[2], dat[1]-1, dat[0]).valueOf();
  if(d.indexOf(':')>-1){
    var time = d.trim().split(' ')[1].split(':');
    stamp = new Date(dat[2], dat[1]-1, dat[0], time[0], time[1], time[2]).valueOf();
  }
  return stamp;
}

/* ----- Start table filtering ----- */
function init_tableFilterItems(tid, typ, col, ulc){         // tableID, type ('cat'|'mon') , column class , list container class
  var t = $('#'+tid)
  if(!t.length) return false;
  if(typ == 'mon'){
    var monItems = $(t).find('thead .'+col);
    $.each(monItems, function(i, mi){
      var count = 0;
      $.each($(t).find('tbody tr:not(.alphaRow)'), function(j,r){
        count += $(this).find('td.'+col).eq(i).text().trim()=='Yes' ? 1 : 0;
      })
      $('ul.'+ulc).append($('<li />')
        .addClass('tableFilterItem')
        .attr('title', $(this).attr('title'))
        .attr('data-col', col)
        .attr('data-val', 'Yes')
        .attr('data-count', count)
        .attr('data-moni', i)
        .append($('<i />').text($(this).text().trim()))
        .on('tap', function(e){
          $(this).toggleClass('active');
          filterTableFinal(tid);
        })
      )
    })
  }else if(typ == 'cat'){
    var catList = [];
    $.each($(t).find('tbody tr td.'+col), function(j,c){
      catList.push($(c).text().trim())
    })
    catList = unique(catList);          // remove duplicates in categories array
    for(i=0; i<catList.length; i++){    // iterate over categories list
      var count = 0;
      $.each($(t).find('tbody tr:not(.alphaRow)'), function(j,r){
        count += $(r).find('td.'+col).text().trim() == catList[i] ? 1 : 0;
      })
      $('ul.'+ulc).append($('<li />')
        .addClass('tableFilterItem monCat')
        .attr('data-col', col)
        .attr('data-val', catList[i])
        .attr('data-count', count)
        .attr('data-monc', i)
        .append($('<i />').text(catList[i]))
        .on('tap', function(e){
          if(!$(this).hasClass('active')){
            $('li.tableFilterItem.monCat.active').removeClass('active');
            $(this).addClass('active')
          }else{
            $('li.tableFilterItem.monCat.active').removeClass('active');
          }
          filterTableFinal(tid);
          e.preventDefault();
        })
      )
    }
  }else{
    console.warn('ATTN: unknown type used for init_tableFilterItems()');
  }
}
function filterTableFinal(tid){
  var tableFilterItems = $('.tableFilterItem.active');
  var t = $('#'+tid);
  if(!$(t).length) t = $('table[class*="'+tid+'"]');
  if(!$(t).length) return false;
  var rows = $(t).find('tbody tr:not(.alphaRow)');          // table rows
  rows.each(function(j,r){
    var allow = 0;
    var col, val, mon, crit;
    tableFilterItems.each(function(i,o){
      col = $(o).data('col');
      val = $(o).data('val');
      mon = $(o).data('moni')||0;
      crit = $(r).find('.'+col).eq(mon).text().trim();
      if($(o).hasClass('TextFilterField')){                 // search/filter text field (from fiterTable-textfield widget)
        if(!o.value){allow += 1}                            // if no value all rows are allowed to display
        else{
          $(o).addClass('actif');                           // add cross icon to the right for deleting the value
          val = o.value.toLowerCase();                      // input field's value redefined
          crit = $(r).find('.'+col).text().trim().toLowerCase();  // get all matching cell text values concatenated
          if(crit.indexOf(val)>-1) allow += 1;
        }
      }else if(o.type=='select-one'){                       // combobox (from fiterTable-select widget)
        val = $(o).val() || '';
        if(crit == val) allow += 1;
      }else if($(o).attr('min')){                           // range slider (from fiterTable-slider-range widget)
        crit = parseFloat(RawNumber(crit) || 0);
        if(crit >= $(o).attr('min') && crit <= $(o).attr('max')) allow += 1;
      }else{                                                // all other items
        if(crit == val) allow += 1;
      }
    });
    if(!tableFilterItems.length) $(r).show();
    else if(allow == tableFilterItems.length) $(r).show();
    else $(r).hide();
  });
  var totRows = $(t).find('tbody tr:not(.alphaRow)').length;
  var visRows = $(t).find('tbody tr:not(.alphaRow):visible').length;
  $('.search_count').text(visRows + ' of ');
  if(totRows == visRows) $('.search_count').text('');
  alphaviz(t);
  if($('.tmValue').length){                                 // update Total Market Value
    var mktval = 0;
    $(t).find('tbody tr:not(.alphaRow):visible').each(function(i,r){
      mktval += Number(RawNumber($(r).find('.mktval').text().trim()));
    });
    $('.tmValue').text(FormatNumber(mktval.toFixed(2)));
  }
}
/* ----- End table filtering ----- */

/* ----- Start HighCharts-Filtering ----- */
function hcfiltering(){}
function ChartFilter(o, CompPrefix, TableFilterContainer, col){
  Component_ID_Prefix = CompPrefix;
  if($("#AssetFilters").find("li[data-col='"+col+"'][data-val='"+o.name+"']").length){  // if item in list
    $("#AssetFilters").find("li[data-col='"+col+"'][data-val='"+o.name+"']").remove();  // remove it
  }else{
    $('#AssetFilters ul').append(                         // else add it
      $('<li />')
      .attr('data-mon',o.series.chart.title.textStr)
      .attr('data-val',o.name)
      .attr('data-col',col)
      .text(o.name)
//      .css('background-color', o.color)
      .append($('<i/>')
        .addClass('icon BlueMagic cancel')
        .on('tap', function(e){
          $(this).parent().remove();
          af1(TableFilterContainer);    // use client side filtering
          e.preventDefault();
//          UpdateFilterFields();         // use server side filtering (odata call, e.g. for paginated tables)
        })
      )
    );
  }
  af1(TableFilterContainer);            // use client side filtering
//  UpdateFilterFields();                 // use server side filtering (odata call, e.g. for paginated tables)
}
function af1(tfc){
  $('#'+tfc+' table').each(function(i,table){
    var tid = $(table).attr('id');
    af2(tid);
  })
}
function af2(tid){
  var table = $('#'+tid);
  if(!table.length) return false;
  var Component_ID_Prefix = tid.split('TBL')[0];
  var FilterTableCont = $(table).closest('div[id^="'+Component_ID_Prefix+'p1_GRP"]');
  $(FilterTableCont).show();
  var FilterResults = $(FilterTableCont).find('.FilterResults');
  if(!FilterResults.length) FilterResults = $('#FilterResults');
  var iscollapsed = false;
  if(!table.is(':visible')){
    iscollapsed = true;
    toggleCollapse(table, Component_ID_Prefix);
  }
  var rows = $(table).find('tbody tr:not(.alphaRow):not(.totalRow)');
  var crit = $('#AssetFilters li');
  var critcols = [];
  $(crit).each(function(){
    critcols.push($(this).attr('data-col'));
  });
  critcols = unique(critcols);
  var crits = critcols.length;
  if(crits){
    $(rows).each(function(ri,r){
      var allow = 0;
      crit.each(function(ci,f){
        var col = $(f).data('col');
        allow += $(r).find('span.'+col).text().trim() == $(f).data('val') ? 1 : 0;
      });
//      if(allow > 0){                  // OR
      if(allow == crits){               // AND
        if ($(table).find('tr.groupedRow').length) {
          $(r).prev('.groupedRow').show();
          $(r).prev('.groupedRow').find('td > i.icon.BlueMagic').first().removeClass('down').addClass('right');
          $(r).hide();
        } else {
          $(r).show();
        }
      }else{
        $(r).hide();
      }
    })
    $(FilterResults).show();
    $('#FilterInfo').hide();
  }else{
    $(rows).show();
    $(table).find('tr.subrow').hide();
    $(table).find('tr.groupedRow td > i.icon.BlueMagic').removeClass('down').addClass('right');
    $('#FilterInfo').show();
    $(FilterResults).hide();
  }
  if ($(table).find('tr.groupedRow').length) {
    var trresults = $(table).find('tbody tr.groupedRow:visible').nextUntil(':not(.subrow)').length;
  } else {
    var trresults = $(table).find('tbody tr:not(.alphaRow):visible').length;
  }
  $(FilterResults).find('i').text(trresults);
  $(FilterResults).find('b').text($(rows).length);
  if(iscollapsed) toggleCollapse(table, Component_ID_Prefix);
  if(trresults < 1 && !$(table).find('tbody .totalRow').length) $(FilterTableCont).hide();
  addSumRow(table);
}
function toggleCollapse(table, Component_ID_Prefix){
  var collapsibleParent = $(table).closest('div[id^="'+Component_ID_Prefix+'p1_GRP"]');
  var collapsibleLink = $(collapsibleParent).find('.collapsibleLink');
  $(collapsibleParent).find('.collapsibleContent').toggleClass('collapse in');
  $(collapsibleLink).find('i.icon.BlueMagic').toggleClass('down').toggleClass('right');
}
/* ----- End HighCharts-Filtering ----- */

function UpdateFilterFields(){                    // use server side filtering (odata call, e.g. for paginated tables)
  $('div[id$=OdFilterSection] input').val('').off('change');
  $('#AssetFilters').find('li').each(function(i,c){
    var typ = $(c).data('col').split('-').pop();
    var dest = $('input[id$=QUE_OdFilter_'+typ+']');
    dest.val((dest.val().length ? dest.val() + '|' : '') + $(c).data('val'));
  })
  $('div[id$=OdFilterSection] input').on('change', ApplyFilterFields());
  if($('#AssetFilters ul li').length){
//    $(FilterResults).show();
    $('#FilterInfo').hide();
  }else{
//    $(FilterResults).hide();
    $('#FilterInfo').show();
  }
}
function ApplyFilterFields(){
  resetMultiOrder();
  $('button[id$=BUT_OdFilter_filter]').trigger('click');
}

var sficols = false
function filterTable(o, tid, cols){               // Filter table column on cell content: show only matched rows
 var visRows = '',
 rowscnt = '',
 t = $('#'+tid);                                  // table id
  if(!$(t).length) return false;
  sficols = cols;
  if($('#hp_search1_'+tid+' b').length){
    callft2(tid, cols)
  }else{
    if(o.value.length){$(o).addClass('active')}
    else{$(o).removeClass('active')}
    var ival = o.value.toLowerCase();             // input field value
    var rows = $(t).find('tbody tr:not(.alphaRow)');  // table rows
    $(rows).each(function(i,c){                   // loop through all table data rows
      var ccont = '';
      for(var j=0; j<cols.length; j++){
        var cell = $(c).find('td').eq(cols[j]);   // get cell in row
        ccont += $(cell).text().toLowerCase()+' ';// get cell text content
      }
      var row = $(cell).closest('tr');            // get row object
      if(ccont.indexOf(ival)>-1)                  // if cell text contains input fields value
        $(row).show();                            // display row
      else                                        // else if no match
        $(row).hide();                            // hide row
    })
  }
  visRows = $(t).find('tbody tr:not(.alphaRow):visible').length;
  rowscnt = $(t).find('tbody tr:not(.alphaRow)').length;
  $('#hp_search_count_'+tid+' b').text(visRows);
  alphaviz(t);
  filterInboxTable(); // Filter inbox table on conversation view is selected
}

function filterInboxTable(){
	var conversationView = $( '#'+Component_ID_Prefix+'conversationView_0' ).is(":checked"), // Conversation View
	    inboxRows = $('#'+Component_ID_Prefix+'TBL_INBOX tbody tr'); // Inbox table rows
    if(conversationView){ // Check conversation view is selected
		$(inboxRows).each(function() { // loop through all table data rows
		    var eachRow = $(this); // current row
			if($(eachRow).hasClass('alphaClass')){  // if parent row
				$(eachRow).find("i").removeClass('down').addClass('right'); // change the chevron direction
			}
			else {
				$(eachRow).hide(); // hide row
			}
		});
    }
}
function alphatize(t){                            // create alphabetic splitter rows
  var column = $(t).attr('data-alpha'),
      oldchar = '';
  $(t).find('.alphaRow').remove();
  $.each($(t).find('tbody tr:visible'), function(){
    var char = $(this).find('td').eq(column).text().trim().charAt(0);
    if(char != oldchar){
      $(this)
      .before($('<tr />')
        .addClass('alphaRow')
        .append($('<td />')
          .attr('colspan', $(this).find('td').length)
          .text(char)
        )
      )
    }
    oldchar = char;
  })
}
function alphaviz(t){                             // show or hide splitter rows
  var alpha = $(t).attr('data-alpha');
  if(alpha > -1){
    var sorted = $(t).find('thead th i.icon').parent().index();
    if((sorted > -1) && (alpha != sorted)){
      $(t).find('.alphaRow').remove();
    }else{
      alphatize(t);
    }
  }
}


function addSumRow(t) {                           // TODO: ATTN with pagination!!!
  if(!$(t).attr('data-sum')) return false;
  if($('body').attr('data-section')=='heldassets') return false;
  $(t).find('.totalRow').remove();

  var columnsToSum = $(t).attr('data-sum').split(',').map(Number);
  var columnsNb = $(t).find('thead > tr').children().length;
  var isFiltered = !!$('#AssetFilters ul li').length;
  var hasHierarchy = !!$(t).find('tr.groupedRow').length;
  if(hasHierarchy){
    var newColumnsToSum = [];
    for(var i=0; i<columnsToSum.length; i++){
      newColumnsToSum.push(columnsToSum[i] - 2)
    }
    columnsToSum = newColumnsToSum;
  }
  $(t).find('tbody > tr:first').before('<tr class="totalRow"></tr>');
  for (var i = 0; i < columnsNb; i++) {
    var tdContent = '';
    if ($.inArray(i,columnsToSum) > -1) {
      // Calculate total
      var sum = 0;
      var postfixValue = '';
      var selector = hasHierarchy ?                                   // select rows depending on hierarchy
        $(t).find('tbody > tr.groupedRow:visible') :                  // if hierarchy enabled
        $(t).find('tbody > tr:visible');                              // if flat view
      selector.each(function(index) {
        var cellValue     = $(this).find('td').eq(i).text().trim();
        var cellvalsplit  = cellValue.split(' ');
        postfixValue      = cellvalsplit.pop().trim();
        var cellval       = cellvalsplit.join('');
        sum += Number(RawNumber(cellval));
      });
      if (postfixValue == '%') {
        tdContent = sum.toFixed(0) + ' %';
      } else {
        tdContent = FormatNumber(sum.toFixed(0)) + ' ' + postfixValue;
      }
    }
    var isClass = $(t).find('tbody > tr').eq(1).find('td').eq(i).attr('class');
    $(t).find('tbody > tr:first').append('<td class="'+isClass+'"><i class="'+isClass+'"><b>'+tdContent+'</b></i></td>');
/*
    var isCell = $(t).find('tbody > tr').eq(1).find('td').eq(i),
        isClass = $(isCell).attr('class');
    if($(isCell).is(':visible'))
      $(t).find('tbody > tr:first').append('<td class="'+isClass+'"><i class="'+isClass+'"><b>'+tdContent+'</b></i></td>');
*/
  }
}

function cssXsTdLabel(){                          // prepare table for mobile display
/*
  return false;
  $('.tableContainer').each(function(){
    var tid = $(this).find('table').attr('id');   // get table id
    if($('#'+tid+'_mobile').length) return;
    $('#'+tid+' tr[id]').on('tap', function(e){$(this).toggleClass('expanded');e.preventDefault();});
    var treetable = $('#'+tid).hasClass('treetable');       // boolean
    var thc = $(this).find('thead th');                     // get table header cells
    var css='<style id="'+tid+'_mobile">@media only screen and (max-width: 567px) {\n';   // init CSS string

    css += "#"+tid+" td {display: none;}";
    css += "#"+tid+" td:first-child {display: block; text-align: left !important}";       // TODO: first visible cell or mark main cell with special class
    css += "#"+tid+"  tr.expanded td {display: block; clear: both;}";
    css += "#"+tid+"  tr.expanded td:first-child {text-align: right !important}";

    $.each(thc, function(i,h){                    // loop through table header cells
      var thcont = treetable ?                    // get table header cell content
          $(h).text() :                           // if treetable get only text
          $(h).html().replace('<br>','\\A ').replace(/&nbsp;/g,' ').trim();  // TAPTable
      var bool = $('#'+tid).find('tbody tr').eq(0).find('td').eq(i).hasClass('boolean');  // check if boolean cell (icon)
      if(thcont.length && !bool)
        css += "#"+tid+" tr.expanded td:nth-of-type("+(i+1)+"):before {content: '"+thcont+"'; color: #333; text-align: left;}\n"; // add CSS declaration for table column
    })
    css += '}</style>';                           // end CSS string
    $('head').append(css);                        // append CSS to HTML head
  })
  // $('.tableContainer').find('td').each(function(){
  //   if(trim($(this).text())=='' && !$(this).hasClass('boolean'))
  //     $(this).addClass('hidden-xs');
  // });
  cleanupCells();
*/
}
function cleanupCells(){
  $('.tableContainer td i').each(function(i,c){
    if($(c).text().trim() == '')
      $(c).text('\20').removeClass('neg');
  });
}
function AJAX_reqOnClick(){
  if(!has.touch) return;
  if(has.MutationObserver){
    var observer = new MutationObserver(function(mutations, observer){
      cssXsTdLabel();
    });
    observer.observe(document.getElementById('form1'),{
      subtree: true,
      attributes: false,
      childList: true,
      characterData: false,
      attributeOldValue: false,
      characterDataOldValue: false
    });
  }else{
    $('#form1').bind('DOMSubtreeModified', function(e) {
      if(e.target && e.target.innerHTML.length > 0) cssXsTdLabel();
      else if(e.srcElement.innerHTML.length > 0) cssXsTdLabel();
    });
  }
}

function questionnaire(){
  $('#C1__BUT_QUESTIONNAIRE_STEP1_NEXT').on('tap', function(event){
    $("#C1__QUESTIONNAIRE_STEP1_POPUP").dialog("close");
    $("#C1__QUESTIONNAIRE_STEP2_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#C1__BUT_QUESTIONNAIRE_STEP2_BACK').on('tap', function(event){
    $("#C1__QUESTIONNAIRE_STEP2_POPUP").dialog("close");
    $("#C1__QUESTIONNAIRE_STEP1_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#C1__BUT_QUESTIONNAIRE_STEP2_NEXT').on('tap', function(event){
    $("#C1__QUESTIONNAIRE_STEP2_POPUP").dialog("close");
    $("#C1__QUESTIONNAIRE_STEP3_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#C1__BUT_QUESTIONNAIRE_STEP3_BACK').on('tap', function(event){
    $("#C1__QUESTIONNAIRE_STEP3_POPUP").dialog("close");
    $("#C1__QUESTIONNAIRE_STEP2_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#C1__BUT_QUESTIONNAIRE_STEP3_NEXT').on('tap', function(event){
    ajaxButtonAction('', 'C1____C3F5DD4AEB6C97B8 FormButton 130', 'C1__BUT_C3F5DD4AEB6C97B860312', false, null, '', 'servletcontroller', '', false, true, '');
    $("#C1__QUESTIONNAIRE_STEP3_POPUP").dialog("close");
    $("#C1__QUESTIONNAIRE_STEP4_POPUP").dialog("open");
    event.preventDefault();
  });
  $('#C1__BUT_QUESTIONNAIRE_STEP4_BACK').on('tap', function(event){
    $("#C1__QUESTIONNAIRE_STEP4_POPUP").dialog("close");
    $("#C1__QUESTIONNAIRE_STEP3_POPUP").dialog("open");
    event.preventDefault();
  });
}

/* ----- Helper functions ----- */
function unique(array){
  return array.filter(function(el,index,arr){
    return index == arr.indexOf(el);
  });
}
