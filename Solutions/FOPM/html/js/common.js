var ls = {                                        // ----- localStorage (k=key, v=value) -----
  get: function(k){return localStorage.getItem(k);},
  set: function(k,v){localStorage.setItem(k,v);},
  clear: function(k){localStorage.removeItem(k);},
  clearall: function(){localStorage.clear();},
  list: function(){return window.localStorage;}
}
var ss = {                                        // ----- sessionStorage (k=key, v=value) -----
  get: function(k){return sessionStorage.getItem(k);},
  set: function(k,v){sessionStorage.setItem(k,v);},
  clear: function(k){sessionStorage.removeItem(k);},
  clearall: function(){sessionStorage.clear();},
  list: function(){return window.sessionStorage;}
}
var ua = navigator.userAgent;
var perf = window.performance ||                  // performance API initialization
           window.msPerformance ||
           window.mozPerformance ||
           window.webkitPerformance;
var has = {};
has.touch = 'ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;       // device with touchscreen?
has.ss = !!window.sessionStorage;                 // browser knows sessionStorage?
has.ls = !!window.localStorage;                   // browser knows localStorage?
has.perf = !!perf;                                // browser supports performance API?

var Component_ID_Prefix = "";
var IPQ = {};       // init questionnaire

var bua = bua || {};
function browserDetect(){     // spoofing
  var ua       = navigator.userAgent;
  bua.Trident  = ua.indexOf('Trident/')>-1;
  bua.MSIE     = ua.indexOf('MSIE')>-1;
  bua.Edge     = ua.indexOf('Edge/')>-1;
  bua.Opera    = ua.indexOf('OPR/')>-1;
  bua.Webkit   = !bua.opera && !bua.Edge && ua.indexOf('AppleWebKit/')>-1;
  bua.Chrome   = !bua.opera && !bua.Edge && bua.Webkit && ua.indexOf('Chrome/')>-1;
  bua.Safari   = !bua.opera && bua.Webkit && !bua.Chrome;
  bua.Firefox  = ua.indexOf('Firefox/')>-1;
  bua.Android  = ua.indexOf('Android')>-1;
  bua.Win      = ua.indexOf('Windows')>-1;
  bua.Mac      = ua.indexOf('Mac')>-1;
  bua.androidv = parseInt(ua.substr(ua.indexOf('Android')+8,5));
  bua.MSIEv    = parseInt(ua.substr(ua.indexOf('MSIE')+5,2));
}
function addBrowser(){
  browserDetect();
  if(bua.Webkit && !bua.Chrome)   $('body').addClass('WebKit');
  if(bua.Trident)  $('body').addClass('Trident');
  if(bua.MSIE)     $('body').addClass('MSIE'+bua.MSIEv);
  if(bua.Edge)     $('body').addClass('Edge');
  if(bua.Opera)    $('body').addClass('Opera');
  if(bua.Android)  $('body').addClass('Android');
  if(bua.Android && !bua.Chrome && !bua.Firefox) $('body').addClass('A'+bua.androidv);
  if(bua.Chrome)   $('body').addClass('Chrome');
  if(bua.Firefox)  $('body').addClass('Firefox');
}
function filterBrowser(){
//  return $.map(bua, function(v,k){if(v) return k}).join(' ').replace(/\b\w/g,l=>l.toUpperCase());
  return $.map(bua, function(v,k){if(v) return k}).join(' ');
}

/* ----- Handle server messages on order entry page (and others) ----- */
var errorLblT = {
  'ERROR' :   ['ERROR','ERREUR'],
  'WARNING' : ['WARNING','AVERTISSEMENT'],
  'INFO' :    ['INFO','INFO'],
  'USER' :    ['Welcome,','Bienvenue,']
}
function errorLbl(){          // use growl to display messages
  if($('.errorLabel').is(':visible')){
    $('#growls .growl').remove();                           // fix TCIB-3975
    $.each($('.errorLabel div[id]'), function(i,o){         // fix TCIB-4104
      if(!$(o).text().length) return;
      var msgs = $(o).text().split('<br>');
      for(var m = 0; m < msgs.length; m++){
        var msg = msgs[m];
        if(msg.split('|').length == 1) msg = 'INFO|' + msg;
        var omsg = msg.split('|'),
            title = omsg[0].trim(),
            message = omsg[1].trim(),
            setting = {
              title: errorLblT[title][LANGUAGE=='French'?1:0],
              message: message,
              icon:  title.toLowerCase(),
              fixed: (title=='ERROR' || title=='WARNING'),
              duration: 3500,
              cb: function(){}
            }
        if(title=='ERROR') $.growl.error(setting);
        else if(title=='WARNING') $.growl.warning(setting);
        else $.growl.notice(setting);
      }
    })
    $('.errorLabel').hide();
  }
  if($('div.errorLabel div').text().trim().length == 0){    // fix WSC-1638
    $('#growls .growl').remove();                           // remove growls if no more message from TAP
  }
}
/* ----- End Handle server messages ----- */

function checkBoxIcons(o){
  var selector = false;
      selector = o ? $(o).closest('.CheckBoxIcon') : $('.formItem.CheckBoxIcon');
  selector.each(function(i,o){
    if($(o).closest('.tableContainer').length){
      $(o).closest('td').addClass('tdwcbicon');
    }
    var checkbox = $(o).find('input[type="checkbox"]');
    if($(checkbox).is(':checked')) $(o).addClass('checked');
    else $(o).removeClass('checked');
    $(checkbox).on('change', function(){
      if($(this).is(':checked')) $(this).parent().addClass('checked');
      else $(this).parent().removeClass('checked');
    })
  });
}

function checkLock() {
  if (ScreenLocked == '1') {
    $('#p1_breadCrumbHome i')
        .removeClass('home')
        .addClass('lock')
        .removeAttr('onclick')
        .closest('a')
        .removeAttr('onclick')
        .removeAttr('href');
    $("#BUT_UNLOCK").on('tap', function(event){
      var isOpen = $("#UNLOCK_POPUP").dialog( "isOpen" );
      if (isOpen) {
        $("#UNLOCK_POPUP").dialog("close");
      } else {
        $("#UNLOCK_POPUP").dialog("open");
      }
      event.preventDefault ? event.preventDefault() : event.returnValue = false;
    });
  }
}

function familyTreeProcessing() {
  if($('div.tree').length){
    var width = $('div.tree > ul > li').width();
    $('div.tree').width(width+100);

    // var r = 18; var g = 31; var b = 43;
    // for(var i=0;i<10;i++) {
    //   r+=20; g+=20; b+=20;
    //   $('.tree li a.level'+i).css('background-color', "rgb("+r+","+g+","+b+")");
    // }
  }
}

/* ----- TapPortfolioSummary / TargetPersonalisation  \ Mutual Funds ----- */
/*
  - Used with HighCharts widget having an external legend table
  - Used to control limits of target personalization with sliders
  - Added global handling without second argument
*/
function addTotalRow(tid, edbtn){
  if($('td.FirstLevel').eq(0).text().trim() == 'No'){$('.PMStext').removeClass('hidden')}else{$('.PMStext').addClass('hidden')}
  $('.PMStxt').find('span').text($('td.ParentMarketSegment').eq(0).text())
  var tcols = 0,                                            // init visible columns number
      Dcols = [],                                           // init Decimal columns array
      t = $('#'+tid);
  if($(t).find('tbody tr').length < 2) return false;        // build totals footer only if more than one row, prevent creation when 'No results found'
  $(t).find('thead th:visible').each(function(a,c){
    if($(c).hasClass('Decimal')) Dcols.push($(c).index())   // build Dcols Array with column positions of type Decimal
    tcols++;                                                // increment visible columns number
  });
  if(!Dcols.length){
    $(t).find('tbody tr').eq(0).find('.formItem.Decimal:visible').each(function(a,c){
      Dcols.push($(c).closest('td').index());
    });
  }
  if(!$(t).find('tfoot').length){                 // build footer
    var tfoot = $('<tfoot/>');
    var frow = $('<tr class="totals"/>');
    for(var c = 0; c < tcols; c++){               // loop over all visible columns
      var cc = $('<td/>');
      if(c==0) cc.text('Total');                  // first cell
      else if(Dcols.indexOf(c) > -1){             // if is Decimal column
        cc.addClass($(t).find('tbody tr').eq(0).find('td').eq(c).attr('class')||'Decimal')
          .text(c+'%');
      }
      frow.append(cc);
    }
    $(t).append(tfoot);
    tfoot.append(frow);
  }
  var csum = 0,                                   // initialize changed target sum
      tchanged = 0,                               // initialize number of changed targets
      cchanged = -1,                              // initialize index of column for changed target slider
      rowslid =  $(t).find('.ui-slider').length;  // number of sliders in table
  for(var c = 0; c < Dcols.length; c++){          // build col sum footer row
    var sum = 0;
    $(t).find('tbody tr').each(function(j,r){
      var cell = $(r).find('td').eq(Dcols[c]);
      var cchanged = $(r).find('td.ispinned1').index();
      if(cell.find('span.slider-marker').length){
        sliderValue = parseFloat($(r).find('td').eq(Dcols[c]).find('span.slider-marker').text());
        sum += sliderValue;
        tchanged += (cchanged > -1 || sliderValue == 0) ? 1 : 0;
        csum += (cchanged > -1) ? sliderValue : 0;        // add value only if target value has been changed manually
      }else{
        if($(r).find('td').eq(Dcols[c]).find('input.Decimal').length){
          var inpval = $(r).find('td').eq(Dcols[c]).find('input.Decimal').val().trim();
          sum += parseFloat(RawNumber(inpval))||0;
        }else{
          sum += parseFloat(RawNumber($(r).find('td').eq(Dcols[c]).text().trim()));
        }
      }
    });
    $(t).find('tfoot td').eq(Dcols[c]).text(FormatNumber(sum.toFixed(2)+' %'));
  }
  if(rowslid > 0 && $('#'+edbtn).length){
    var tsumtot =  $('td.ParentMarketSegmentWeight').eq(0).text();         // set variable
    var psegment = $('td.ParentMarketSegment').eq(0).text();               // set variable
    if(csum > tsumtot){
      var msg = ss.get(tid+'_msgHigh').replace('~csum', csum).replace('~tsumtot', tsumtot)
      $.growl.warning({
        title: "Warning",
    	  message: msg,
    	  icon:  "warning",
    	  fixed: true
    	});
    	$('#'+edbtn).prop('disabled',true);
    }else if(rowslid == tchanged && csum < tsumtot){
      var msg = ss.get(tid+'_msgLow').replace('~csum', csum).replace('~tsumtot', tsumtot)
      $.growl.warning({
        title: "Warning",
    	  message: msg,
    	  icon:  "warning",
    	  fixed: true
    	});
    	$('#'+edbtn).prop('disabled',true);
    }else{
      $('#'+edbtn).prop('disabled',false);
    }
  }else if($(t).hasClass('limit0100')){
    var colTxt = $(t).find('thead th').eq(Dcols[0]).text().trim();
    var cval = parseInt($(t).find('tfoot td.Decimal').text());
    if(cval > 100){
      var msg = colTxt+' greater than 100%';
      $.growl.warning({title: "Warning", message: msg, icon:  "warning", fixed: true});
    }else if(cval < 0){
      var msg = colTxt+' below 0%';
      $.growl.warning({title: "Warning", message: msg, icon:  "warning", fixed: true});
    }
  }
}

function handleTAPalike(tid){
  var allRowClick = $('#'+tid+'.TAPalike').hasClass('allRowClick');
  $('#'+tid+'.TAPalike').find('tbody tr').each(function(i,r){
    $(r).addClass('R'+(i+1)).removeAttr('onclick style');
    if(allRowClick || $(r).find('td.exec:visible').length) $(r).addClass('rowClick');
  })
  $('#'+tid+'.TAPalike tr.rowClick').on('tap', function(e){
    var clickItem = $(this).find('td.exec a');
    if(clickItem.length){
      if(!$(e.target).is('.showmore, .showmore *, .icon, .tdwcbicon, .boolean, .boolean *, .CheckBoxIcon, .slider, .slider *') && e.target.nodeName != 'INPUT'){
        $(clickItem).click();
      }
    }
  });
};
function PopupInTable(oid, head, b1, b2){
  if($('#PIT_'+oid).closest('tr').find('td.inPopup .formItem.mandatory').length){$('#PIT_'+oid).closest('td').addClass('mandatory')}
  var RowNr = oid.split('_R').pop();
  $('#PIT_'+oid).closest('td').on('tap', function(e){
    if(!$('#PIT_'+oid+' div.item').length){
      if(!!head) $('#PIT_'+oid).prepend($('<div class="head"/>').text(head).append($('<span class="icon BlueMagic cancel" style="float:right" onclick="$(\'.PIT\').hide();"/>')));
      if(!!b1 || !!b2){
        $('#PIT_'+oid).append(
          $('<div class="foot"/>')
          .append(!!b1 ? $('<button class="BMButton Action Minimal" onclick ="ResetFieldsInPIT(this)"/>').text(b1) : '')
          .append(!!b2 ? $('<button class="BMButton Action Minimal" onclick="$(\'.PIT\').hide();"/>').text(b2) : '')
        )
      }
      ofieldVal[RowNr] = [];
      $(this).closest('tr').find('.inPopup').each(function(i,o){
        var olabel = $('#'+$(o).attr('headers')).text().trim();
        var lastChar = olabel.substr(olabel.length - 1); if(lastChar == "*"){olabel = olabel.slice(0,-1);}
        var ofield = $(o).find('.formItem').parent();
        if(!ofield.length) ofield = $(o).find('span').css('font-weight',700).parent();
        ofieldVal[RowNr].push($(o).find('.formItem').val());
        $('#PIT_'+oid+'>.body').append($('<div class="item"/>').append($('<span/>').text(olabel).append(lastChar == "*"?$('<span class="mandatory"/>').text('*'):'')).append(ofield));
      });
    }
    $('#PIT_'+oid).toggle();
    e.preventDefault();
  });
};

/* Start Performance Logger */
function logperf(ajx){
  if(!has.perf) return false;
  var pt  = perf.timing,
      pn  = perf.navigation,
      now = new Date().getTime();
  if(!ajx){
    var Navigation      = (pn.type==0?'Navigation':pn.type==1?'Reload':pn.type==2?'Back/Forward':'unknown');
    var totalduration   = ((now-pt.navigationStart)/1000).toFixed(3),
        serverduration  = ((pt.responseEnd-pt.navigationStart)/1000).toFixed(3),
        loaddom         = ((pt.domInteractive-pt.responseEnd)/1000).toFixed(3),
        JSduration      = ((now-pt.domInteractive)/1000).toFixed(3);
  }else{
    if(typeof AJAXstartstamp != 'number' || typeof AJAXcompletestamp != 'number') return false;
    var Navigation      = 'AJAX';
    var totalduration   = ((now-AJAXstartstamp)/1000).toFixed(3),
        serverduration  = ((AJAXcompletestamp-AJAXstartstamp)/1000).toFixed(3),
        loaddom         = ((now-AJAXcompletestamp)/1000).toFixed(3),
        JSduration      = (0/1000).toFixed(3);
  }
  var stabs           = $('.tab.active:visible, .InnerTab.InnerTabSelected:visible').clone().children().remove().end().text().trim();
  var docsize         = (document.body.innerHTML.length/1024).toFixed(3);
  var totsize         = ($('html').html().length/1024).toFixed(3);
  var pstatus         = $('.Phase_ErrorPhase').length ? 'Error' : 'OK';

  if(has.ls && ls.get('debugmode')!=null){
    console.info(
        'Page Reference         : '+$('body').data('phase')+' on '+$('body').attr('class')+
      '\nStatus                 : '+pstatus+
      '\ncorrelationId          : '+correlationId+
      '\nNavigation             : '+Navigation+
      '\nTotal Request Duration : '+totalduration+' sec'+
      '\nServer Side Duration   : '+serverduration+' sec'+
      '\nDOM Content Loaded     : '+loaddom+' sec'+
      '\nJS Exec Duration       : '+JSduration+' sec'+
      '\nSelected Tab           : '+stabs+
      '\nDocument Size          : '+docsize+' KB  (inline total: '+totsize+' KB)'
    );
  }
  if(!!profurl){
    (new Image()).src = profurl+        // send performance data to logfile
        '?cid='+correlationId+          // unique token (correlationId)
        '&page='+$('body').data('phase')+
        '&b='+theme+' '+filterBrowser()+// browser info
        '&trd='+totalduration+          // the duration from navigation start to now
        '&ssd='+serverduration+         // the duration server time
        '&ldc='+loaddom+                // the duration for DOM loading
        '&js='+JSduration+              // the duration of JavaScript execution
        '&stat='+pstatus+               // Page status
        '&size='+docsize+               // the size of the document
        '&stabs='+stabs+' '+pstatus+    // selected tabs + OK/Error
        '&ref='+Navigation+
        '&info='+window.location.pathname.split('/')[1];
  }
}
/* End Performance Logger */


/* ----- Helper Functions ----- */
function FormatNumber(x){                         // Unformatted valid JS Number (en) to locale formatting
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, tSep);
  return parts.join(dSep);
}
function RawNumber(x, ds){                        // Remove country specific formatting of numbers and return valid JS unformatted number
  var parts = x.toString().split(ds||dSep);       // ds = decimal separator defined to override default, in case of always raw numbers or formatted in edge Connect with e.g.: xxx.format('#,##0.00'). If undefined, use default.
  parts[0] = parts[0].replace(/[^0-9\-]+/g, '');
  return parts.join(".");
}
function FormatDate(rawdate){
  var df = typeof dateFormat == 'string' ? dateFormat : 'dd/mm/yyyy';
}
function RawDate(date){
  var df = typeof dateFormat == 'string' ? dateFormat : 'dd/mm/yyyy';
  var separator = df.match(/[^dmy]/);
  var dsplit = df.split(separator[0]);
  var datesplit = date.split(separator[0]);
  var day = datesplit[dsplit.indexOf("dd")];
  var month = datesplit[dsplit.indexOf("mm")];
  var year = datesplit[dsplit.indexOf("yyyy")];
  var d = new Date(year,month,day);
  return d.valueOf();
}
function reorderTableRows(tid,dataitem,order){
  var table = $('#'+tid);
  var rows = table.find('tbody tr').get();
  rows.sort(function(a,b){
    var keyA = $(a).data(dataitem);
    var keyB = $(b).data(dataitem);
    if (keyA < keyB) return (order=='asc')?-1:1;
    if (keyA > keyB) return (order=='asc')?1:-1;
    return 0;
  });
  $(rows).each(function(i,r){
    table.children('tbody').append(r);
  });
}
function reorderLists(lid,dataitem,order){
  var list = $(lid);
  var items = list.find('>li').get();
  items.sort(function(a,b){
    var keyA = $(a).data(dataitem);
    var keyB = $(b).data(dataitem);
    if (keyA < keyB) return (order=='asc')?-1:1;
    if (keyA > keyB) return (order=='asc')?1:-1;
    return 0;
  });
  $(items).each(function(i,r){
    list.append(r);
  });
}
jQuery.fn.htmlClean = function(){                 // Usage: e.g.: $('table:not(".minified")').htmlClean().addClass('minified');
  this.contents().filter(function(){
    if (this.nodeType != 3){
      $(this).htmlClean(); return false;
    }else{
      this.textContent = $.trim(this.textContent);
      return !/\S/.test(this.nodeValue);
    }
  }).remove();
  return this;
}
